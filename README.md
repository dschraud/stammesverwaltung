# Stammesverwaltung

## Development

If needed, setup virtualenv:

```shell
pip install virtualenv
pip install virtualenvwrapper

mkvirtualenv stammesverwaltung
workon stammesverwaltung
```

Setup the project environment:

```shell
pip install -r requirements.txt

python src/manage.py migrate
python src/manage.py createsuperuser
python src/manage.py runserver
```