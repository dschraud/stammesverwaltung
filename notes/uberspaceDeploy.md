# Deploy stammesverwaltung on uberspace

See [Django uberspace lab guide](https://lab.uberspace.de/guide_django.html)

* settings.py -> ALLOWED_HOSTS anpassen

1. Create web backend
2. setup daemon

* create databases
  * `username_stammesverwaltung`

## Todo

1. gitlab-ci.yml umschreiben
    - stop supervisord service
    - rewrite uwsgi config (use new template)
2. uwsgi config with defaults
3. create a config file containing all env variables

## Config file

* ~/config/stammesverwaltung.staging.cfg
* ~/config/stammesverwaltung.prod.cfg

```shell
export SV_DOMAIN=sv.pfadfinder-hischaid.de
export SV_PORT=443
export SV_DEVELOPER_MAIL=svadmin@domain.com
export SV_DB_NAME=foo
export SV_DB_URL=foo
export SV_DB_USER=foo
export SV_DB_PASSWORD=foo
export SV_DJANGO_SECRET=asdfasdf
export SV_FROM_EMAIL=verwaltung@domain.com
export SV_MAIL_HOST=neujmin.uberspace.de
export SV_MAIL_USER=foo
export SV_MAIL_PASSWORD=geheim
```

sourced in uwsgi config

## uwsgi config template

https://uwsgi-docs.readthedocs.io/en/latest/Configuration.html

```shell
[uwsgi]
base = /home/$(USERNAME)/projects/stammesverwaltung
chdir = /home/$(USERNAME)/projects

http = :8000
master = true
wsgi-file = %(base)/wsgi.py
touch-reload = %(wsgi-file)

# ~/config/stammesverwaltung.cfg

app = wsgi

#virtualenv = %(chdir)/venv

plugin = python

uid = $(USERNAME)
gid = $(USERNAME)
```

## Deployment

* staging -> current master branch (automaticall with gitlab ci)
* prod -> latest tag (automatically with gitlab ci)

## Customization

## First steps for smooth deployment

* uberspace username
* domain (e.g. verwaltung.dpsg-friedberg.de)
* port
* developer email address
* database credentials
* database url
* database name
* django secret key (create during deploy)
* email configuration
  * from mail (used in finance/utils)
* domain (currently used from request)


## later

* Stammesname (Impressum)
* Stammeswebsite URL (Impressum)
* Scoutnet Stammesnummer / Kalender ID
* Logo upload
* media folders (uploaded images + pdf files)
* events/utils
  * hardcoded scoutnet values
* make latex templates customizable
* nami login data
  * username
  * password
  * stammesnummer
* email templates
* default accounts
* debit export hardcoded IBANs