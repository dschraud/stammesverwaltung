---
- hosts: localhost
  gather_facts: no

  vars:
    app_name: test
    app_dir: /home/{{ ansible_env.USER }}/projects
    domain: "{{ app_name }}.{{ ansible_env.USER }}.uber.space"
    admin_name: Daniel Schraudner
    admin_email: daniel@dschraudner.de
    admin_user_first_name: Test
    admin_user_last_name: User
    admin_user_email: test@example.com
    admin_user_password: changeme
    settings: config.settings.production
    secret_key: "{{ lookup('password', '/dev/null') }}"
    smtp_pass: asdf1234
    version: master

  tasks:
  - name: get database password from config
    command: sed -nr "/^\[client\]/ { :l /^password[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ~/.my.cnf
    args:
      warn: false
    register: db_pass

  - name: get random free port
    set_fact:
      app_port: "{{ 65535 |random(61000) }}"

  - name: Pull git repository
    git:
      repo: 'https://gitlab.com/dschraudner/stammesverwaltung.git'
      dest: "{{ app_dir }}/sv_{{ app_name }}"
      version: "{{ version }}"

  - name: uwsgi is installed
    pip:
      name: uwsgi
      executable: pip3.6
      state: present
      extra_args: --user

  - name: supervisord service config for uwsgi
    template:
      src: uwsgi_emperor.j2
      dest: ~/etc/services.d/uwsgi.ini

  - name: error log for uwsgi
    file:
      path: ~/logs/uwsgi/err.log
      state: touch

  - name: out log for uwsgi
    file:
      path: ~/logs/uwsgi/out.log
      state: touch

  - name: uwsgi config for app {{ app_name }}
    template:
      src: uwsgi_app.j2
      dest: ~/etc/uwsgi/apps-enabled/sv_{{ app_name }}.ini

  - name: restart uwsgi
    supervisorctl:
      name: uwsgi
      state: restarted

  - name: get registered uberspace domains
    shell: uberspace web domain list 2>&1
    register: currentdomains

  - name: add domain in uberspace
    command: uberspace web domain add {{ domain }}
    when: domain not in currentdomains.stdout.split('\n')

  - name: set backend in uberspace
    shell: uberspace web backend set --http --port {{ app_port }} {{ domain }}

  - name: create database
    mysql_db:
      name: "{{ ansible_env.USER }}_sv_{{ app_name }}"
      state: present

  - name: install requirements
    pip:
      requirements: "{{ app_dir }}/sv_{{ app_name }}/requirements.txt"
      virtualenv: "{{ app_dir }}/sv_{{ app_name }}/venv"
      virtualenv_python: python3.6

  - name: collect static files for django
    environment:
      SECRET_KEY: "{{ secret_key }}"
      DB_NAME: "{{ ansible_env.USER }}_sv_{{ app_name }}"
      DB_USER: "{{ ansible_env.USER }}"
      DB_PASSWORD: "{{ db_pass.stdout }}"
    django_manage:
      command: collectstatic
      app_path:  "{{ app_dir }}/sv_{{ app_name }}/src"
      settings: "{{ settings }}"
      virtualenv: "{{ app_dir }}/sv_{{ app_name }}/venv"

  - name: apply django database migrations
    environment:
      SECRET_KEY: "{{ secret_key }}"
      DB_NAME: "{{ ansible_env.USER }}_sv_{{ app_name }}"
      DB_USER: "{{ ansible_env.USER }}"
      DB_PASSWORD: "{{ db_pass.stdout }}"
    django_manage:
      command: migrate
      app_path:  "{{ app_dir }}/sv_{{ app_name }}/src"
      settings: "{{ settings }}"
      virtualenv: "{{ app_dir }}/sv_{{ app_name }}/venv"

  - name: look for django admin user
    environment:
      SECRET_KEY: "{{ secret_key }}"
      DB_NAME: "{{ ansible_env.USER }}_sv_{{ app_name }}"
      DB_USER: "{{ ansible_env.USER }}"
      DB_PASSWORD: "{{ db_pass.stdout }}"
    register: currentsuperuser
    django_manage:
      command: shell -c "from authentication.models import SVUser; print(SVUser.objects.filter(is_superuser=True).exists())"
      app_path:  "{{ app_dir }}/sv_{{ app_name }}/src"
      settings: "{{ settings }}"
      virtualenv: "{{ app_dir }}/sv_{{ app_name }}/venv"

  - name: create django admin user
    environment:
      SECRET_KEY: "{{ secret_key }}"
      DB_NAME: "{{ ansible_env.USER }}_sv_{{ app_name }}"
      DB_USER: "{{ ansible_env.USER }}"
      DB_PASSWORD: "{{ db_pass.stdout }}"
    django_manage:
      command: shell -c "from authentication.models import SVUser; SVUser.objects.create_superuser(first_name='{{ admin_user_first_name }}', last_name='{{ admin_user_last_name }}', email='{{ admin_user_email }}', password='{{ admin_user_password }}')"
      app_path:  "{{ app_dir }}/sv_{{ app_name }}/src"
      settings: "{{ settings }}"
      virtualenv: "{{ app_dir }}/sv_{{ app_name }}/venv"
    when: currentsuperuser.out == 'False\n'

  - name: restart uwsgi
    supervisorctl:
      name: uwsgi
      state: restarted

