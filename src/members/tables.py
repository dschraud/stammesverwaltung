from django_tables2 import Table
from django_tables2.columns import TemplateColumn, Column
from django.utils.translation import gettext_lazy as _

from . import models


class MemberTable(Table):
    details = TemplateColumn(
        ('<a href="{% url "members:details_member" record.nr %}" '
         'class="btn btn-primary btn-sm btn-esm">Details</a>'),
        verbose_name=_('Details'),
        orderable=False
    )
    address = Column(verbose_name=_('Address'))

    def order_address(self, QuerySet, is_descending):
        QuerySet = QuerySet.order_by(
            ('-' if is_descending else '') + 'zip',
            ('-' if is_descending else '') + 'street'
        )
        return (QuerySet, True)

    class Meta:
        model = models.Member
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'nr',
            'first_name',
            'last_name',
            'function',
            'section',
            'address',
            'dob',
            'gender'
        )
        attrs = {'class': 'table table-striped table-sm'}

        def check_promotion_class(record):
            if record.function == 0 and record.section != record.ideal_section:
                if record.age > record.section.max_age:
                    return 'text-danger'
                else:
                    return 'text-info'
            else:
                return ''

        def check_promotion_data_tooltip(record):
            if record.function == 0 and record.section != record.ideal_section:
                return 'true'
            else:
                return 'false'

        def check_promotion_title(record):
            if record.function == 0 and record.section != record.ideal_section:
                if record.age > record.section.max_age:
                    return _('%(member)s is %(age)s years old and must '
                             'be promoted to %(section)s!') % {
                            'member': record,
                            'age': record.age,
                            'section': record.ideal_section
                        }
                else:
                    return _('%(member)s is %(age)s years old and can '
                             'be promoted to %(section)s!') % {
                            'member': record,
                            'age': record.age,
                            'section': record.ideal_section
                        }
            else:
                return ''

        row_attrs = {
                'class': check_promotion_class,
                'data-tooltip': check_promotion_data_tooltip,
                'data-placement': 'bottom',
                'title': check_promotion_title,
        }


class MandateTable(Table):
    edit = TemplateColumn(
        ('<a href="{%% url "members:edit_mandate" record.reference %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "members:delete_mandate" record.reference %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    def __init__(self, queryset, can_edit, *args, **kwargs):
        super().__init__(queryset, args, kwargs)
        if not can_edit:
            self.exclude = self.exclude + ('edit',)
            self.exclude = self.exclude + ('delete',)

    class Meta:
        model = models.SEPAMandate
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'reference',
            'depositor',
            'iban',
            'bic',
            'bank',
            'date'
        )
        attrs = {'class': 'table table-striped table-sm'}
