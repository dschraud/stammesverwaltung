from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from rolepermissions.roles import assign_role
from django.utils.translation import gettext_lazy as _
from datetime import datetime

from . import models
from authentication import roles
from finances.models import MembershipFeeCollection, Debit, DebitGroup

# Create your tests here.


class ViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
        )

        self.section1 = models.Section.objects.create(
                name='Beever',
                min_age=10,
                max_age=14
        )
        self.section2 = models.Section.objects.create(
                name='Cupse',
                min_age=13,
                max_age=17
        )
        self.mandate1 = models.SEPAMandate.objects.create(
                reference='1-1',
                depositor='Martina Mustermann',
                iban='DE00770500000000123456',
                bic='DJANGODEF12',
                bank='Django Reserve',
                date='2017-04-18'
                )
        self.mandate2 = models.SEPAMandate.objects.create(
                reference='1-2',
                depositor='Jochen Mustermann',
                iban='DE00770500000000789012',
                bic='DJANGODEF12',
                bank='Django Reserve',
                date='2018-05-20'
                )
        self.mandate3 = models.SEPAMandate.objects.create(
                reference='2-1',
                depositor='Jakob Mustermann',
                iban='DE00770500000000789013',
                bic='DJANGODEF12',
                bank='Django Reserve',
                date='2018-05-20'
                )
        self.member1 = models.Member.objects.create(
                nr=1,
                first_name='Max',
                last_name='Mustermann',
                section=self.section1,
                function=1,
                nami_nr=100001,
                fee=15.00,
                mandate_membership_fee=self.mandate1,
                mandate_participation_fee=self.mandate2,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m',
                mobile='016012345678',
                user=self.user
        )
        self.member2 = models.Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section1,
                function=0,
                nami_nr=100002,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='f'
        )
        self.member3 = models.Member.objects.create(
                nr=3,
                first_name='Mario',
                last_name='Mustermann',
                section=self.section2,
                function=0,
                nami_nr=100003,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2002-01-01',
                gender='m'
        )
        self.mfc = MembershipFeeCollection.objects.create(
                year=2066,
                nr=1
        )
        self.debit_group = DebitGroup.objects.create(
                from_date='2018-12-11',
                to_date='2019-01-10',
                execution_date='2019-01-15'
        )
        self.debit = Debit.objects.create(
                member=self.member3,
                amount=15.00,
                reference='Membership fee',
                group=self.debit_group,
                mfc=self.mfc
        )

        self.user.is_active = True
        self.user.save()
        self.client.login(email='test@test.com', password='1234asdfg')

    def test_member_list_view_login_required(self):
        self.client.logout()
        response = self.client.get(reverse('members:list_member'))
        self.assertNotEqual(response.status_code, 200)

    def test_member_list_view(self):
        response = self.client.get(reverse('members:list_member'))

        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, 'Mustermann', status_code=200)
        self.assertContains(response, 'Beever', status_code=200)
        self.assertNotContains(response, '100001', status_code=200)
        self.assertNotContains(response, 'Maria', status_code=200)
        self.assertNotContains(response, 'Mario', status_code=200)

    def test_member_list_view_leader(self):
        assign_role(self.user, roles.Leader)
        response = self.client.get(reverse('members:list_member'))

        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, 'Maria', status_code=200)
        self.assertContains(response, 'Mustermann', status_code=200)
        self.assertContains(response, 'Beever', status_code=200)

        self.assertNotContains(response, '100001', status_code=200)
        self.assertNotContains(response, 'Mario', status_code=200)

    def test_member_list_view_board(self):
        assign_role(self.user, roles.Board)
        response = self.client.get(reverse('members:list_member'))

        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, 'Maria', status_code=200)
        self.assertContains(response, 'Mario', status_code=200)
        self.assertContains(response, 'Mustermann', status_code=200)
        self.assertContains(response, 'Beever', status_code=200)
        self.assertNotContains(response, '100001', status_code=200)

    def test_mandate_list_view(self):
        response = self.client.get(reverse('members:list_mandate'))

        self.assertContains(response, 'Martina', status_code=200)
        self.assertContains(response, 'Jochen', status_code=200)
        self.assertNotContains(response, 'Jakob', status_code=200)

    def test_mandate_list_view_leader(self):
        assign_role(self.user, roles.Leader)
        response = self.client.get(reverse('members:list_mandate'))

        self.assertContains(response, 'Martina', status_code=200)
        self.assertContains(response, 'Jochen', status_code=200)
        self.assertNotContains(response, 'Jakob', status_code=200)

    def test_mandate_list_view_board(self):
        assign_role(self.user, roles.Board)
        response = self.client.get(reverse('members:list_mandate'))

        self.assertContains(response, 'Martina', status_code=200)
        self.assertContains(response, 'Jochen', status_code=200)
        self.assertContains(response, 'Jakob', status_code=200)

    def test_mfc_list_view_not_allowed(self):
        response = self.client.get(reverse('members:mfc'))

        self.assertEqual(response.status_code, 403)

    def test_mfc_list_view(self):
        assign_role(self.user, roles.Cashier)
        response = self.client.get(reverse('members:mfc'))

        self.assertContains(response, 2066, status_code=200)

    def test_member_detail_view(self):
        response = self.client.get(reverse(
                'members:details_member',
                kwargs={'pk': self.member1.pk}
        ))
        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, '100001', status_code=200)
        self.assertContains(response, '016012345678', status_code=200)

    def test_member_detail_view_not_allowed(self):
        response = self.client.get(reverse(
                'members:details_member',
                kwargs={'pk': self.member2.pk}
        ))

        self.assertEqual(response.status_code, 404)

    def test_member_detail_view_leader(self):
        assign_role(self.user, roles.Leader)
        response = self.client.get(reverse(
                'members:details_member',
                kwargs={'pk': self.member2.pk}
        ))
        self.assertContains(response, 'Maria', status_code=200)
        self.assertContains(response, '100002', status_code=200)

    def test_member_detail_view_board(self):
        assign_role(self.user, roles.Board)
        response = self.client.get(reverse(
                'members:details_member',
                kwargs={'pk': self.member3.pk}
        ))
        self.assertContains(response, 'Mario', status_code=200)
        self.assertContains(response, '100003', status_code=200)

    def test_member_detail_view_without_mandates(self):
        response = self.client.get(reverse(
                'members:details_member',
                kwargs={'pk': self.member1.pk}
        ))

        self.assertNotContains(response, 'DE00770500000000123456', status_code=200)
        self.assertNotContains(response, 'DE00770500000000789012', status_code=200)

    def test_member_detail_view_with_mandates_secretary(self):
        assign_role(self.user, roles.Secretary)
        response = self.client.get(reverse(
                'members:details_member',
                kwargs={'pk': self.member1.pk}
        ))

        self.assertContains(response, 'DE00770500000000123456', status_code=200)
        self.assertContains(response, 'DE00770500000000789012', status_code=200)

    def test_member_create_view_not_allowed(self):
        response = self.client.post(reverse('members:create_member'), {
                'nr': 4,
                'first_name': 'Manuela',
                'last_name': 'Mustermann',
                'section': self.section2.pk,
                'function': 0,
                'nami_nr': 100004,
                'fee': 15.00,
                'street': 'Musterstreet 12',
                'zip': '12345',
                'town': 'Mustertown',
                'dob': '2005-01-01',
                'gender': 'f'
            }, follow=True)

        self.assertEqual(response.status_code, 403)

    def test_member_create_view(self):
        assign_role(self.user, roles.Secretary)
        response = self.client.post(reverse('members:create_member'), {
                'nr': 4,
                'first_name': 'Manuela',
                'last_name': 'Mustermann',
                'section': self.section2.pk,
                'function': 0,
                'nami_nr': 100004,
                'fee': 15.00,
                'street': 'Musterstreet 12',
                'zip': '12345',
                'town': 'Mustertown',
                'dob': '2005-01-01',
                'gender': 'f',
                'photo_rights': 'false'
            }, follow=True)

        member = models.Member.objects.get(pk=4)

        self.assertEqual('Musterstreet 12', member.street)
        self.assertContains(response, 'Musterstreet 12', status_code=200)

    def test_mandate_create_view_not_allowed(self):
        response = self.client.post(reverse('members:create_mandate'), {
                'reference': '4-1',
                'depositor': 'Madleine Mustermann',
                'iban': 'DE00770500000000123456',
                'bic': 'DJANGODEF12',
                'bank': 'Django Reserve',
                'date': '2017-04-18'
            })

        self.assertEqual(response.status_code, 403)

    def test_mandate_create_view(self):
        assign_role(self.user, roles.Secretary)
        self.client.post(reverse('members:create_mandate'), {
                'reference': '4-1',
                'depositor': 'Madleine Mustermann',
                'iban': 'DE00770500000000123456',
                'bic': 'DJANGODEF12',
                'bank': 'Django Reserve',
                'date': '2017-04-18'
            })

        mandate = models.SEPAMandate.objects.get(pk='4-1')

        self.assertEqual('Madleine Mustermann', mandate.depositor)

    def test_member_update_view_not_allowed(self):
        response = self.client.post(reverse(
                        'members:edit_member',
                        kwargs={'pk': self.member1.pk}
                        ), {
                'nr': 1,
                'first_name': 'Max',
                'last_name': 'Masterfrau',
                'section': self.section1.pk,
                'function': 0,
                'nami_nr': 100001,
                'fee': 15.00,
                'street': 'Musterstreet 12',
                'zip': '12345',
                'town': 'Mustertown',
                'dob': '2000-01-01',
                'gender': 'm'
            }, follow=True)

        self.assertEqual(response.status_code, 403)

    def test_member_update_view(self):
        assign_role(self.user, roles.Secretary)
        response = self.client.post(reverse(
                        'members:edit_member',
                        kwargs={'pk': self.member1.pk}
                        ), {
                'nr': 1,
                'first_name': 'Max',
                'last_name': 'Masterfrau',
                'section': self.section1.pk,
                'function': 0,
                'nami_nr': 100001,
                'fee': 15.00,
                'street': 'Musterstreet 12',
                'zip': '12345',
                'town': 'Mustertown',
                'dob': '2000-01-01',
                'gender': 'm'
            }, follow=True)

        members = models.Member.objects.all()
        member = models.Member.objects.get(pk=1)

        self.assertEqual(3, len(members))
        self.assertEqual('Masterfrau', member.last_name)
        self.assertContains(response, 'Masterfrau', status_code=200)

    def test_mandate_update_view_not_allowed(self):
        response = self.client.post(
                reverse('members:edit_mandate', kwargs={'pk': self.mandate1.reference}), {
                        'reference': '1-1',
                        'depositor': 'Martina Mustermann',
                        'iban': 'DE00770500000000123456',
                        'bic': 'DJANGODEF12',
                        'bank': 'FED',
                        'date': '2017-04-18'
                },
                follow=True)

        self.assertEqual(response.status_code, 403)

    def test_mandate_update_view(self):
        assign_role(self.user, roles.Secretary)
        response = self.client.post(
                reverse('members:edit_mandate', kwargs={'pk': self.mandate1.reference}), {
                        'reference': '1-1',
                        'depositor': 'Martina Mustermann',
                        'iban': 'DE00770500000000123456',
                        'bic': 'DJANGODEF12',
                        'bank': 'FED',
                        'date': '2017-04-18'
                },
                follow=True)

        mandate = models.SEPAMandate.objects.get(pk=self.mandate1.reference)

        self.assertEqual(response.status_code, 200)
        self.assertEqual('FED', mandate.bank)

    def test_member_delete_view_not_allowed(self):
        response = self.client.post(
                reverse('members:delete_member', kwargs={'pk': self.member1.nr}),
                follow=True)

        self.assertEqual(response.status_code, 403)

    def test_member_delete_view(self):
        assign_role(self.user, roles.Secretary)
        response = self.client.post(
                reverse('members:delete_member', kwargs={'pk': self.member1.nr}),
                follow=True)

        members = models.Member.objects.all()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, len(members))

    def test_mandate_delete_view_not_allowed(self):
        response = self.client.post(
                reverse('members:delete_mandate', kwargs={'pk': self.mandate3.reference}),
                follow=True)

        self.assertEqual(response.status_code, 403)

    def test_mandate_delete_view(self):
        assign_role(self.user, roles.Secretary)
        response = self.client.post(
                reverse('members:delete_mandate', kwargs={'pk': self.mandate3.reference}),
                follow=True)

        mandates = models.SEPAMandate.objects.all()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, len(mandates))

    def test_member_export_view_not_allowed(self):
        response = self.client.get(reverse('members:export_members'))

        self.assertEqual(response.status_code, 403)

    def test_member_export_view_leader(self):
        assign_role(self.user, roles.Leader)
        response = self.client.get(reverse('members:export_members'))

        self.assertEquals(
            response.get('Content-Disposition'), 'attachment; filename="%s-%s.csv"'
            % (_('members'), datetime.today().strftime('%d-%m-%Y'))
        )
        self.assertContains(response, 'Max')
        self.assertContains(response, 'Maria')
        self.assertNotContains(response, 'Mario')

    def test_member_export_view_secretary(self):
        assign_role(self.user, roles.Secretary)
        response = self.client.get(reverse('members:export_members'))

        self.assertEquals(
            response.get('Content-Disposition'), 'attachment; filename="%s-%s.csv"'
            % (_('members'), datetime.today().strftime('%d-%m-%Y'))
        )
        self.assertContains(response, 'Max')
        self.assertContains(response, 'Maria')
        self.assertContains(response, 'Mario')


class ModelTests(TestCase):
    def test_member_string(self):
        section = models.Section.objects.create(
                name='Beever',
                min_age=10,
                max_age=14
                )
        member = models.Member.objects.create(
                nr=1,
                first_name='Max',
                last_name='Mustermann',
                section=section,
                function=0,
                nami_nr=100001,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m'
                )

        self.assertEqual('Max Mustermann', str(member))
