from django_tables2 import SingleTableMixin, SingleTableView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic.detail import DetailView, View
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.contrib.messages.views import SuccessMessageMixin
from django_filters.views import FilterView
from django.contrib import messages
from rolepermissions.mixins import HasRoleMixin
from rolepermissions.checkers import has_role
from import_export.formats import base_formats
from django.db.models import Q
from datetime import datetime
import pytoml as toml
import os
import codecs

from . import models
from . import tables
from . import utils
from . import filters
from . import admin
from . import forms
from finances.tables import MembershipFeeCollectionTable
from finances.models import MembershipFeeCollection
from authentication import roles

# Create your views here.


class MemberListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    table_class = tables.MemberTable
    template_name = 'members/member_list.html'
    filterset_class = filters.MemberFilter
    strict = False

    def get_queryset(self):
        if has_role(self.request.user, [roles.Board, roles.Secretary]):
            return models.Member.objects.all().select_related('section').order_by(
                'section',
                'function',
                'first_name',
                'last_name'
            )
        elif has_role(self.request.user, [roles.Leader]):
            sections = models.Member.objects.filter(
                function=1,
                user=self.request.user
            ).values_list('section', flat=True)
            return models.Member.objects.filter(
                section__in=sections
            ).select_related('section').order_by(
                'section',
                'function',
                'first_name',
                'last_name'
            )
        else:
            return models.Member.objects.filter(
                user=self.request.user
            ).select_related('section').order_by(
                'section',
                'function',
                'first_name',
                'last_name'
            )


class MandateListView(LoginRequiredMixin, SingleTableMixin, FilterView):
    template_name = 'members/mandate_list.html'
    filterset_class = filters.MandateFilter
    strict = False

    def get_table(self):
        if has_role(self.request.user, [roles.Board, roles.Secretary]):
            return tables.MandateTable(
                self.object_list,
                True
            )
        else:
            return tables.MandateTable(
                self.object_list.filter(
                    Q(participation_fee__user=self.request.user) |
                    Q(membership_fee__user=self.request.user)
                ),
                False
            )


class MembershipFeeCollectionListView(LoginRequiredMixin, HasRoleMixin, SingleTableView):
    allowed_roles = [roles.Board, roles.Secretary, roles.Cashier, roles.Leader]
    table_class = MembershipFeeCollectionTable
    template_name = 'members/mfc_list.html'

    def get_queryset(self):
        return MembershipFeeCollection.objects.all()


class MemberDetailView(LoginRequiredMixin, DetailView):
    model = models.Member

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            with open(os.path.expanduser('~/.pynami.conf'), 'r') as cfg:
                config = toml.load(cfg)
            nami = utils.NaMi(config['username'], config['password'], config['stammesnummer'])
            nami.auth()
            nami_object = nami.get_nami_mitglied(context['object'].nami_nr)
        except Exception:
            nami_object = {'id': _('NaMi is not available at the moment!')}

        context['nami_object'] = nami_object
        return context

    def get_queryset(self):
        if has_role(self.request.user, [roles.Board, roles.Secretary]):
            return models.Member.objects.all()
        elif has_role(self.request.user, [roles.Leader]):
            sections = models.Member.objects.filter(
                function=1,
                user=self.request.user
            ).values_list('section', flat=True)
            return models.Member.objects.filter(section__in=sections)
        else:
            return models.Member.objects.filter(user=self.request.user)


class MemberCreateView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, CreateView):
    allowed_roles = [roles.Board, roles.Secretary]
    success_message = _("Member '%(first_name)s %(last_name)s' "
                        "has been created successfully!")
    template_name = 'members/form.html'
    form_class = forms.MemberForm

    def get_success_url(self):
        return reverse('members:details_member', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create member')
        return context


class MandateCreateView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, CreateView):
    allowed_roles = [roles.Board, roles.Secretary]
    success_message = _("Mandate '%(depositor)s (%(reference)s)' "
                        "has been created successfully!")
    template_name = 'members/form.html'
    form_class = forms.MandateForm

    def get_success_url(self):
        return reverse('members:list_mandate')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create mandate')
        return context


class MemberUpdateView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Secretary]
    success_message = _("Member '%(first_name)s %(last_name)s' "
                        "has been updated successfully!")
    template_name = 'members/form.html'
    form_class = forms.MemberForm

    def get_queryset(self):
        return models.Member.objects.all()

    def get_success_url(self):
        return reverse('members:details_member', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit member')
        return context


class MandateUpdateView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Secretary]
    success_message = _("Mandate '%(depositor)s (%(reference)s)' "
                        "has been updated successfully!")
    template_name = 'members/form.html'
    form_class = forms.MandateForm

    def get_queryset(self):
        return models.SEPAMandate.objects.all()

    def get_success_url(self):
        return reverse('members:list_mandate')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit mandate')
        return context


class MemberDeleteView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Secretary]
    success_message = _("Member '%(first_name)s %(last_name)s' "
                        "has been deleted successfully!")
    model = models.Member
    success_url = reverse_lazy('members:list_member')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)


class MandateDeleteView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Secretary]
    success_message = _("Mandate '%(depositor)s (%(reference)s)' "
                        "has been deleted successfully!")
    model = models.SEPAMandate

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['members'] = (
            self.object.membership_fee.all() |
            self.object.participation_fee.all()
        ).distinct()
        return context

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('members:list_mandate')


class MemberExportView(LoginRequiredMixin, HasRoleMixin, View):
    allowed_roles = [roles.Board, roles.Secretary, roles.Leader]

    def get(self, request):
        qs = self.get_queryset()
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = (
            'attachment; filename="%s-%s.csv"'
            % (_('members'), datetime.today().strftime('%d-%m-%Y'))
        )
        response.write(codecs.BOM_UTF8)

        dataset = admin.MemberResource().export(queryset=qs)
        response.write(base_formats.CSV().export_data(
            dataset,
            delimiter=';')
        )

        return response

    def get_queryset(self):
        if has_role(self.request.user, [roles.Board, roles.Secretary]):
            return models.Member.objects.all().select_related('section').order_by(
                'section',
                'function',
                'first_name',
                'last_name'
            )
        else:
            sections = models.Member.objects.filter(
                function=1,
                user=self.request.user
            ).values_list('section', flat=True)
            return models.Member.objects.filter(
                section__in=sections
            ).select_related('section').order_by(
                'section',
                'function',
                'first_name',
                'last_name'
            )


class SectionCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    """
    Board members can create new sections.
    """

    allowed_roles = [roles.Board]
    template_name = 'members/form.html'
    form_class = forms.SectionForm

    def get_success_url(self):
        return reverse('auth:administration')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create section')
        return context


class SectionUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    """
    Board members can update extisting sections.
    """

    allowed_roles = [roles.Board]
    template_name = 'members/form.html'
    form_class = forms.SectionForm

    def get_queryset(self):
        return models.Section.objects.all()

    def get_success_url(self):
        return reverse('auth:administration')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit section')
        return context


class SectionDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    """
    Board members can delete extisting sections.
    """

    allowed_roles = [roles.Board]
    model = models.Section

    def get_success_url(self):
        return reverse('auth:administration')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['members'] = models.Member.objects.filter(section=self.object)
        return context
