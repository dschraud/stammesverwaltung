"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

app_name = 'members'

urlpatterns = [
    path(_('create-member/'), views.MemberCreateView.as_view(), name='create_member'),
    path(_('edit-member/<pk>/'), views.MemberUpdateView.as_view(), name='edit_member'),
    path(_('delete-member/<pk>/'), views.MemberDeleteView.as_view(), name='delete_member'),
    path(_('create-mandate/'), views.MandateCreateView.as_view(), name='create_mandate'),
    path(_('edit-mandate/<pk>/'), views.MandateUpdateView.as_view(), name='edit_mandate'),
    path(_('delete-mandate/<pk>/'), views.MandateDeleteView.as_view(), name='delete_mandate'),
    path(_('mfc/'), views.MembershipFeeCollectionListView.as_view(), name='mfc'),
    path(_('list-members/'), views.MemberListView.as_view(), name='list_member'),
    path(_('list-mandates/'), views.MandateListView.as_view(), name='list_mandate'),
    path(_('member/<pk>/'), views.MemberDetailView.as_view(), name='details_member'),
    path(_('export-members/'), views.MemberExportView.as_view(), name='export_members'),
    path(_('create-section/'), views.SectionCreateView.as_view(), name='create_section'),
    path(_('edit-section/<pk>/'), views.SectionUpdateView.as_view(), name='edit_section'),
    path(_('delete-section/<pk>/'), views.SectionDeleteView.as_view(), name='delete_section'),
]
