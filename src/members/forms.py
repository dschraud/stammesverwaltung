from django import forms
from bootstrap_datepicker_plus.widgets import DatePickerInput

from config.widgets import EURWidget, BootstrapSelect2Widget

from . import models


class MemberForm(forms.ModelForm):
    class Meta:
        model = models.Member
        fields = [
            'nr',
            'first_name',
            'last_name',
            'section',
            'function',
            'nami_nr',
            'fee',
            'mandate_membership_fee',
            'mandate_participation_fee',
            'street',
            'zip',
            'town',
            'dob',
            'gender',
            'landline',
            'mobile',
            'email',
            'juleica_nr',
            'photo_rights',
            'user'
        ]
        widgets = {
            'nr': forms.NumberInput(attrs={'min': '1'}),
            'section': BootstrapSelect2Widget,
            'function': BootstrapSelect2Widget,
            'nami_nr': forms.NumberInput(attrs={'min': '1'}),
            'fee': EURWidget,
            'mandate_membership_fee': BootstrapSelect2Widget,
            'mandate_participation_fee': BootstrapSelect2Widget,
            'zip': forms.NumberInput(attrs={'min': '10000', 'max': '99999'}),
            'dob': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'gender': BootstrapSelect2Widget,
            'email': forms.EmailInput,
            'juleica_nr': forms.NumberInput(attrs={'min': '0'}),
            'user': BootstrapSelect2Widget,
        }


class MandateForm(forms.ModelForm):
    class Meta:
        model = models.SEPAMandate
        fields = [
            'reference',
            'depositor',
            'iban',
            'bic',
            'bank',
            'date'
        ]
        widgets = {
            'date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
        }


class SectionForm(forms.ModelForm):
    """
    Form for SectionCreateView and SectionUpdateView.
    """

    class Meta:
        model = models.Section
        fields = [
            'name',
            'min_age',
            'max_age',
        ]
