import requests
import json
from datetime import datetime


class NaMi:
    server = 'https://nami.dpsg.de'
    auth_url = '/ica/rest/nami/auth/manual/sessionStartup'
    search_url = '/ica/rest/api/1/2/service/nami/search/result-list'

    def __init__(self, username, password, stammesnummer):
        self.session = requests.Session()
        self.username = username
        self.password = password
        self.stammesnummer = stammesnummer

    def auth(self):
        payload = {
            'Login': 'API',
            'username': self.username,
            'password': self.password,
        }
        url = "%s%s" % (NaMi.server, NaMi.auth_url)
        r = self.session.post(url, data=payload, timeout=1)
        r.raise_for_status()

    def get_nami_mitglied(self, nami_nr):
        # Get id by nami_nr
        search = {
            'mitgliedsNummber': nami_nr,
        }
        params = {
            'searchedValues': json.dumps(search),
            'page': 1,
            'start': 0,
            'limit': 999999
        }
        url = "%s%s" % (NaMi.server, NaMi.search_url)
        r = self.session.get(url, params=params, timeout=1)
        r.raise_for_status()
        mglid = r.json()['data'][0]['entries_id']

        # Get data set for nami_mitglied
        url = (
            '%s/ica/rest/nami/mitglied/filtered-for-navigation/gruppierung/gruppierung/'
            '%s/%s/'
            % (NaMi.server, self.stammesnummer, mglid)
        )
        r = self.session.get(url, timeout=1)
        r.raise_for_status()
        nami_mitglied = r.json()['data']

        # Get taetigkeiten
        url = (
            '%s/ica/rest/nami/zugeordnete-taetigkeiten/filtered-for-navigation/'
            'gruppierung-mitglied/mitglied/%s/flist'
            % (NaMi.server, mglid)
        )
        r = self.session.get(url, timeout=1)
        r.raise_for_status()
        nami_mitglied['taetigkeiten'] = r.json()['data']

        # Convert date strings
        for t in nami_mitglied['taetigkeiten']:
            t['entries_aktivVon'] = datetime.strptime(t['entries_aktivVon'], '%Y-%m-%d %H:%M:%S')
            if t['entries_aktivBis']:
                t['entries_aktivBis'] = datetime.strptime(
                                            t['entries_aktivBis'],
                                            '%Y-%m-%d %H:%M:%S'
                                        )
        nami_mitglied['geburtsDatum'] = datetime.strptime(
                                            nami_mitglied['geburtsDatum'],
                                            '%Y-%m-%d %H:%M:%S'
                                        )
        if nami_mitglied['austrittsDatum']:
            nami_mitglied['austrittsDatuAm'] = datetime.strptime(
                                                nami_mitglied['austrittsDatum'],
                                                '%Y-%m-%d %H:%M:%S'
                                            )
        nami_mitglied['eintrittsdatum'] = datetime.strptime(
                                            nami_mitglied['eintrittsdatum'],
                                            '%Y-%m-%d %H:%M:%S'
                                        )

        return nami_mitglied
