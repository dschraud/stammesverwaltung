from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.functional import cached_property

from datetime import date, timedelta

from authentication.models import SVUser

# Create your models here.

FUNCTIONS = (
        (0, _('Member')),
        (1, _('Leader')),
        (2, _('Staff')),
        )

GENDERS = (
        ('f', _('female')),
        ('m', _('male')),
        )


class SEPAMandate(models.Model):
    reference = models.CharField(_('mandate reference'), max_length=10, primary_key=True)
    depositor = models.CharField(_('depositor'), max_length=50)
    iban = models.CharField('IBAN', max_length=34)
    bic = models.CharField('BIC', max_length=11)
    bank = models.CharField(_('bank'), max_length=30)
    date = models.DateField(_('date'))

    def __str__(self):
        return '%s (%s)' % (self.depositor, self.reference)

    class Meta:
        verbose_name = _('SEPA mandate')
        verbose_name_plural = _('SEPA mandates')
        ordering = ['depositor', 'reference']


class Section(models.Model):
    name = models.CharField(max_length=15)
    min_age = models.IntegerField(_('minimum age'))
    max_age = models.IntegerField(_('maximum age'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Section')
        verbose_name_plural = _('Sections')
        ordering = ['min_age']


class Member(models.Model):
    nr = models.IntegerField(_('no.'), primary_key=True)
    first_name = models.CharField(_('first name'), max_length=50)
    last_name = models.CharField(_('last name'), max_length=50)
    section = models.ForeignKey(
            Section,
            on_delete=models.PROTECT,
            related_name='members',
            verbose_name=_('section')
            )
    function = models.IntegerField(_('function'), choices=FUNCTIONS)
    nami_nr = models.IntegerField(_('NaMi number'))
    fee = models.DecimalField(_('membership fee'), max_digits=5, decimal_places=2)
    mandate_membership_fee = models.ForeignKey(
            SEPAMandate,
            on_delete=models.SET_NULL,
            related_name='membership_fee',
            verbose_name=_('membership fee mandate'),
            null=True,
            blank=True
            )
    mandate_participation_fee = models.ForeignKey(
            SEPAMandate,
            on_delete=models.SET_NULL,
            related_name='participation_fee',
            verbose_name=_('participation fee mandate'),
            null=True,
            blank=True
            )
    street = models.CharField(_('street'), max_length=50)
    zip = models.DecimalField(_('zip'), max_digits=5, decimal_places=0)
    town = models.CharField(_('town'), max_length=50)
    dob = models.DateField(_('date of birth'))
    gender = models.CharField(_('gender'), max_length=1, choices=GENDERS)
    landline = models.CharField(_('landline number'), max_length=20, null=True, blank=True)
    mobile = models.CharField(_('mobile number'), max_length=20, null=True, blank=True)
    email = models.EmailField(_('email address'), null=True, blank=True)
    juleica_nr = models.CharField(_('Juleica number'), max_length=11, null=True, blank=True)
    photo_rights = models.BooleanField(_('photo rights'), default=False)
    user = models.ForeignKey(
            SVUser,
            on_delete=models.SET_NULL,
            related_name='members',
            verbose_name=_('user'),
            null=True,
            blank=True
            )

    def address(self):
        return '%s, %s %s' % (self.street, self.zip, self.town)

    address.verbose_name = _('address')

    @property
    def age(self):
        return (date.today() - self.dob) // timedelta(days=365.2425)

    @cached_property
    def ideal_section(self):
        section = Section.objects.filter(min_age__lte=self.age, max_age__gte=self.age)
        return section.last()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    class Meta:
        verbose_name = _('Member')
        verbose_name_plural = _('Members')
        ordering = ['first_name', 'last_name']
