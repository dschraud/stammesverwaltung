from django.contrib import admin

from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field

from . import models

# Register your models here.

admin.site.register(models.Section)


class MemberResource(resources.ModelResource):
    nr = Field(attribute='nr', column_name='Mit-Nr.')
    first_name = Field(attribute='first_name', column_name='Vorname')
    last_name = Field(attribute='last_name', column_name='Nachname')
    section = Field(attribute='section', column_name='Stufe')
    function = Field(attribute='function', column_name='Funktion')
    nami_nr = Field(attribute='nami_nr', column_name='NaMi-Nr.')
    photo_rights = Field(attribute='photo_rights', column_name='Bildrechte')
    fee = Field(attribute='fee', column_name='Beitrag')
    mandate_membership_fee = Field(
        attribute='mandate_membership_fee',
        column_name='SEPA-Mandat Mitgliedsbeitrag'
    )
    mandate_participation_fee = Field(
        attribute='mandate_participation_fee',
        column_name='SEPA-Mandat Teilnehmerbeitrag'
    )
    zip = Field(attribute='zip', column_name='PLZ')
    town = Field(attribute='town', column_name='Ort')
    street = Field(attribute='street', column_name='Straße')
    dob = Field(attribute='dob', column_name='Geburtsdatum')
    landline = Field(attribute='landline', column_name='Festnetznr.')
    mobile = Field(attribute='mobile', column_name='Mobilfunknr.')
    email = Field(attribute='email', column_name='E-Mail')
    gender = Field(attribute='gender', column_name='Geschlecht')
    juleica_nr = Field(attribute='juleica_nr', column_name='Juleica-Nr.')

    def dehydrate_function(self, member):
        return member.get_function_display()

    def dehydrate_photo_rights(self, member):
        return 'Ja' if member.photo_rights else 'Nein'

    def dehydrate_dob(self, member):
        return member.dob.strftime('%d.%m.%Y')

    def dehydrate_gender(self, member):
        return 'männlich' if member.gender == 'm' else 'weiblich'

    class Meta:
        model = models.Member
        fields = (
            'nr',
            'first_name',
            'last_name',
            'section',
            'function',
            'nami_nr',
            'photo_rights',
            'fee',
            'mandate_membership_fee',
            'mandate_participation_fee',
            'zip',
            'town',
            'street',
            'dob',
            'landline',
            'mobile',
            'email',
            'gender',
            'juleica_nr'
        )
        import_id_fields = ('nr',)


@admin.register(models.Member)
class MemberAdmin(ImportExportModelAdmin):
    resource_class = MemberResource


class SEPAMandateResource(resources.ModelResource):
    class Meta:
        model = models.SEPAMandate
        import_id_fields = ('reference',)


@admin.register(models.SEPAMandate)
class SEPAMandateAdmin(ImportExportModelAdmin):
    resource_class = SEPAMandateResource


class DebitResource(resources.ModelResource):
    class Meta:
        model = models.Member
        fields = (
            'nr',
            'first_name',
            'last_name',
            'section',
            'function',
            'nami_nr',
            'photo_rights',
            'fee',
            'mandate_membership_fee',
            'mandate_participation_fee',
            'zip',
            'town',
            'street',
            'dob',
            'landline',
            'mobile',
            'email',
            'gender',
            'juleica_nr'
        )
