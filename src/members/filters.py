from django_filters import FilterSet, CharFilter
from django.db.models import Q

from . import models


class MemberFilter(FilterSet):
    name = CharFilter(method='filter_name')

    def filter_name(self, queryset, name, value):
        return queryset.filter(Q(first_name__icontains=value) | Q(last_name__icontains=value))

    class Meta:
        model = models.Member
        fields = []


class MandateFilter(FilterSet):
    name = CharFilter(method='filter_name')

    def filter_name(self, queryset, name, value):
        return queryset.filter(
            Q(depositor__icontains=value) |
            Q(iban__icontains=value) |
            Q(reference=value)
        )

    class Meta:
        model = models.SEPAMandate
        fields = []
