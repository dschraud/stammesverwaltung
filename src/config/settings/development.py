# flake8: noqa
from .base import *
import os

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@on(p5jqkbe(6y(u6503-q94w9ywfmnqoln2#q8-^-j7n&#wn0'

INSTALLED_APPS += [
    'bandit',
    'debug_toolbar',
]

# Internal IPs for debug toolbar
INTERNAL_IPS = {'127.0.0.1'}

# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'incremental': True,
    'root': {
        'level': 'DEBUG',
    },
}

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)

MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
