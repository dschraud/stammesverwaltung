# flake8: noqa

from .base import *
import os

SECRET_KEY = os.environ.get('SECRET_KEY')

ADMINS = [(os.environ.get('ADMIN_NAME'), os.environ.get('ADMIN_EMAIL'))]

ALLOWED_HOSTS = [os.environ.get('DOMAIN')]

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
    }
}

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = os.environ.get('SMTP_HOST'),
EMAIL_PORT = os.environ.get('SMTP_PORT'),
EMAIL_HOST_USER = os.environ.get('SMTP_USER'),
EMAIL_HOST_PASSWORD = os.environ.get('SMTP_PASSWORD'),
EMAIL_USE_TLS = True
