# flake8: noqa

from .base import *
import os

DEBUG = True

SECRET_KEY = os.environ.get('SECRET_KEY')

ALLOWED_HOSTS = [os.environ.get('DOMAIN')]

INSTALLED_APPS += [
    'bandit',
    'debug_toolbar',
]

# Internal IPs for debug toolbar
INTERNAL_IPS = {'127.0.0.1'}

# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'incremental': True,
    'root': {
        'level': 'DEBUG',
    },
}

MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': os.environ.get('DB_PORT'),
    }
}

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

EMAIL_BACKEND = 'bandit.backends.smtp.HijackSMTPBackend'
BANDIT_EMAIL = os.environ.get('ADMIN_EMAIL')
EMAIL_HOST = os.environ.get('SMTP_HOST'),
EMAIL_PORT = os.environ.get('SMTP_PORT'),
EMAIL_HOST_USER = os.environ.get('SMTP_USER'),
EMAIL_HOST_PASSWORD = os.environ.get('SMTP_PASSWORD'),
EMAIL_USE_TLS = True
