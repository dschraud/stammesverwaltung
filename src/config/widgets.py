from django import forms
from django_addanother.widgets import AddAnotherWidgetWrapper
from django.urls import reverse_lazy
from django_select2.forms import Select2Widget, Select2MultipleWidget


class AccountWidget(AddAnotherWidgetWrapper):
    template = 'finances/account_widget.html'

    def __init__(self, contra_account=False):
        if contra_account:
            super().__init__(
                forms.Select(attrs={'class': 'form-control custom-select'}),
                reverse_lazy('finances:create_contra_account')
            )
        else:
            super().__init__(
                forms.Select(attrs={'class': 'form-control custom-select'}),
                reverse_lazy('finances:create_account')
            )


class EURWidget(forms.NumberInput):
    template_name = 'finances/eur_widget.html'


class FileWidget(forms.ClearableFileInput):
    template_name = 'finances/file_widget.html'


class KmWidget(forms.NumberInput):
    template_name = 'finances/km_widget.html'


class BootstrapSelect2Widget(Select2Widget):
    def build_attrs(self, *args, **kwargs):
        attrs = super().build_attrs(*args, **kwargs)
        attrs.setdefault('data-theme', 'bootstrap4')
        attrs['data-allow-clear'] = 'false'
        return attrs

    @property
    def media(self):
        return super().media + forms.Media(css={
            'screen': ('/static/css/select2-bootstrap4-theme.css',)
        })


class BootstrapSelect2MultipleWidget(Select2MultipleWidget):
    def build_attrs(self, *args, **kwargs):
        attrs = super().build_attrs(*args, **kwargs)
        attrs.setdefault('data-theme', 'bootstrap4')
        attrs['data-allow-clear'] = 'false'
        return attrs

    @property
    def media(self):
        return super().media + forms.Media(css={
            'screen': ('/static/css/select2-bootstrap4-theme.css',)
        })
