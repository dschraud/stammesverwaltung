from django_filters import FilterSet, CharFilter
from django.db.models import Q

from . import models


class UserFilter(FilterSet):
    name_email = CharFilter(method='filter_ne')

    def filter_ne(self, queryset, name, value):
        return queryset.filter(
            Q(first_name__icontains=value) |
            Q(last_name__icontains=value) |
            Q(email__icontains=value)
        )

    class Meta:
        model = models.SVUser
        fields = []
