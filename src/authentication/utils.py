from django.core.mail import EmailMultiAlternatives
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from django.template import loader
from rolepermissions.checkers import has_role
from datetime import datetime

from . import roles, models


class RegistrationRequestBoardEmail(EmailMultiAlternatives):
    template_name = 'authentication/rr_board_email.txt'
    subject_template_name = 'authentication/rr_board_email_subject.txt'
    html_template_name = 'authentication/rr_board_email.html'
    from_email = 'Stammesverwaltung DPSG St. Vitus Hirschaid <sv@pfadfinder-hirschaid.de>'

    def __init__(self, board, domain, ruser):
        context = {
            'first_name': board.first_name,
            'domain': domain,
            'protocol': 'https',
            'user': ruser,
            'year': datetime.today().year,
            'debug': settings.DEBUG,
        }
        subject = loader.render_to_string(self.subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(self.template_name, context)
        html = loader.render_to_string(self.html_template_name, context)

        super().__init__(
            subject=subject,
            body=body,
            from_email=self.from_email,
            to=[board.email],
        )
        self.attach_alternative(html, 'text/html')


def notify_board_registration_request(request, ruser):
    domain = get_current_site(request).domain
    board = [u for u in models.SVUser.objects.all() if has_role(
        u, [roles.Board]
    ) and models.BoardEmailSettings.objects.get_or_create(user=u)[0].rr_created]

    for b in board:
        RegistrationRequestBoardEmail(b, domain, ruser).send()
