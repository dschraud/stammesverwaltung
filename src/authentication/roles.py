from rolepermissions.roles import AbstractUserRole
from django.utils.translation import gettext_lazy as _


class Board(AbstractUserRole):
    name = _('Board')


class Cashier(AbstractUserRole):
    name = _('Cashier')


class Secretary(AbstractUserRole):
    name = _('Secretary')


class StockWarden(AbstractUserRole):
    name = _('Stock warden')


class Leader(AbstractUserRole):
    name = _('Leader')


ROLES = (
    (0, _('Board')),
    (1, _('Cashier')),
    (2, _('Secretary')),
    (3, _('Stock warden')),
    (4, _('Leader'))
)

ROLES_DICT = {
    '0': Board,
    '1': Cashier,
    '2': Secretary,
    '3': StockWarden,
    '4': Leader
}
