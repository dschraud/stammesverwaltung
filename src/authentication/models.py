from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils.translation import gettext_lazy as _

# Create your models here.

ROLES = (
        (0, _('Parent')),
        (1, _('Full-aged member')),
        (2, _('Leader')),
        (3, _('Board')),
        )


class SVUserManager(BaseUserManager):
    def _create_user(self, email, password, first_name, last_name, **other_fields):
        if not email:
            raise ValueError(_('The email adress has to be set!'))
        email = self.normalize_email(email)
        user = self.model(email=email, first_name=first_name, last_name=last_name, **other_fields)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email, password, first_name, last_name, **other_fields):
        other_fields.setdefault('is_active', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, first_name, last_name, **other_fields)

    def create_superuser(self, email, password, first_name, last_name, **other_fields):
        other_fields.setdefault('is_active', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_active') is not True:
            raise ValueError(_('Superuser must have is_active=True.'))
        if other_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))

        return self._create_user(email, password, first_name, last_name, **other_fields)


class SVUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), max_length=50, unique=True)
    first_name = models.CharField(_('first name'), max_length=50)
    last_name = models.CharField(_('last name'), max_length=50)
    is_active = models.BooleanField(_('active'), default=False)
    is_superuser = models.BooleanField(_('superuser'), default=False)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = SVUserManager()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    @property
    def is_staff(self):
        return self.is_superuser

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')


class RegistrationRequest(models.Model):
    user = models.OneToOneField(SVUser, verbose_name=_('user'), on_delete=models.CASCADE)
    role = models.IntegerField(_('role'), choices=ROLES)
    members = models.CharField(_('members'), max_length=200)
    email_confirmed = models.BooleanField(_('email confirmed'), default=False)
    date = models.DateTimeField(_('date'), auto_now=True)

    def __str__(self):
        return '%s (%s)' % (self.user.get_full_name(), self.get_role_display())

    class Meta:
        verbose_name = _('registration request')
        verbose_name_plural = _('registration requests')
        ordering = ['date']


class BankDetails(models.Model):
    depositor = models.CharField(_('depositor'), max_length=50)
    iban = models.CharField('IBAN', max_length=34)
    bic = models.CharField('BIC', max_length=11)
    bank = models.CharField(_('bank'), max_length=30)
    user = models.ForeignKey(SVUser, on_delete=models.CASCADE, verbose_name=_('user'))

    def __str__(self):
        return '%s (%s)' % (self.depositor, self.iban)

    class Meta:
        verbose_name = _('Bank details')
        verbose_name_plural = _('Bank details')
        ordering = ['depositor', 'iban']


class MemberEmailSettings(models.Model):
    user = models.OneToOneField(SVUser, verbose_name=_('user'), on_delete=models.CASCADE)
    event_created = models.BooleanField(
        _('A new event has been created'),
        default=True
    )
    member_created = models.BooleanField(
        _('A member has been assigned to your user'),
        default=True
    )
    member_changed = models.BooleanField(
        _('The data about one of your member has been changed'),
        default=False
    )
    mandate_changed = models.BooleanField(
        _('The data about one of your SEPA mandates has been changed'),
        default=False
    )
    debit_collected = models.BooleanField(
        _('Debits will be withdrawn'),
        default=True
    )
    order_completed = models.BooleanField(
        _('An order has been completed'),
        default=True
    )

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Member email settings')
        verbose_name_plural = _('Member email settings')
        ordering = ['user']


class LeaderEmailSettings(models.Model):
    user = models.OneToOneField(SVUser, verbose_name=_('user'), on_delete=models.CASCADE)
    event_changed = models.BooleanField(
        _('A event has changed'),
        default=False
    )
    participation_changed = models.BooleanField(
        _('A member has enrolled or disenrolled for an event'),
        default=False
    )
    group_changed = models.BooleanField(
        _('A member of your group has changed or has been added/removed to/from it'),
        default=True
    )
    settlement_completed = models.BooleanField(
        _('A settlement of yours has been completed'),
        default=True
    )

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Leader email settings')
        verbose_name_plural = _('Leader email settings')
        ordering = ['user']


class CashierEmailSettings(models.Model):
    user = models.OneToOneField(SVUser, verbose_name=_('user'), on_delete=models.CASCADE)
    debit_available = models.BooleanField(
        _('Debits for a month are available'),
        default=True
    )
    settlement_created = models.BooleanField(
        _('A settlement has been created'),
        default=True
    )

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Cashier email settings')
        verbose_name_plural = _('Cashier email settings')
        ordering = ['user']


class StockEmailSettings(models.Model):
    user = models.OneToOneField(SVUser, verbose_name=_('user'), on_delete=models.CASCADE)
    order_created = models.BooleanField(
        _('An order has been created'),
        default=True
    )
    reservation_created = models.BooleanField(
        _('A reservation has been created'),
        default=True
    )

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Stock email settings')
        verbose_name_plural = _('Stock email settings')
        ordering = ['user']


class BoardEmailSettings(models.Model):
    user = models.OneToOneField(SVUser, verbose_name=_('user'), on_delete=models.CASCADE)
    rr_created = models.BooleanField(
        _('A user registration has been requested'),
        default=True
    )

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = _('Board email settings')
        verbose_name_plural = _('Board email settings')
        ordering = ['user']
