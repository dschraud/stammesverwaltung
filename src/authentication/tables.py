from django_tables2 import Table, Column
from django_tables2.columns import TemplateColumn
from django.utils.translation import gettext_lazy as _
from rolepermissions.roles import get_user_roles

from . import models
from members.models import Member, Section


class RegistrationRequestTable(Table):
    delete = TemplateColumn(
        '<a href="{%% url "auth:delete_rr" record.id %%}" '
        'class="btn btn-danger btn-sm btn-esm">%s</a>' % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )
    activate = TemplateColumn(
        '{%% if record.email_confirmed %%}'
        '<a href="{%% url "auth:confirm_rr" record.id %%}" '
        'class="btn btn-success btn-sm btn-esm">%s</a>'
        '{%% else %%}'
        '<a href="{%% url "auth:confirm_rr" record.id %%}" '
        'class="btn btn-success btn-sm btn-esm disabled">%s</a>'
        '{%% endif %%}' % (_('Activate'), _('Activate')),
        verbose_name=_('Activate'),
        orderable=False
    )
    email = Column(verbose_name=_('Email address'), accessor='user.email')
    name = Column(verbose_name=_('Name'), accessor='user')

    def render_members(self, value):
        return value.replace('\r\n', ', ')

    class Meta:
        model = models.RegistrationRequest
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'date',
            'role',
            'members',
            'email_confirmed',
        )
        sequence = (
            'date',
            'email',
            'name',
            'members',
            'role',
            'email_confirmed',
            'activate',
            'delete'
        )
        attrs = {'class': 'table table-striped table-sm'}


class BankDetailsTable(Table):
    edit = TemplateColumn(
        ('<a href="{%% url "auth:edit_bankdetails" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "auth:delete_bankdetails" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    class Meta:
        model = models.BankDetails
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'depositor',
            'iban',
            'bic',
            'bank',
        )
        sequence = (
            'depositor',
            'iban',
            'bic',
            'bank',
            'edit',
            'delete',
        )
        attrs = {'class': 'table table-striped table-sm'}


class SVUserTable(Table):
    edit = TemplateColumn(
        ('<a href="{%% url "auth:edit_user" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "auth:delete_user" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )
    name = Column(verbose_name=_('Name'), accessor='get_full_name')
    members = Column(verbose_name=_('Members'))
    roles = Column(verbose_name=_('Roles'), empty_values=())

    def __init__(self, queryset, privileged, *args,  **kwargs):
        super().__init__(queryset, *args, **kwargs)
        if not privileged:
            self.exclude = self.exclude + ('roles', 'is_superuser')

    def render_members(self, record):
        return ", ".join(str(m) for m in Member.objects.filter(user=record))

    def render_roles(self, record):
        return ", ".join(str(r.name) for r in get_user_roles(record))

    class Meta:
        model = models.SVUser
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'email',
            'is_superuser'
        )
        sequence = (
            'email',
            'name',
            'members',
            'roles',
            'is_superuser',
            'edit',
            'delete'
        )
        attrs = {'class': 'table table-striped table-sm'}


class SectionTable(Table):
    """
    Table for viewing and editing sections.
    """

    edit = TemplateColumn(
        ('<a href="{%% url "members:edit_section" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "members:delete_section" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    class Meta:
        model = Section
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'name',
            'min_age',
            'max_age'
        )
        sequence = (
            'name',
            'min_age',
            'max_age',
            'edit',
            'delete'
        )
        attrs = {'class': 'table table-striped table-sm'}
