from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from django.core import mail
from django.forms import formset_factory
from django.utils.translation import gettext_lazy as _
from rolepermissions.roles import assign_role

import re

from . import forms
from . import models
from . import roles
from members.models import Member, Section

# Create your tests here.


class FormTests(TestCase):
    def test_user_creation_form_invalid(self):
        form = forms.UserCreationForm()
        self.assertFalse(form.is_valid())

    def test_user_creation_form_passwords_not_matching(self):
        form = forms.UserCreationForm(data={
            'email': 'test@test.com',
            'first_name': 'Te',
            'last_name': 'St',
            'password1': '1234asdfg',
            'password2': '1234asdfh'
            })
        self.assertFalse(form.is_valid())

    def test_user_creation_form(self):
        form = forms.UserCreationForm(data={
            'email': 'test@test.com',
            'first_name': 'Te',
            'last_name': 'St',
            'password1': '1234asdfg',
            'password2': '1234asdfg'
            })
        self.assertTrue(form.is_valid())

        user = form.save()

        self.assertEqual(user.email, 'test@test.com')

    def test_user_registration_form_invalid(self):
        form = forms.UserRegistrationForm()
        self.assertFalse(form.is_valid())

    def test_user_registration_form(self):
        form = forms.UserRegistrationForm(data={
            'email': 'test@test.com',
            'first_name': 'Te',
            'last_name': 'St',
            'password1': '1234asdfg',
            'password2': '1234asdfg',
            'role': '0',
            'members': 'St Te',
            })
        self.assertTrue(form.is_valid())

        form.save(domain_override='test.com')
        users = models.SVUser.objects.all()
        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(len(users), 1)
        self.assertEqual(len(rrs), 1)
        self.assertEqual(len(mail.outbox), 1)

        user = models.SVUser.objects.get(pk=1)
        self.assertFalse(user.is_active)
        self.assertFalse(user.is_superuser)

    def test_user_registration_form_html(self):
        form = forms.UserRegistrationForm(data={
            'email': 'test@test.com',
            'first_name': 'Te',
            'last_name': 'St',
            'password1': '1234asdfg',
            'password2': '1234asdfg',
            'role': '0',
            'members': 'St Te',
            })
        self.assertTrue(form.is_valid())

        form.save(
                domain_override='test.com',
                html_email_template_name='authentication/register_email.html'
                )
        users = models.SVUser.objects.all()
        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(len(users), 1)
        self.assertEqual(len(rrs), 1)
        self.assertEqual(len(mail.outbox), 1)

        user = models.SVUser.objects.get(pk=1)
        self.assertFalse(user.is_active)
        self.assertFalse(user.is_superuser)

    def test_user_change_form_invalid(self):
        form = forms.UserChangeForm()
        self.assertFalse(form.is_valid())

    def test_user_change_form(self):
        user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        form = forms.UserChangeForm(data={
            'email': 'test@test.com',
            'first_name': 'Te',
            'last_name': 'Ts',
            'password1': '1234asdfg',
            'is_active': False,
            'is_superuser': False,
            }, instance=user)
        self.assertTrue(form.is_valid())

        form.save()
        self.assertNotEqual(user.last_name, 'St')

    def test_user_form(self):
        user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        form = forms.UserForm(data={
            'email': 'tset@test.com',
            'first_name': 'Ab',
            'last_name': 'Cd',
            'roles': [1, 3],
        }, instance=user)
        self.assertTrue(form.is_valid())

        form.save()
        self.assertNotEqual(user.email, 'test@test.com')

    def test_member_user_form(self):
        section = Section.objects.create(
            name='Beever',
            min_age=10,
            max_age=14
        )
        Member.objects.create(
            nr=1,
            first_name='Max',
            last_name='Mustermann',
            section=section,
            function=0,
            nami_nr=100001,
            fee=15.00,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2000-01-01',
            gender='m'
        )
        form = forms.MemberUserForm(member_name='Max Mustermann', data={'member': '1'})
        self.assertTrue(form.is_valid())

    def test_member_user_form_set(self):
        section = Section.objects.create(
            name='Beever',
            min_age=10,
            max_age=14
        )
        Member.objects.create(
            nr=1,
            first_name='Max',
            last_name='Mustermann',
            section=section,
            function=0,
            nami_nr=100001,
            fee=15.00,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2000-01-01',
            gender='m'
        )
        members = ['Max Mustermann', 'Max Mustermann']
        MemberUserFormSet = formset_factory(
                forms.MemberUserForm,
                formset=forms.BaseMemberUserFormSet,
                extra=len(members)
        )
        formset = MemberUserFormSet(members=members, data={
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '2',
            'form-MAX_NUM_FORMS': '1000',
            'form-MIN_NUM_FORMS': '0',
            'form-0-member': None,
            'form-1-member': '1',
        })
        self.assertTrue(formset.is_valid())

    def test_members_email_form(self):
        form = forms.MembersEmailForm(data={
            'event_created': True,
            'member_created': False,
            'member_changed': False,
            'mandate_changed': True,
            'debit_collected': False,
            'order_completed': True,
        })
        self.assertTrue(form.is_valid())

    def test_leader_email_form(self):
        form = forms.MembersEmailForm(data={
            'event_changed': False,
            'participation_changed': True,
            'group_changed': True,
            'settlement_completed': False,
        })
        self.assertTrue(form.is_valid())

    def test_cashier_email_form(self):
        form = forms.MembersEmailForm(data={
            'debit_available': False,
            'settlement_created': True,
        })
        self.assertTrue(form.is_valid())

    def test_stock_email_form(self):
        form = forms.MembersEmailForm(data={
            'order_created': True,
            'reservation_created': False,
        })
        self.assertTrue(form.is_valid())


class ViewTests(TestCase):
    def setUp(self):
        self.client = Client()

    def create_user(self):
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()

    def create_board_user(self):
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()
        assign_role(self.user, roles.Board)

    def create_not_board_user(self):
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()
        assign_role(self.user, roles.Cashier)
        assign_role(self.user, roles.Leader)
        assign_role(self.user, roles.Secretary)
        assign_role(self.user, roles.StockWarden)

    def create_leader_user(self):
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()
        assign_role(self.user, roles.Leader)

    def create_cashier_user(self):
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()
        assign_role(self.user, roles.Cashier)

    def create_stock_warden_user(self):
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()
        assign_role(self.user, roles.StockWarden)

    def test_home_login_required(self):
        response = self.client.get(reverse('home'))
        self.assertNotEqual(response.status_code, 200)

    def test_home_with_login(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        response = self.client.get(reverse('home'), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_login_view(self):
        self.create_user()
        response = self.client.post(reverse('auth:login'), {
            'username': 'test@test.com',
            'password': '1234asdfg',
            })
        self.assertEqual(response.status_code, 302)

    def test_login_view_failure(self):
        response = self.client.post(reverse('auth:login'), {
            'username': 'test@test.com',
            'password': '1234asdfg',
            })
        self.assertEqual(response.status_code, 200)

    def test_logout_view(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        self.client.get(reverse('auth:logout'))
        response = self.client.get(reverse('home'))
        self.assertNotEqual(response.status_code, 200)

    def test_password_change_view(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        self.client.post(reverse('auth:password-change'), {
            'old_password': '1234asdfg',
            'new_password1': '4321gfdsa',
            'new_password2': '4321gfdsa',
            })
        self.client.logout()
        self.assertTrue(self.client.login(email='test@test.com', password='4321gfdsa'))

    def test_password_change_view_failure(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        self.client.post(reverse('auth:password-change'), {
            'old_password': 'falsepassword',
            'new_password1': '4321gfdsa',
            'new_password2': '4321gfdsa',
            })
        self.client.logout()
        self.assertFalse(self.client.login(email='test@test.com', password='4321gfdsa'))

    def test_password_change_view_password_required(self):
        response = self.client.get(reverse('auth:password-change'))
        self.assertNotEqual(response.status_code, 200)

    def test_password_reset_view(self):
        self.create_user()
        self.client.post(reverse('auth:password-reset'), {'email': 'test@test.com'})
        self.assertEqual(len(mail.outbox), 1)

    def test_password_reset_done_view(self):
        self.create_user()
        self.client.post(reverse('auth:password-reset'), {'email': 'test@test.com'})
        url = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2})|/)+', mail.outbox[0].body)[0]
        res = self.client.get(url)
        self.client.post(res.url, {
            'new_password1': '4321gfdsa',
            'new_password2': '4321gfdsa'
            }, follow=True)
        login = self.client.login(email='test@test.com', password='4321gfdsa')
        self.assertTrue(login)

    def test_user_registration_view(self):
        self.client.post(reverse('auth:register'), {
            'first_name': 'Te',
            'last_name': 'St',
            'email': 'test@test.com',
            'password1': '1234asdfg',
            'password2': '1234asdfg',
            'role': '0',
            'members': 'St Te',
            })

        users = models.SVUser.objects.all()
        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(len(users), 1)
        self.assertEqual(len(rrs), 1)
        self.assertEqual(len(mail.outbox), 1)

        user = models.SVUser.objects.get(pk=1)
        self.assertFalse(user.is_active)
        self.assertFalse(user.is_superuser)

        rr = models.RegistrationRequest.objects.get(pk=1)
        self.assertEqual(rr.user, user)
        self.assertFalse(rr.email_confirmed)

    def test_user_registration_view_failure(self):
        self.create_user()
        self.client.post(reverse('auth:register'), {
            'first_name': 'Test2',
            'last_name': 'Test3',
            'email': 'test@test.com',
            'password1': '1234asdfg',
            'password2': '1234asdfg',
            'role': '0',
            'members': 'St Te',
            })

        users = models.SVUser.objects.all()
        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(len(users), 1)
        self.assertEqual(len(rrs), 0)
        self.assertEqual(len(mail.outbox), 0)

        user = models.SVUser.objects.get(pk=1)
        self.assertEqual(user.first_name, 'Te')

    def test_user_registration_confirm_view(self):
        self.client.post(reverse('auth:register'), {
            'first_name': 'Test2',
            'last_name': 'Test3',
            'email': 'test@test.com',
            'password1': '1234asdfg',
            'password2': '1234asdfg',
            'role': '0',
            'members': 'St Te',
            })
        url = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2})|/)+', mail.outbox[0].body)[0]
        self.client.get(url)

        rr = models.RegistrationRequest.objects.get(pk=1)
        self.assertTrue(rr.email_confirmed)

    def test_user_registration_confirm_view_failure1(self):
        self.client.post(reverse('auth:register'), {
            'first_name': 'Test2',
            'last_name': 'Test3',
            'email': 'test@test.com',
            'password1': '1234asdfg',
            'password2': '1234asdfg',
            'role': '0',
            'members': 'St Te',
            })
        url = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2})|/)+', mail.outbox[0].body)[0]
        url = url[:-4] + '$' + url[-4:]
        self.client.get(url)

        rr = models.RegistrationRequest.objects.get(pk=1)
        self.assertFalse(rr.email_confirmed)

    def test_user_registration_confirm_view_failure2(self):
        self.create_user()
        self.client.post(reverse('auth:register'), {
            'first_name': 'Test2',
            'last_name': 'Test3',
            'email': 'test2@test.com',
            'password1': '1234asdfg',
            'password2': '1234asdfg',
            'role': '0',
            'members': 'St Te',
            })

        user1 = models.SVUser.objects.get(pk=1)
        rr = models.RegistrationRequest.objects.get(pk=1)
        rr.user = user1
        rr.save()
        user2 = models.SVUser.objects.get(pk=2)
        user2.delete()

        url = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2})|/)+', mail.outbox[0].body)[0]
        self.client.get(url)

        self.assertFalse(rr.email_confirmed)

    def test_adminstration_view_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        response = self.client.get(reverse('auth:administration'))

        self.assertEqual(response.status_code, 403)

    def test_adminstration_view(self):
        self.create_board_user()
        user2 = models.SVUser.objects.create_user(
                email='tesdt@test.com',
                first_name='Tse',
                last_name='Sat',
                password='12334asdfg'
                )
        user2.save()
        self.client.login(email='test@test.com', password='1234asdfg')
        models.RegistrationRequest.objects.create(
            user=self.user,
            role=2,
            members='Max Mustermann\r\mMaria Musermann'
        )
        response = self.client.get(reverse('auth:administration'))

        self.assertContains(response, 'Maria', status_code=200)

    def test_delete_rr_view_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        rr = models.RegistrationRequest.objects.create(
            user=self.user,
            role=2,
            members='Max Mustermann\r\mMaria Musermann'
        )
        response = self.client.post(reverse('auth:delete_rr', kwargs={'pk': rr.pk}))

        self.assertEqual(response.status_code, 403)

    def test_delete_rr_view(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        rr = models.RegistrationRequest.objects.create(
            user=self.user,
            role=2,
            members='Max Mustermann\r\mMaria Musermann'
        )
        self.client.post(reverse('auth:delete_rr', kwargs={'pk': rr.pk}))

        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(0, len(rrs))

    def test_confirm_rr_view_get_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user = models.SVUser.objects.create_user(
                email='max@mustermann.com',
                first_name='Ab',
                last_name='Cd',
                password='1234asdfg'
        )
        rr = models.RegistrationRequest.objects.create(
            user=user,
            role=2,
            members='Max Mustermann\r\nMaria Musermann',
            email_confirmed=True
        )
        response = self.client.get(reverse('auth:confirm_rr', kwargs={'pk': rr.pk}))

        self.assertEqual(response.status_code, 403)

    def test_confirm_rr_view_get(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user = models.SVUser.objects.create_user(
                email='max@mustermann.com',
                first_name='Ab',
                last_name='Cd',
                password='1234asdfg'
        )
        rr = models.RegistrationRequest.objects.create(
            user=user,
            role=2,
            members='Max Mustermann\r\nMaria Musermann',
            email_confirmed=True
        )
        response = self.client.get(reverse('auth:confirm_rr', kwargs={'pk': rr.pk}))

        self.assertContains(response, 'Maria', status_code=200)

    def test_confirm_rr_view_post_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user = models.SVUser.objects.create_user(
                email='max@mustermann.com',
                first_name='Ab',
                last_name='Cd',
                password='1234asdfg'
        )
        rr = models.RegistrationRequest.objects.create(
            user=user,
            role=2,
            members='Max Mustermann\r\nMaria Musermann',
            email_confirmed=True
        )
        response = self.client.post(reverse('auth:confirm_rr', kwargs={'pk': rr.pk}), {
            'email': 'tset@test.com',
            'first_name': 'Ab',
            'last_name': 'Cd',
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '2',
            'form-MAX_NUM_FORMS': '1000',
            'form-MIN_NUM_FORMS': '0',
            'form-1-member': '1',
        })

        self.assertEqual(response.status_code, 403)

    def test_confirm_rr_view_post(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        section = Section.objects.create(
            name='Beever',
            min_age=10,
            max_age=14
        )
        member = Member.objects.create(
            nr=1,
            first_name='Max',
            last_name='Mustermann',
            section=section,
            function=0,
            nami_nr=100001,
            fee=15.00,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2000-01-01',
            gender='m'
        )
        user = models.SVUser.objects.create_user(
                email='max@mustermann.com',
                first_name='Ab',
                last_name='Cd',
                password='1234asdfg'
        )
        rr = models.RegistrationRequest.objects.create(
            user=user,
            role=2,
            members='Max Mustermann\r\nMaria Musermann',
            email_confirmed=True
        )
        self.client.post(reverse('auth:confirm_rr', kwargs={'pk': rr.pk}), {
            'email': 'tset@test.com',
            'first_name': 'Ab',
            'last_name': 'Cd',
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '2',
            'form-MAX_NUM_FORMS': '1000',
            'form-MIN_NUM_FORMS': '0',
            'form-1-member': '1',
        })

        user.refresh_from_db()
        member.refresh_from_db()
        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(0, len(rrs))
        self.assertTrue(user.is_active)
        self.assertEqual(user, member.user)

    def test_confirm_rr_view_post_failure1(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        section = Section.objects.create(
            name='Beever',
            min_age=10,
            max_age=14
        )
        member = Member.objects.create(
            nr=1,
            first_name='Max',
            last_name='Mustermann',
            section=section,
            function=0,
            nami_nr=100001,
            fee=15.00,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2000-01-01',
            gender='m'
        )
        user = models.SVUser.objects.create_user(
                email='max@mustermann.com',
                first_name='Ab',
                last_name='Cd',
                password='1234asdfg'
        )
        rr = models.RegistrationRequest.objects.create(
            user=user,
            role=2,
            members='Max Mustermann\r\nMaria Musermann',
        )
        self.client.post(reverse('auth:confirm_rr', kwargs={'pk': rr.pk}), {
            'email': 'tset@test.com',
            'first_name': 'Ab',
            'last_name': 'Cd',
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '2',
            'form-MAX_NUM_FORMS': '1000',
            'form-MIN_NUM_FORMS': '0',
            'form-1-member': '1',
        })

        user.refresh_from_db()
        member.refresh_from_db()
        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(1, len(rrs))
        self.assertFalse(user.is_active)
        self.assertNotEqual(user, member.user)

    def test_confirm_rr_view_post_failure2(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        section = Section.objects.create(
            name='Beever',
            min_age=10,
            max_age=14
        )
        member = Member.objects.create(
            nr=1,
            first_name='Max',
            last_name='Mustermann',
            section=section,
            function=0,
            nami_nr=100001,
            fee=15.00,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2000-01-01',
            gender='m'
        )
        user = models.SVUser.objects.create_user(
                email='max@mustermann.com',
                first_name='Ab',
                last_name='Cd',
                password='1234asdfg'
        )
        rr = models.RegistrationRequest.objects.create(
            user=user,
            role=2,
            members='Max Mustermann\r\nMaria Musermann',
            email_confirmed=True,
        )
        self.client.post(reverse('auth:confirm_rr', kwargs={'pk': rr.pk}), {
            'email': 'tset',
            'first_name': 'Ab',
            'last_name': 'Cd',
            'form-INITIAL_FORMS': '0',
            'form-TOTAL_FORMS': '2',
            'form-MAX_NUM_FORMS': '1000',
            'form-MIN_NUM_FORMS': '0',
            'form-1-member': '1',
        })

        user.refresh_from_db()
        member.refresh_from_db()
        rrs = models.RegistrationRequest.objects.all()

        self.assertEqual(1, len(rrs))
        self.assertFalse(user.is_active)
        self.assertNotEqual(user, member.user)

    def test_settings_view_get_user(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:settings'))

        self.assertContains(response, _('E-Mail settings'), status_code=200)
        self.assertContains(response, _('A new event has been created'), status_code=200)

        self.assertNotContains(response, _('A event has changed'), status_code=200)
        self.assertNotContains(response, _('Debits for a month are available'), status_code=200)
        self.assertNotContains(response, _('An order has been created'), status_code=200)
        self.assertNotContains(response, _('Bank details'), status_code=200)

    def test_settings_view_get_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user2 = models.SVUser.objects.create_user(
            email='tesat@test.com',
            first_name='Te',
            last_name='St',
            password='1234asdfg'
        )

        models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB34',
            bank='VR Bank Musterhausen',
            user=user2
        )

        response = self.client.get(reverse('auth:settings'))

        self.assertContains(response, _('E-Mail settings'), status_code=200)
        self.assertContains(response, _('A new event has been created'), status_code=200)
        self.assertContains(response, _('A event has changed'), status_code=200)
        self.assertContains(response, _('Bank details'), status_code=200)
        self.assertContains(response, 'GENODEFAB12', status_code=200)

        self.assertNotContains(response, _('Debits for a month are available'), status_code=200)
        self.assertNotContains(response, _('An order has been created'), status_code=200)
        self.assertNotContains(response, 'GENODEFAB34', status_code=200)

    def test_settings_view_get_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user2 = models.SVUser.objects.create_user(
            email='tesat@test.com',
            first_name='Te',
            last_name='St',
            password='1234asdfg'
        )

        models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB34',
            bank='VR Bank Musterhausen',
            user=user2
        )

        response = self.client.get(reverse('auth:settings'))

        self.assertContains(response, _('E-Mail settings'), status_code=200)
        self.assertContains(response, _('A new event has been created'), status_code=200)
        self.assertContains(response, _('Debits for a month are available'), status_code=200)
        self.assertContains(response, _('Bank details'), status_code=200)
        self.assertContains(response, 'GENODEFAB12', status_code=200)

        self.assertNotContains(response, _('A event has changed'), status_code=200)
        self.assertNotContains(response, _('An order has been created'), status_code=200)
        self.assertNotContains(response, 'GENODEFAB34', status_code=200)

    def test_settings_view_get_stock_warden(self):
        self.create_stock_warden_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:settings'))

        self.assertContains(response, _('E-Mail settings'), status_code=200)
        self.assertContains(response, _('A new event has been created'), status_code=200)
        self.assertContains(response, _('An order has been created'), status_code=200)

        self.assertNotContains(response, _('Debits for a month are available'), status_code=200)
        self.assertNotContains(response, _('A event has changed'), status_code=200)
        self.assertNotContains(response, _('Bank details'), status_code=200)

    def test_settings_view_get_board(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user2 = models.SVUser.objects.create_user(
            email='tesat@test.com',
            first_name='Te',
            last_name='St',
            password='1234asdfg'
        )

        models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB34',
            bank='VR Bank Musterhausen',
            user=user2
        )

        response = self.client.get(reverse('auth:settings'))

        self.assertContains(response, _('E-Mail settings'), status_code=200)
        self.assertContains(response, _('A new event has been created'), status_code=200)
        self.assertContains(response, _('An order has been created'), status_code=200)
        self.assertContains(response, _('Debits for a month are available'), status_code=200)
        self.assertContains(response, _('A event has changed'), status_code=200)
        self.assertContains(response, _('Bank details'), status_code=200)
        self.assertContains(response, 'GENODEFAB12', status_code=200)

        self.assertNotContains(response, 'GENODEFAB34', status_code=200)

    def test_settings_view_post_user(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        models.LeaderEmailSettings.objects.create(user=self.user)
        models.CashierEmailSettings.objects.create(user=self.user)
        models.StockEmailSettings.objects.create(user=self.user)

        response = self.client.post(reverse('auth:settings'), {
            'member_created': 'on',
            'member_changed': 'on',
            'event_changed': 'on',
        }, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, 200)

        self.assertTrue(self.user.memberemailsettings.member_created)
        self.assertTrue(self.user.memberemailsettings.member_changed)
        self.assertTrue(self.user.memberemailsettings.debit_collected)
        self.assertTrue(self.user.cashieremailsettings.debit_available)
        self.assertTrue(self.user.stockemailsettings.order_created)

        self.assertFalse(self.user.memberemailsettings.mandate_changed)
        self.assertFalse(self.user.leaderemailsettings.event_changed)

    def test_settings_view_post_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        models.CashierEmailSettings.objects.create(user=self.user)
        models.StockEmailSettings.objects.create(user=self.user)

        response = self.client.post(reverse('auth:settings'), {
            'member_created': 'on',
            'member_changed': 'on',
            'event_changed': 'on',
        }, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, 200)

        self.assertTrue(self.user.memberemailsettings.member_created)
        self.assertTrue(self.user.memberemailsettings.member_changed)
        self.assertTrue(self.user.memberemailsettings.debit_collected)
        self.assertTrue(self.user.cashieremailsettings.debit_available)
        self.assertTrue(self.user.stockemailsettings.order_created)
        self.assertTrue(self.user.leaderemailsettings.event_changed)

        self.assertFalse(self.user.memberemailsettings.mandate_changed)
        self.assertFalse(self.user.leaderemailsettings.group_changed)

    def test_settings_view_post_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        models.LeaderEmailSettings.objects.create(user=self.user)
        models.StockEmailSettings.objects.create(user=self.user)

        response = self.client.post(reverse('auth:settings'), {
            'member_created': 'on',
            'member_changed': 'on',
            'event_changed': 'on',
        }, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, 200)

        self.assertTrue(self.user.memberemailsettings.member_created)
        self.assertTrue(self.user.memberemailsettings.member_changed)
        self.assertTrue(self.user.memberemailsettings.debit_collected)
        self.assertTrue(self.user.stockemailsettings.order_created)

        self.assertFalse(self.user.leaderemailsettings.event_changed)
        self.assertFalse(self.user.cashieremailsettings.debit_available)
        self.assertFalse(self.user.memberemailsettings.mandate_changed)

    def test_settings_view_post_stock_warden(self):
        self.create_stock_warden_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        models.CashierEmailSettings.objects.create(user=self.user)
        models.LeaderEmailSettings.objects.create(user=self.user)

        response = self.client.post(reverse('auth:settings'), {
            'member_created': 'on',
            'member_changed': 'on',
            'event_changed': 'on',
        }, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, 200)

        self.assertTrue(self.user.memberemailsettings.member_created)
        self.assertTrue(self.user.memberemailsettings.member_changed)
        self.assertTrue(self.user.memberemailsettings.debit_collected)
        self.assertTrue(self.user.cashieremailsettings.debit_available)

        self.assertFalse(self.user.stockemailsettings.order_created)
        self.assertFalse(self.user.leaderemailsettings.event_changed)
        self.assertFalse(self.user.memberemailsettings.mandate_changed)

    def test_settings_view_post_board(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(reverse('auth:settings'), {
            'member_created': 'on',
            'member_changed': 'on',
            'event_changed': 'on',
        }, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, 200)

        self.assertTrue(self.user.memberemailsettings.member_created)
        self.assertTrue(self.user.memberemailsettings.member_changed)
        self.assertTrue(self.user.leaderemailsettings.event_changed)
        self.assertTrue(self.user.memberemailsettings.debit_collected)

        self.assertFalse(self.user.cashieremailsettings.debit_available)
        self.assertFalse(self.user.stockemailsettings.order_created)
        self.assertFalse(self.user.memberemailsettings.mandate_changed)

    def test_bank_details_create_view_get_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:create_bankdetails'))

        self.assertEqual(response.status_code, 403)

    def test_bank_details_create_view_post_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(reverse('auth:create_bankdetails'), {
            'depositor': 'Te st',
            'iban': 'DE123456789012345678890',
            'bic': 'GENODEFAB12',
            'bank': 'VR Bank Musterhausen',
        }, follow=True)

        self.assertEqual(response.status_code, 403)
        self.assertEqual(models.BankDetails.objects.all().count(), 0)

    def test_bank_details_create_view_get_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:create_bankdetails'))
        self.assertContains(response, _('Create bank details'), status_code=200)

    def test_bank_details_create_view_get_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:create_bankdetails'))
        self.assertContains(response, _('Create bank details'), status_code=200)

    def test_bank_details_create_view_post_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(reverse('auth:create_bankdetails'), {
            'depositor': 'Te st',
            'iban': 'DE123456789012345678890',
            'bic': 'GENODEFAB12',
            'bank': 'VR Bank Musterhausen',
        }, follow=True)

        self.assertContains(response, 'GENODEFAB12', status_code=200)

    def test_bank_details_create_view_post_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(reverse('auth:create_bankdetails'), {
            'depositor': 'Te st',
            'iban': 'DE123456789012345678890',
            'bic': 'GENODEFAB12',
            'bank': 'VR Bank Musterhausen',
        }, follow=True)

        self.assertContains(response, 'GENODEFAB12', status_code=200)

    def test_bank_details_update_view_get_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.get(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}))

        self.assertEqual(response.status_code, 403)

    def test_bank_details_update_view_get_board_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user2 = models.SVUser.objects.create_user(
            email='tesat@test.com',
            first_name='Te',
            last_name='St',
            password='1234asdfg'
        )

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=user2
        )

        response = self.client.get(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}))

        self.assertEqual(response.status_code, 403)

    def test_bank_details_update_view_post_board_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user2 = models.SVUser.objects.create_user(
            email='tesat@test.com',
            first_name='Te',
            last_name='St',
            password='1234asdfg'
        )

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=user2
        )

        response = self.client.post(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}), {
            'depositor': 'Te st',
            'iban': 'DE123456789012345678890',
            'bic': 'GENODEFAB34',
            'bank': 'VR Bank Musterhausen',
        }, follow=True)
        bd.refresh_from_db()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(bd.bic, 'GENODEFAB12')

    def test_bank_details_update_view_post_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.post(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}), {
            'depositor': 'Te st',
            'iban': 'DE123456789012345678890',
            'bic': 'GENODEFAB34',
            'bank': 'VR Bank Musterhausen',
        }, follow=True)
        bd.refresh_from_db()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(bd.bic, 'GENODEFAB12')

    def test_bank_details_update_view_get_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.get(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}))

        self.assertContains(response, _('Edit bank details'), status_code=200)
        self.assertContains(response, 'GENODEFAB12', status_code=200)

    def test_bank_details_update_view_get_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.get(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}))

        self.assertContains(response, _('Edit bank details'), status_code=200)
        self.assertContains(response, 'GENODEFAB12', status_code=200)

    def test_bank_details_update_view_post_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.post(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}), {
            'depositor': 'Te st',
            'iban': 'DE123456789012345678890',
            'bic': 'GENODEFAB34',
            'bank': 'VR Bank Musterhausen',
        }, follow=True)

        self.assertContains(response, 'GENODEFAB34', status_code=200)

    def test_bank_details_update_view_post_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.post(reverse('auth:edit_bankdetails', kwargs={'pk': bd.pk}), {
            'depositor': 'Te st',
            'iban': 'DE123456789012345678890',
            'bic': 'GENODEFAB34',
            'bank': 'VR Bank Musterhausen',
        }, follow=True)

        self.assertContains(response, 'GENODEFAB34', status_code=200)

    def test_bank_details_delete_view_get_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.get(reverse('auth:delete_bankdetails', kwargs={'pk': bd.pk}))

        self.assertEqual(response.status_code, 403)

    def test_bank_details_delete_view_get_board_not_allowed(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        user2 = models.SVUser.objects.create_user(
            email='tesat@test.com',
            first_name='Te',
            last_name='St',
            password='1234asdfg'
        )

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=user2
        )

        response = self.client.get(reverse('auth:delete_bankdetails', kwargs={'pk': bd.pk}))

        self.assertEqual(response.status_code, 404)

    def test_bank_details_delete_view_post_not_allowed(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.post(reverse('auth:delete_bankdetails', kwargs={'pk': bd.pk}))

        self.assertEqual(response.status_code, 403)
        self.assertEqual(models.BankDetails.objects.all().count(), 1)

    def test_bank_details_delete_view_get_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.get(
            reverse('auth:delete_bankdetails', kwargs={'pk': bd.pk}),
            follow=True
        )

        self.assertContains(response, _('Delete bank details'), status_code=200)

    def test_bank_details_delete_view_get_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.get(
            reverse('auth:delete_bankdetails', kwargs={'pk': bd.pk}),
            follow=True
        )

        self.assertContains(response, _('Delete bank details'), status_code=200)

    def test_bank_details_delete_view_post_leader(self):
        self.create_leader_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.post(
            reverse('auth:delete_bankdetails', kwargs={'pk': bd.pk}),
            follow=True
        )

        self.assertNotContains(response, 'GENODEFAB12', status_code=200)
        self.assertEqual(models.BankDetails.objects.all().count(), 0)

    def test_bank_details_delete_view_post_cashier(self):
        self.create_cashier_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        bd = models.BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )

        response = self.client.post(
            reverse('auth:delete_bankdetails', kwargs={'pk': bd.pk}),
            follow=True
        )

        self.assertNotContains(response, 'GENODEFAB12', status_code=200)
        self.assertEqual(models.BankDetails.objects.all().count(), 0)

    def test_legal_terms_not_allowed(self):
        response = self.client.get(reverse('auth:legal_terms'), follow=True)

        self.assertNotContains(response, _('Privacy policy'), status_code=200)

    def test_privacy_policy_not_allowed(self):
        response = self.client.get(reverse('auth:privacy_policy'), follow=True)

        self.assertNotContains(response, _('Legal terms'), status_code=200)

    def test_legal_terms(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        response = self.client.get(reverse('auth:legal_terms'))

        self.assertContains(response, _('Legal terms'), status_code=200)

    def test_privacy_policy(self):
        self.create_user()
        self.client.login(email='test@test.com', password='1234asdfg')
        response = self.client.get(reverse('auth:privacy_policy'))

        self.assertContains(response, _('Privacy policy'), status_code=200)

    def test_svuser_update_view_get_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:edit_user', kwargs={'pk': self.user.id}))

        self.assertEqual(response.status_code, 403)

    def test_svuser_update_view_get(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:edit_user', kwargs={'pk': self.user.id}))

        self.assertContains(response, 'test@test.com', status_code=200)

    def test_svuser_update_view_post_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(reverse('auth:edit_user', kwargs={'pk': self.user.id}), {
            'email': 'tast@test.com',
            'first_name': 'Te',
            'last_name': 'St',
        }, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(self.user.email, 'test@test.com')

    def test_svuser_update_view_post(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(reverse('auth:edit_user', kwargs={'pk': self.user.id}), {
            'email': 'tast@test.com',
            'first_name': 'Te',
            'last_name': 'St',
            'roles': 0,
        }, follow=True)
        self.user.refresh_from_db()

        self.assertEqual(self.user.email, 'tast@test.com')
        self.assertContains(response, 'tast@test.com', status_code=200)

    def test_svuser_delete_view_get_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:delete_user', kwargs={'pk': self.user.id}))

        self.assertEqual(response.status_code, 403)

    def test_svuser_delete_view_get(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.get(reverse('auth:delete_user', kwargs={'pk': self.user.id}))

        self.assertContains(response, _('Delete user'), status_code=200)

    def test_svuser_delete_view_post_not_allowed(self):
        self.create_not_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(
            reverse('auth:edit_user', kwargs={'pk': self.user.id}),
            follow=True
        )
        self.user.refresh_from_db()

        self.assertEqual(response.status_code, 403)
        self.assertEqual(models.SVUser.objects.all().count(), 1)

    def test_svuser_delete_view_post(self):
        self.create_board_user()
        self.client.login(email='test@test.com', password='1234asdfg')

        response = self.client.post(
            reverse('auth:edit_user', kwargs={'pk': self.user.id}),
            follow=True
        )

        self.assertNotContains(response, 'test@test.com', status_code=200)


class ModelTests(TestCase):
    def setUp(self):
        self.client = Client()

    def create_user(self):
        self.user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()

    def test_login(self):
        self.create_user()
        login = self.client.login(email='test@test.com', password='1234asdfg')

        self.assertTrue(login)

    def test_create_user(self):
        user = models.SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.assertEqual(user.get_short_name(), 'Te')
        self.assertEqual(user.is_staff, False)

    def test_create_user_failure(self):
        with self.assertRaises(ValueError):
            models.SVUser.objects.create_user(
                    email='',
                    first_name='Te',
                    last_name='St',
                    password='1234asdfg'
                    )

    def test_create_superuser(self):
        user = models.SVUser.objects.create_superuser(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        login = self.client.login(email='test@test.com', password='1234asdfg')

        self.assertTrue(login)
        self.assertTrue(user.is_active)
        self.assertTrue(user.is_superuser)

    def test_create_superuser_failure1(self):
        with self.assertRaises(ValueError):
            models.SVUser.objects.create_superuser(
                    email='',
                    first_name='Te',
                    last_name='St',
                    password='1234asdfg',
                    is_active=False
                    )

    def test_create_superuser_failure2(self):
        with self.assertRaises(ValueError):
            models.SVUser.objects.create_superuser(
                    email='',
                    first_name='Te',
                    last_name='St',
                    password='1234asdfg',
                    is_superuser=False
                    )

    def test_registration_request(self):
        self.create_user()
        rr = models.RegistrationRequest.objects.create(
                user=self.user,
                role=0,
                members='Max'
                )

        self.assertEqual(str(rr), 'Te St (Eltern)')
