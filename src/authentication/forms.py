from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.template import loader
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import force_bytes
from datetime import date
from rolepermissions.roles import assign_role, remove_role
from rolepermissions.checkers import get_user_roles

from . import models
from members.models import Member
from . import roles
from config.widgets import BootstrapSelect2MultipleWidget


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=_('Password'), widget=forms.PasswordInput)
    password2 = forms.CharField(label=_('Password confirmation'), widget=forms.PasswordInput)

    class Meta:
        model = models.SVUser
        fields = ('email', 'first_name', 'last_name')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserRegistrationForm(UserCreationForm):
    members = forms.CharField(max_length=200)
    role = forms.ChoiceField(choices=models.ROLES)

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        subject = loader.render_to_string(subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
        if html_email_template_name is not None:
            html_email = loader.render_to_string(html_email_template_name, context)
            email_message.attach_alternative(html_email, 'text/html')

        email_message.send()

    def save(self, domain_override=None,
             subject_template_name='authentication/register_subject.txt',
             email_template_name='authentication/register_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None,
             html_email_template_name='authentication/register_email.html',
             extra_email_context=None):
        user = super().save(commit=False)
        user.save()

        email = self.cleaned_data["email"]
        if not domain_override:
            current_site = get_current_site(request)
            site_name = current_site.name
            domain = current_site.domain
        else:
            site_name = domain = domain_override
        context = {
            'email': email,
            'domain': domain,
            'site_name': site_name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
            'token': token_generator.make_token(user),
            'protocol': 'https' if use_https else 'http',
            'first_name': user.first_name,
            'year': date.today().year,
            **(extra_email_context or {}),
        }
        self.send_mail(
            subject_template_name, email_template_name, context, from_email,
            email, html_email_template_name=html_email_template_name,
        )

        models.RegistrationRequest.objects.create(
            user=user, role=self.cleaned_data['role'], members=self.cleaned_data['members']
        )

        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = models.SVUser
        fields = ('email', 'password', 'first_name', 'last_name', 'is_active', 'is_superuser')

    def clean_password(self):
        return self.initial["password"]


class UserForm(forms.ModelForm):
    roles = forms.MultipleChoiceField(
        label=_('Roles'),
        choices=roles.ROLES,
        required=False,
        widget=BootstrapSelect2MultipleWidget
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance is not None:
            user_roles = get_user_roles(self.instance)
            rs = []
            for k, r in roles.ROLES_DICT.items():
                if r in user_roles:
                    rs.append(k)
            self.fields['roles'].initial = rs

    def save(self, commit=True):
        for k, r in roles.ROLES_DICT.items():
            if k in self.cleaned_data['roles']:
                assign_role(self.instance, r)
            else:
                remove_role(self.instance, r)
        super().save()

    class Meta:
        model = models.SVUser
        fields = ['email', 'first_name', 'last_name']


class MemberUserForm(forms.Form):
    member = forms.ModelChoiceField(queryset=Member.objects.all())

    def __init__(self, member_name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['member'].label = member_name


class BaseMemberUserFormSet(forms.BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.members = kwargs.pop('members')
        super().__init__(*args, **kwargs)

    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)
        kwargs['member_name'] = self.members[index]
        return kwargs


class MembersEmailForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['event_created'].disabled = True
        self.fields['debit_collected'].disabled = True

    class Meta:
        model = models.MemberEmailSettings
        fields = [
            'event_created',
            'member_created',
            'member_changed',
            'mandate_changed',
            'debit_collected',
            'order_completed',
        ]


class LeaderEmailForm(forms.ModelForm):
    class Meta:
        model = models.LeaderEmailSettings
        fields = [
            'event_changed',
            'participation_changed',
            'group_changed',
            'settlement_completed',
        ]


class CashierEmailForm(forms.ModelForm):
    class Meta:
        model = models.CashierEmailSettings
        fields = [
            'debit_available',
            'settlement_created',
        ]


class StockEmailForm(forms.ModelForm):
    class Meta:
        model = models.StockEmailSettings
        fields = [
            'order_created',
            'reservation_created',
        ]


class BoardEmailForm(forms.ModelForm):
    class Meta:
        model = models.BoardEmailSettings
        fields = [
            'rr_created',
        ]
