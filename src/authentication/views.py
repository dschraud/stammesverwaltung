from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from django.contrib import messages
from django.views.generic.edit import FormView, DeleteView, CreateView, UpdateView
from django.contrib.auth.tokens import default_token_generator
from django.views.generic.base import View, TemplateView
from django.utils.http import urlsafe_base64_decode
from django.shortcuts import redirect, get_object_or_404, render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.forms import formset_factory
from django_tables2 import MultiTableMixin
from django.conf import settings
from django_filters.views import FilterView
from rolepermissions.mixins import HasRoleMixin
from rolepermissions.checkers import has_role
from datetime import date

from . import forms, tables, models, utils, filters
from authentication import roles
from members.models import Section

# Create your views here.


class SVLoginView(LoginView):
    template_name = 'authentication/login.html'
    redirect_authenticated_user = True


class SVLogoutView(LogoutView):
    pass


class SVPasswordChangeView(PasswordChangeView):
    template_name = 'authentication/password_change.html'

    @property
    def success_url(self):
        return reverse_lazy('home')

    def form_valid(self, form):
        messages.success(self.request, _("Your password has been changed."))
        return super().form_valid(form)


class SVPasswordResetView(PasswordResetView):
    template_name = 'authentication/password_reset.html'
    from_email = 'Stammesverwaltung DPSG St. Vitus Hirschaid <sv@pfadfinder-hirschaid.de>'
    email_template_name = 'authentication/password_reset_email.txt'
    html_email_template_name = 'authentication/password_reset_email.html'
    subject_template_name = 'authentication/password_reset_subject.txt'
    token_generator = default_token_generator

    def form_valid(self, form):
        user = models.SVUser.objects.filter(email=form.cleaned_data['email'])
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': {
                'first_name': user.first().first_name if user.exists() else '',
                'year': date.today().year,
                'debug': settings.DEBUG,
            },
        }
        form.save(**opts)
        messages.success(self.request, _(
            "We've emailed you instructions for setting your password, if an account exists with "
            "the email you entered. You should receive them shortly. If you don't receive an "
            "email, please make sure you've entered the address you registered with, and check "
            "your spam folder."
            ))
        return redirect(reverse_lazy('auth:login'))


class SVPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'authentication/password_reset_confirm.html'

    @property
    def success_url(self):
        return reverse_lazy('auth:login')

    def form_valid(self, form):
        messages.success(self.request, _("Your password has been set. You may go ahead and log "
                                         "in now."))
        return super().form_valid(form)


class UserRegistrationView(FormView):
    email_template_name = 'authentication/register_email.txt'
    extra_email_context = None
    from_email = 'Stammesverwaltung DPSG St. Vitus Hirschaid <sv@pfadfinder-hirschaid.de>'
    html_email_template_name = 'authentication/register_email.html'
    subject_template_name = 'authentication/register_subject.txt'
    token_generator = default_token_generator
    template_name = 'authentication/register.html'
    form_class = forms.UserRegistrationForm

    @property
    def success_url(self):
        return reverse_lazy('auth:login')

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': {
                'debug': settings.DEBUG,
            },
        }
        form.save(**opts)
        messages.success(self.request, _(
            "We've sent a message to the email you entered. Please click on the link in the email "
            "to confirm your address. Also remember to check your spam folder."
            ))
        return super().form_valid(form)


class UserRegistrationConfirmView(View):
    token_generator = default_token_generator

    @method_decorator(sensitive_post_parameters())
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        assert 'uidb64' in kwargs and 'token' in kwargs
        try:
            user = models.SVUser.objects.get(id=urlsafe_base64_decode(kwargs['uidb64']).decode())
            rr = models.RegistrationRequest.objects.get(user=user)
        except(TypeError, ValueError, OverflowError, models.SVUser.DoesNotExist,
                models.RegistrationRequest.DoesNotExist):
            rr = None
        if rr is not None and self.token_generator.check_token(user, kwargs['token']):
            rr.email_confirmed = True
            rr.save()
            utils.notify_board_registration_request(self.request, user)
            messages.success(self.request, _(
                "Your email address has been confirmed successfully. Now please wait for an "
                "administrator to activate your account. Note that this can take some time!"
                ))
        else:
            messages.error(self.request, _("The activation link is invalid!"))
        return redirect(reverse('auth:login'))


class AdministrationView(LoginRequiredMixin, HasRoleMixin, MultiTableMixin, FilterView):
    """
    Allows board members to configure the application through various tables.
    """

    allowed_roles = [roles.Board]
    template_name = 'authentication/administration.html'
    filterset_class = filters.UserFilter
    strict = False

    def get_tables(self):
        privileged = []
        regular = []
        rs = [
            roles.Board,
            roles.Cashier,
            roles.Leader,
            roles.Secretary,
            roles.StockWarden
        ]
        for u in self.object_list.filter(is_active=True).order_by('first_name', 'last_name'):
            if u.is_superuser or has_role(u, rs):
                privileged.append(u)
            else:
                regular.append(u)
        return [
            tables.RegistrationRequestTable(models.RegistrationRequest.objects.all()),
            tables.SVUserTable(privileged, True),
            tables.SVUserTable(regular, False),
            tables.SectionTable(Section.objects.all())
        ]


class RegistrationRequestDeleteView(
    LoginRequiredMixin,
    HasRoleMixin,
    SuccessMessageMixin,
    DeleteView
):
    allowed_roles = [roles.Board]
    success_message = _("Registration request of '%(user)s (%(role)s)' "
                        "has been deleted successfully!")
    model = models.RegistrationRequest
    success_url = reverse_lazy('auth:administration')

    def delete(self, request, *args, **kwargs):
        self.get_object().user.delete()
        messages.success(self.request, self.success_message)
        return redirect(self.success_url)


class RegistrationRequestConfirmView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, View):
    allowed_roles = [roles.Board]
    success_message = _("Account of '%(first_name)s %(last_name)s' "
                        "has been activated successfully!")
    template_name = 'authentication/registrationrequest_confirm.html'
    success_url = reverse_lazy('auth:administration')

    def dispatch(self, request, pk, *args, **kwargs):
        self.rr = get_object_or_404(models.RegistrationRequest, pk=pk)
        if not self.rr.email_confirmed:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        members = self.rr.members.split('\r\n')
        user_form = forms.UserForm(instance=self.rr.user)
        MemberUserFormSet = formset_factory(
            forms.MemberUserForm,
            formset=forms.BaseMemberUserFormSet,
            extra=len(members)
        )
        member_formset = MemberUserFormSet(members=members)
        return render(request, self.template_name, {
            'user_form': user_form,
            'member_formset': member_formset
        })

    def post(self, request, *args, **kwargs):
        members = self.rr.members.split('\r\n')
        user_form = forms.UserForm(request.POST, instance=self.rr.user)
        MemberUserFormSet = formset_factory(
            forms.MemberUserForm,
            formset=forms.BaseMemberUserFormSet,
            extra=len(members)
        )
        member_formset = MemberUserFormSet(request.POST, members=members)
        if user_form.is_valid() and member_formset.is_valid():
            user_form.save()
            self.rr.user.is_active = True
            self.rr.user.save()
            for form in member_formset.forms:
                member = form.cleaned_data.get('member')
                if member:
                    member.user = self.rr.user
                    member.save()
            self.rr.delete()
            return redirect('auth:administration')
        else:
            return redirect('auth:confirm_rr', self.rr.pk)


class SettingsListView(LoginRequiredMixin, MultiTableMixin, TemplateView):
    template_name = 'authentication/settings.html'

    def get_tables(self):
        return [
            tables.BankDetailsTable(models.BankDetails.objects.filter(user=self.request.user))
        ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["member_email_form"] = forms.MembersEmailForm(
            instance=models.MemberEmailSettings.objects.get_or_create(
                user=self.request.user
            )[0]
        )
        if has_role(self.request.user, [roles.Board, roles.Leader]):
            context["leader_email_form"] = forms.LeaderEmailForm(
                instance=models.LeaderEmailSettings.objects.get_or_create(
                    user=self.request.user
                )[0]
            )
        if has_role(self.request.user, [roles.Board, roles.Cashier]):
            context["cashier_email_form"] = forms.CashierEmailForm(
                instance=models.CashierEmailSettings.objects.get_or_create(
                    user=self.request.user
                )[0]
            )
        if has_role(self.request.user, [roles.Board, roles.StockWarden]):
            context["stock_email_form"] = forms.StockEmailForm(
                instance=models.StockEmailSettings.objects.get_or_create(
                    user=self.request.user
                )[0]
            )
        if has_role(self.request.user, roles.Board):
            context["board_email_form"] = forms.BoardEmailForm(
                instance=models.BoardEmailSettings.objects.get_or_create(
                    user=self.request.user
                )[0]
            )
        return context

    def post(self, request):
        member_email_form = forms.MembersEmailForm(
            request.POST,
            instance=models.MemberEmailSettings.objects.get_or_create(
                user=self.request.user
            )[0]
        )
        if has_role(self.request.user, [roles.Board, roles.Leader]):
            leader_email_form = forms.LeaderEmailForm(
                request.POST,
                instance=models.LeaderEmailSettings.objects.get_or_create(
                    user=self.request.user
                )[0]
            )
        if has_role(self.request.user, [roles.Board, roles.Cashier]):
            cashier_email_form = forms.CashierEmailForm(
                request.POST,
                instance=models.CashierEmailSettings.objects.get_or_create(
                    user=self.request.user
                )[0]
            )
        if has_role(self.request.user, [roles.Board, roles.StockWarden]):
            stock_email_form = forms.StockEmailForm(
                request.POST,
                instance=models.StockEmailSettings.objects.get_or_create(
                    user=self.request.user
                )[0]
            )

        member_email_form.save()
        if has_role(
            self.request.user,
            [roles.Board, roles.Leader]
        ) and leader_email_form.is_valid():
            leader_email_form.save()
        if has_role(
            self.request.user,
            [roles.Board, roles.Cashier]
        ) and cashier_email_form.is_valid():
            cashier_email_form.save()
        if has_role(
            self.request.user,
            [roles.Board, roles.StockWarden]
        ) and stock_email_form.is_valid():
            stock_email_form.save()

        return redirect(reverse('auth:settings'))


class BankDetailsCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    model = models.BankDetails
    fields = [
        'depositor',
        'iban',
        'bic',
        'bank',
    ]
    template_name = 'authentication/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('auth:settings')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create bank details')
        return context


class BankDetailsUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    model = models.BankDetails
    fields = [
        'depositor',
        'iban',
        'bic',
        'bank',
    ]
    template_name = 'authentication/form.html'

    def get_queryset(self):
        return models.BankDetails.objects.filter(user=self.request.user)

    def get_success_url(self):
        return reverse('auth:settings')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit bank details')
        return context


class BankDetailsDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    model = models.BankDetails
    success_url = reverse_lazy('auth:settings')

    def get_queryset(self):
        return models.BankDetails.objects.filter(user=self.request.user)


class LegalTermsView(LoginRequiredMixin, TemplateView):
    template_name = 'authentication/legal_terms.html'


class PrivacyPolicyView(LoginRequiredMixin, TemplateView):
    template_name = 'authentication/privacy_policy.html'


class SVUserUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board]
    form_class = forms.UserForm
    template_name = 'authentication/form.html'

    def get_queryset(self):
        return models.SVUser.objects.all()

    def get_success_url(self):
        return reverse('auth:administration')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit user')
        return context


class SVUserDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board]
    model = models.SVUser
    success_url = reverse_lazy('auth:administration')
