"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

app_name = 'auth'

urlpatterns = [
    path(_('login/'), views.SVLoginView.as_view(), name='login'),
    path(_('logout/'), views.SVLogoutView.as_view(), name='logout'),
    path(_('password-change/'), views.SVPasswordChangeView.as_view(), name='password-change'),
    path(_('password-reset/'), views.SVPasswordResetView.as_view(), name='password-reset'),
    path(_('password-reset-confirm/<uidb64>/<token>/'), views.SVPasswordResetConfirmView.as_view(),
         name='password-reset-confirm'),
    path(_('register/'), views.UserRegistrationView.as_view(), name='register'),
    path(_('register-confirm/<uidb64>/<token>/'), views.UserRegistrationConfirmView.as_view(),
         name='register-confirm'),
    path(_('administration/'), views.AdministrationView.as_view(), name='administration'),
    path(_('delete-rr/<pk>/'), views.RegistrationRequestDeleteView.as_view(), name='delete_rr'),
    path(_('confirm-rr/<pk>/'), views.RegistrationRequestConfirmView.as_view(), name='confirm_rr'),
    path(_('settings/'), views.SettingsListView.as_view(), name='settings'),
    path(
        _('create-bankdetails/'),
        views.BankDetailsCreateView.as_view(),
        name='create_bankdetails'
    ),
    path(
        _('edit-bankdetails/<pk>/'),
        views.BankDetailsUpdateView.as_view(),
        name='edit_bankdetails'
    ),
    path(
        _('delete-bankdetails/<pk>/'),
        views.BankDetailsDeleteView.as_view(),
        name='delete_bankdetails'
    ),
    path(
        _('edit-user/<pk>/'),
        views.SVUserUpdateView.as_view(),
        name='edit_user'
    ),
    path(
        _('delete-user/<pk>/'),
        views.SVUserDeleteView.as_view(),
        name='delete_user'
    ),
    path(_('legal-terms/'), views.LegalTermsView.as_view(), name='legal_terms'),
    path(_('privacy-policy/'), views.PrivacyPolicyView.as_view(), name='privacy_policy'),
]
