{% extends  "_base_email.txt" %}

{% load i18n %}

{% block message %}
{% blocktrans %}"{{ user }}" wants to register for the Stammesverwaltung. You need to confirm this request.{% endblocktrans %}
{% endblock %}

{% block clicklink %}
{% trans "Click the following link to see all registration requests." %}
{% endblock %}

{% block url %}{% url 'auth:administration' %}{% endblock %}