from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from . import models
from . import forms

# Register your models here.

admin.site.register(models.RegistrationRequest)
admin.site.register(models.MemberEmailSettings)
admin.site.register(models.LeaderEmailSettings)
admin.site.register(models.CashierEmailSettings)
admin.site.register(models.StockEmailSettings)
admin.site.register(models.BoardEmailSettings)
admin.site.register(models.BankDetails)


@admin.register(models.SVUser)
class SVUserAdmin(UserAdmin):
    form = forms.UserChangeForm
    add_form = forms.UserCreationForm

    list_display = ('email', 'first_name', 'last_name', 'is_active', 'is_superuser')
    list_filter = ('is_superuser', 'is_active',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_superuser',)}),
    )
    add_fieldsets = (
        (None, {'classes': ('wide',),
                'fields': ('email', 'first_name', 'last_name', 'password1', 'password2')}),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()
