# Generated by Django 2.0.6 on 2019-04-03 20:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0011_auto_20190403_0414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='leaderemailsettings',
            name='settlement_completed',
            field=models.BooleanField(default=True, verbose_name='A settlement of yours has been completed'),
        ),
    ]
