# Generated by Django 2.0.6 on 2018-11-21 21:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_auto_20180716_1804'),
        ('finances', '0008_auto_20181021_1723'),
    ]

    operations = [
        migrations.CreateModel(
            name='Settlement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(auto_now=True, verbose_name='date')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=7, verbose_name='amount')),
                ('voucher', models.ImageField(upload_to='', verbose_name='voucher')),
                ('comment', models.CharField(max_length=100, verbose_name='comment')),
            ],
            options={
                'verbose_name': 'Settlement',
                'verbose_name_plural': 'Settlements',
                'ordering': ['date', 'payee'],
            },
        ),
        migrations.AlterField(
            model_name='account',
            name='name',
            field=models.CharField(max_length=50, verbose_name='name'),
        ),
        migrations.AlterField(
            model_name='account',
            name='type',
            field=models.IntegerField(choices=[(0, 'Asset account'), (1, 'Profit and loss account')], verbose_name='type'),
        ),
        migrations.AddField(
            model_name='settlement',
            name='contra_account',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='contra', to='finances.Account', verbose_name='contra account'),
        ),
        migrations.AddField(
            model_name='settlement',
            name='payee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='payee', to='members.Member', verbose_name='payee'),
        ),
    ]
