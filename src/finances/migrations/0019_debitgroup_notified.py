# Generated by Django 2.0.6 on 2019-04-02 23:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0018_auto_20190315_2039'),
    ]

    operations = [
        migrations.AddField(
            model_name='debitgroup',
            name='notified',
            field=models.BooleanField(default=False),
        ),
    ]
