# Generated by Django 2.0.6 on 2018-10-21 13:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0008_auto_20180925_2102'),
        ('finances', '0003_auto_20181020_1805'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='debit',
            name='event',
        ),
        migrations.AddField(
            model_name='debit',
            name='participation',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='events.Participation'),
        ),
    ]
