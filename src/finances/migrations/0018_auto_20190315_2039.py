# Generated by Django 2.0.6 on 2019-03-15 20:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0017_auto_20190227_0102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='settlement',
            name='payee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='authentication.BankDetails', verbose_name='payee'),
        ),
    ]
