# Generated by Django 2.0.6 on 2018-12-26 18:28

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0011_auto_20181127_2154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='settlement',
            name='date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='date'),
        ),
    ]
