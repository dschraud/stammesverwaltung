from django import forms
from rolepermissions.checkers import has_role
from bootstrap_datepicker_plus.widgets import DatePickerInput, YearPickerInput

from . import models
from authentication.models import BankDetails
from authentication import roles
from members.models import Member
from config.widgets import EURWidget, KmWidget, AccountWidget, FileWidget, BootstrapSelect2Widget


class DebitForm(forms.ModelForm):
    class Meta:
        model = models.Debit
        fields = [
            'member',
            'reference',
            'amount',
        ]
        widgets = {
            'member': BootstrapSelect2Widget,
            'amount': EURWidget
        }


class EntryForm(forms.ModelForm):
    class Meta:
        model = models.Entry
        fields = [
            'date',
            'amount',
            'voucher',
            'comment',
            'debit_account',
            'credit_account',
        ]
        widgets = {
            'date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'debit_account': AccountWidget,
            'credit_account': AccountWidget,
            'amount': EURWidget,
            'voucher': FileWidget
        }


class ContraAccountForm(forms.ModelForm):
    class Meta:
        model = models.Account
        fields = [
            'name',
            'year',
        ]
        widgets = {
            'year': YearPickerInput(options={'format': 'YYYY', 'locale': 'de'}),
        }


class AccountCreateForm(forms.ModelForm):
    class Meta:
        model = models.Account
        fields = [
            'name',
            'year',
            'type',
        ]
        widgets = {
            'type': BootstrapSelect2Widget,
            'year': YearPickerInput(options={'format': 'YYYY', 'locale': 'de'}),
        }


class AccountUpdateForm(forms.ModelForm):
    class Meta:
        model = models.Account
        fields = [
            'name',
            'year',
            'type',
            'closed',
        ]
        widgets = {
            'type': BootstrapSelect2Widget,
            'year': YearPickerInput(options={'format': 'YYYY', 'locale': 'de'}),
        }


class MembershipFeeCollectionCreateForm(forms.ModelForm):
    class Meta:
        model = models.MembershipFeeCollection
        fields = ['year', 'nr']
        widgets = {
            'year': YearPickerInput(options={'format': 'YYYY', 'locale': 'de'}),
            'nr': forms.NumberInput(attrs={'min': '1'})
        }


class SettlementForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        self.fields['contra_account'].queryset = models.Account.objects.filter(
            closed=False,
            type=1
        )
        if not has_role(user, [roles.Board, roles.Cashier]):
            self.fields['payee'].queryset = BankDetails.objects.filter(user=user)

    class Meta:
        model = models.Settlement
        fields = [
            'voucher',
            'comment',
            'amount',
            'contra_account',
            'payee'
        ]
        widgets = {
            'contra_account': AccountWidget(contra_account=True),
            'payee': BootstrapSelect2Widget,
            'amount': EURWidget,
            'voucher': FileWidget
        }


class DonationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        self.fields['payee'].queryset = Member.objects.filter(user=user)

    class Meta:
        model = models.Donation
        fields = [
            'date',
            'name',
            'amount',
            'comment',
            'payee'
        ]
        widgets = {
            'date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'payee': BootstrapSelect2Widget,
            'amount': EURWidget,
        }


class TravelingCostForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        self.fields['payee'].queryset = Member.objects.filter(user=user)

    class Meta:
        model = models.TravelingCost
        fields = [
            'date',
            'from_place',
            'to_place',
            'kilometers',
            'comment',
            'payee'
        ]
        widgets = {
            'date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'payee': BootstrapSelect2Widget,
            'amount': EURWidget,
            'kilometers': KmWidget,
        }
