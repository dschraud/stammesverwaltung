from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from django.core.files.storage import FileSystemStorage
from django.utils._os import upath
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from rolepermissions.roles import assign_role
import datetime
from decimal import Decimal

from authentication.models import SVUser, BankDetails
from authentication import roles
from members.models import Section, Member, SEPAMandate
from events.models import Event, Participation
from . import models
from . import utils

# Create your tests here.

THIS_FILE = upath(__file__.rstrip("c"))
file_storage = FileSystemStorage(location=settings.MEDIA_ROOT)


class ViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
        )
        self.user2 = SVUser.objects.create_user(
                email='tast@test.com',
                first_name='Tesd',
                last_name='Stas',
                password='1235asdfg'
        )
        self.user.is_active = True
        self.user.save()
        self.client.login(email='test@test.com', password='1234asdfg')

        self.section1 = Section.objects.create(
                name='Beever',
                min_age=10,
                max_age=14
        )
        self.section2 = Section.objects.create(
                name='Cupse',
                min_age=13,
                max_age=17
        )
        self.mandate1 = SEPAMandate.objects.create(
                reference='1-1',
                depositor='Martina Mustermann',
                iban='DE00770500000000123456',
                bic='DJANGODEF12',
                bank='Django Reserve',
                date='2017-04-18'
                )
        self.mandate2 = SEPAMandate.objects.create(
                reference='1-2',
                depositor='Jochen Mustermann',
                iban='DE00770500000000789012',
                bic='DJANGODEF12',
                bank='Django Reserve',
                date='2018-05-20'
                )
        self.mandate3 = SEPAMandate.objects.create(
                reference='2-1',
                depositor='Jakob Mustermann',
                iban='DE00770500000000789013',
                bic='DJANGODEF12',
                bank='Django Reserve',
                date='2018-05-20'
                )
        self.member1 = Member.objects.create(
                nr=1,
                first_name='Max',
                last_name='Mustermann',
                section=self.section1,
                function=1,
                nami_nr=100001,
                fee=15.00,
                mandate_membership_fee=self.mandate1,
                mandate_participation_fee=self.mandate2,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m',
                mobile='016012345678',
                user=self.user
        )
        self.member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section1,
                function=0,
                nami_nr=100002,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='f'
        )
        self.member3 = Member.objects.create(
                nr=3,
                first_name='Mario',
                last_name='Mustermann',
                section=self.section2,
                function=0,
                nami_nr=100003,
                fee=15.00,
                mandate_membership_fee=self.mandate1,
                mandate_participation_fee=self.mandate2,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2002-01-01',
                gender='m'
        )
        fd, td, ed = utils.get_debit_dates()
        self.debit_group1 = models.DebitGroup.objects.create(
                from_date=fd,
                to_date=td,
                execution_date=ed
        )
        self.debit_group2 = models.DebitGroup.objects.create(
                from_date='2018-12-11',
                to_date='2019-01-10',
                execution_date='2019-01-15'
        )
        self.debit1 = models.Debit.objects.create(
                member=self.member1,
                amount=15.00,
                reference='Membership fee',
                group=self.debit_group1,
        )
        self.debit2 = models.Debit.objects.create(
                member=self.member3,
                amount=10.00,
                reference='Membership fee',
                group=self.debit_group1,
        )
        self.debit3 = models.Debit.objects.create(
                member=self.member1,
                amount=25.00,
                reference='Participation fee',
                group=self.debit_group2,
        )
        self.debit4 = models.Debit.objects.create(
                member=self.member3,
                amount=35.00,
                reference='Participation fee',
                group=self.debit_group2,
        )
        self.bank_details1 = BankDetails.objects.create(
            depositor='Te st',
            iban='DE123456789012345678890',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user
        )
        self.bank_details2 = BankDetails.objects.create(
            depositor='Tesd Stas',
            iban='DE09876543210987654321',
            bic='GENODEFAB12',
            bank='VR Bank Musterhausen',
            user=self.user2
        )
        self.account1 = models.Account.objects.create(
            name='Group meetings',
            year=2018,
            type=1
        )
        self.account2 = models.Account.objects.create(
            name='Bank account',
            year=2018,
            type=0
        )
        with open(upath(THIS_FILE), 'rb') as file:
            file_storage.save('voucher.pdf', file)
        with file_storage.open('voucher.pdf') as voucher:
            self.entry1 = models.Entry.objects.create(
                date='2018-10-31',
                amount=2.50,
                voucher=voucher,
                comment='Glue',
                debit_account=self.account1,
                credit_account=self.account2
            )
            self.entry2 = models.Entry.objects.create(
                date='2018-09-30',
                amount=7.40,
                voucher=voucher,
                comment='Paper',
                debit_account=self.account1,
                credit_account=self.account2
            )
            self.settlement1 = models.Settlement.objects.create(
                amount=3.30,
                voucher=voucher,
                comment='Scissors',
                contra_account=self.account1,
                payee=self.bank_details1
            )
            self.settlement2 = models.Settlement.objects.create(
                amount=2.50,
                voucher=voucher,
                comment='Glue',
                contra_account=self.account1,
                payee=self.bank_details1,
                entry=self.entry1
            )
            self.settlement3 = models.Settlement.objects.create(
                amount=8.70,
                voucher=voucher,
                comment='Candy',
                contra_account=self.account1,
                payee=self.bank_details2,
            )
            self.settlement4 = models.Settlement.objects.create(
                amount=7.40,
                voucher=voucher,
                comment='Paper',
                contra_account=self.account1,
                payee=self.bank_details2,
                entry=self.entry2
            )
        self.donation1 = models.Donation.objects.create(
            date='2018-08-31',
            name='Printing',
            amount=4.99,
            comment='For camp',
            payee=self.member1
        )
        self.donation2 = models.Donation.objects.create(
            date='2018-07-31',
            name='Postage',
            amount=0.80,
            comment='For camp',
            payee=self.member3
        )
        self.traveling_costs1 = models.TravelingCost.objects.create(
            date='2018-06-30',
            kilometers=43,
            from_place='Musterhausen',
            to_place='Rothmannsthal',
            comment='For camp',
            payee=self.member1
        )
        self.traveling_costs2 = models.TravelingCost.objects.create(
            date='2018-05-31',
            kilometers=54,
            from_place='Musterstadt',
            to_place='Musterhausen',
            comment='Meeting',
            payee=self.member3
        )
        self.event = Event.objects.create(
            name='Camp',
            from_date='2018-01-01',
            to_date='2018-01-08',
            place='Somewhere',
            fee=100.99,
            level=1,
            description="It's gonna be great",
            deadline=datetime.date.today() + datetime.timedelta(days=1)
        )
        self.participation = Participation.objects.create(
            event=self.event,
            member=self.member3
        )

    def tearDown(self):
        file_storage.delete('voucher.pdf')

    def test_debit_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:debits'))

        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, '15,00', status_code=200)
        self.assertContains(response, '25,00', status_code=200)

        self.assertNotContains(response, 'Mario', status_code=200)
        self.assertNotContains(response, '10,00', status_code=200)
        self.assertNotContains(response, '35,00', status_code=200)

    def test_debit_list_view_cashier(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(reverse('finances:debits'))

        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, '15,00', status_code=200)
        self.assertContains(response, 'Mario', status_code=200)
        self.assertContains(response, '10,00', status_code=200)
        self.assertContains(response, '60,00', status_code=200)

    def test_old_debit_list_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:old_debits', kwargs={
            'group': self.debit_group2.pk
        }))

        self.assertEqual(response.status_code, 403)

    def test_old_debit_list_view_cashier(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(reverse('finances:old_debits', kwargs={
            'group': self.debit_group2.pk
        }))

        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, 'Mario', status_code=200)
        self.assertContains(response, '25,00', status_code=200)
        self.assertContains(response, '35,00', status_code=200)

        self.assertNotContains(response, '15,00', status_code=200)
        self.assertNotContains(response, '10,00', status_code=200)

    def test_settlement_list_view_not_allowed(self):
        response = self.client.get(reverse('finances:settlements'))

        self.assertEqual(response.status_code, 403)

    def test_settlement_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:settlements'))

        self.assertContains(response, 'Scissors', status_code=200)
        self.assertContains(response, 'Glue', status_code=200)

        self.assertNotContains(response, 'Paper', status_code=200)
        self.assertNotContains(response, 'Candy', status_code=200)

    def test_settlement_list_view_cashier(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(reverse('finances:settlements'))

        self.assertContains(response, 'Scissors', status_code=200)
        self.assertContains(response, 'Glue', status_code=200)
        self.assertContains(response, 'Paper', status_code=200)
        self.assertContains(response, 'Candy', status_code=200)

    def test_entry_list_view_not_allowed(self):
        response = self.client.get(reverse('finances:entries', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 403)

    def test_entry_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:entries', kwargs={
            'year': 2018
        }))

        self.assertContains(response, 'Glue', status_code=200)
        self.assertContains(response, 'Paper', status_code=200)
        self.assertContains(response, '30.09.2018', status_code=200)
        self.assertContains(response, '31.10.2018', status_code=200)

        self.assertNotContains(response, 'Scissors', status_code=200)
        self.assertNotContains(response, 'Candy', status_code=200)

    def test_entry_list_view_cashier(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(reverse('finances:entries', kwargs={
            'year': 2018
        }))

        self.assertContains(response, 'Glue', status_code=200)
        self.assertContains(response, 'Paper', status_code=200)
        self.assertContains(response, '30.09.2018', status_code=200)
        self.assertContains(response, '31.10.2018', status_code=200)

    def test_account_list_view_not_allowed(self):
        response = self.client.get(reverse('finances:accounts', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 403)

    def test_accounts_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:accounts', kwargs={
            'year': 2018
        }))

        self.assertContains(response, 'Bank account', status_code=200)
        self.assertContains(response, '9,90 %s' % _('Credit'), status_code=200)
        self.assertContains(response, 'Group meetings', status_code=200)
        self.assertContains(response, '9,90 %s' % _('Debit'), status_code=200)

    def test_donation_list_view_not_allowed(self):
        response = self.client.get(reverse('finances:donations', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 403)

    def test_donation_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:donations', kwargs={
            'year': 2018
        }))

        self.assertContains(response, '4,99', status_code=200)
        self.assertContains(response, '12,9', status_code=200)

        self.assertNotContains(response, '0,80', status_code=200)
        self.assertNotContains(response, '16,2', status_code=200)

    def test_donation_export_view_not_allowed(self):
        response = self.client.get(reverse('finances:export_donations', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 403)

    def test_debit_create_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member2.pk,
            'amount': '15.45',
            'reference': 'Random fee'
        }
        response = self.client.post(
            reverse('finances:create_debit'),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_debit_create_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:create_debit'),
            follow=True
        )

        self.assertContains(response, _('Create debit'), status_code=200)

    def test_debit_create_view_cashier(self):
        assign_role(self.user, roles.Cashier)

        post_data = {
            'member': self.member2.pk,
            'amount': '15.45',
            'reference': 'Random fee'
        }
        response = self.client.post(
            reverse('finances:create_debit'),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)

        debit = models.Debit.objects.get(pk=5)

        self.assertEqual(debit.reference, 'Random fee')

    def test_debit_update_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'reference': 'Random fee'
        }
        response = self.client.get(
            reverse('finances:edit_debit', kwargs={
                'pk': self.debit2.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_debit_update_view_cashier_debit_completed(self):
        assign_role(self.user, roles.Cashier)

        post_data = {
            'member': self.member3.pk,
            'amount': 10.00,
            'reference': 'Random fee'
        }
        response = self.client.post(
            reverse('finances:edit_debit', kwargs={
                'pk': self.debit4.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_debit_update_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:edit_debit', kwargs={
                'pk': self.debit2.pk
            }),
            follow=True
        )

        self.assertContains(response, 'Membership fee', status_code=200)

    def test_debit_update_view_cashier(self):
        assign_role(self.user, roles.Cashier)

        post_data = {
            'member': self.member3.pk,
            'amount': 10.00,
            'reference': 'Random fee'
        }
        response = self.client.post(
            reverse('finances:edit_debit', kwargs={
                'pk': self.debit2.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)

        debit = models.Debit.objects.get(pk=2)

        self.assertEqual(debit.reference, 'Random fee')

    def test_debit_delete_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:delete_debit', kwargs={
                'pk': self.debit2.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_debit_delete_view_cashier_debit_completed(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.post(
            reverse('finances:delete_debit', kwargs={
                'pk': self.debit4.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_debit_delete_view_cashier(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.post(
            reverse('finances:delete_debit', kwargs={
                'pk': self.debit2.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 200)

        debit = models.Debit.objects.filter(pk=2)

        self.assertFalse(debit.exists())

    def test_debit_export_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:export_debits', kwargs={
            'group': self.debit_group2.pk
        }))

        self.assertEqual(response.status_code, 403)

    def test_debit_export_view(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(reverse('finances:export_debits', kwargs={
            'group': self.debit_group2.pk
        }))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.get('Content-Disposition'),
            'attachment; filename="SEPA-15-01-2019.csv"'
        )

    def test_mfc_create_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_mfc')
        )

        self.assertEqual(response.status_code, 403)

    def test_mfc_create_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:create_mfc')
        )

        self.assertContains(response, _('Collect membership fees'), status_code=200)

    def test_mfc_create_view_post(self):
        assign_role(self.user, roles.Secretary)

        post_data = {
            'year': 2019,
            'nr': 1,
            'create': [1, 2],
        }
        response = self.client.post(
            reverse('finances:create_mfc'),
            post_data,
            follow=True
        )

        self.assertContains(response, '30,00', status_code=200)

    def test_event_debit_create_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_event_debits', kwargs={
                'event': self.event.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_event_debit_create_view_get(self):
        assign_role(self.user, roles.Board)

        response = self.client.get(
            reverse('finances:create_event_debits', kwargs={
                'event': self.event.pk
            })
        )

        self.assertContains(response, _('Collect participation fees for'), status_code=200)

    def test_event_debit_create_view_post(self):
        assign_role(self.user, roles.Board)

        post_data = {
            'create': self.participation.pk,
        }
        response = self.client.post(
            reverse('finances:create_event_debits', kwargs={
                'event': self.event.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertAlmostEqual(models.Debit.objects.get(pk=5).amount, Decimal(100.99))

    def test_entry_create_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_entry')
        )

        self.assertEqual(response.status_code, 403)

    def test_entry_create_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:create_entry')
        )

        self.assertContains(response, _('Create entry'), status_code=200)

    def test_entry_create_view_post(self):
        assign_role(self.user, roles.Cashier)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'date': '14.02.2019',
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Number One Dime',
                'debit_account': self.account1.pk,
                'credit_account': self.account2.pk
            }
            response = self.client.post(
                reverse('finances:create_entry'),
                post_data,
                follow=True
            )

        self.assertContains(response, 'Number One Dime', status_code=200)
        self.assertEqual(models.Entry.objects.all().count(), 3)

    def test_entry_update_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:edit_entry', kwargs={
                'pk': self.entry1.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_entry_update_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:edit_entry', kwargs={
                'pk': self.entry1.pk
            })
        )

        self.assertContains(response, _('Edit entry'), status_code=200)

    def test_entry_update_view_post(self):
        assign_role(self.user, roles.Cashier)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'date': '14.02.2019',
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Number One Dime',
                'debit_account': self.account1.pk,
                'credit_account': self.account2.pk
            }
            response = self.client.post(
                reverse('finances:edit_entry', kwargs={
                    'pk': self.entry1.pk
                }),
                post_data,
                follow=True
            )

        self.assertContains(response, 'Number One Dime', status_code=200)
        self.assertEqual(models.Entry.objects.all().count(), 2)

    def test_entry_delete_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:delete_entry', kwargs={
                'pk': self.entry1.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_entry_delete_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:delete_entry', kwargs={
                'pk': self.entry1.pk
            })
        )

        self.assertContains(response, _('Delete entry'), status_code=200)

    def test_entry_delete_view_post(self):
        self.settlement2.delete()

        assign_role(self.user, roles.Cashier)

        response = self.client.post(
            reverse('finances:delete_entry', kwargs={
                'pk': self.entry1.pk
            }),
            follow=True
        )

        self.assertNotContains(response, 'Glue', status_code=200)
        self.assertEqual(models.Entry.objects.all().count(), 1)

    def test_account_create_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_account')
        )

        self.assertEqual(response.status_code, 403)

    def test_account_create_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:create_account')
        )

        self.assertContains(response, _('Create account'), status_code=200)

    def test_account_create_view_post(self):
        assign_role(self.user, roles.Cashier)

        post_data = {
            'name': 'Money',
            'year': '2019',
            'type': 1,
        }
        response = self.client.post(
            reverse('finances:create_account'),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Money', status_code=200)
        self.assertEqual(models.Account.objects.all().count(), 3)

    def test_contra_account_create_view_not_allowed(self):

        response = self.client.get(
            reverse('finances:create_contra_account')
        )

        self.assertEqual(response.status_code, 403)

    def test_contra_account_create_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_contra_account')
        )

        self.assertContains(response, _('Create contra account'), status_code=200)

    def test_contra_account_create_view_post(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'name': 'Money',
            'year': '2019',
        }
        response = self.client.post(
            reverse('finances:create_contra_account'),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Money', status_code=200)
        self.assertEqual(models.Account.objects.get(pk=3).type, 1)

    def test_account_update_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:edit_account', kwargs={
                'pk': self.account1.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_account_update_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:edit_account', kwargs={
                'pk': self.account1.pk
            })
        )

        self.assertContains(response, _('Edit account'), status_code=200)

    def test_account_update_view_post(self):
        assign_role(self.user, roles.Cashier)

        post_data = {
            'name': 'Money',
            'year': '2019',
            'type': 1,
        }
        response = self.client.post(
            reverse('finances:edit_account', kwargs={
                'pk': self.account1.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Money', status_code=200)
        self.assertEqual(models.Account.objects.all().count(), 2)

    def test_account_close_view_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:close_accounts', kwargs={
                'year': 2018
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_account_close_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:close_accounts', kwargs={
                'year': 2018
            })
        )

        self.assertContains(response, '%s %s' % (_('Close accounts for'), '2018'), status_code=200)

    def test_account_close_view_post(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.post(
            reverse('finances:close_accounts', kwargs={
                'year': 2018
            }),
            follow=True
        )
        self.account1.refresh_from_db()
        self.account2.refresh_from_db()

        self.assertContains(response, _('closed'), status_code=200)
        self.assertTrue(self.account1.closed)
        self.assertTrue(self.account2.closed)

    def test_entry_export_view_not_allowed(self):
        response = self.client.get(reverse('finances:export_entry', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 403)

    def test_entry_export_view(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:export_entry', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.get('Content-Disposition'),
            _('attachment; filename="Entries-%s.csv"') % 2018
        )

    def test_voucher_export_view_not_allowed(self):
        response = self.client.get(reverse('finances:export_voucher', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 403)

    def test_voucher_export_view(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:export_voucher', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.get('Content-Disposition'),
            'attachment; filename=%s' % (_('Vouchers-%s.pdf') % 2018)
        )

    def test_event_voucher_export_view_not_allowed(self):
        response = self.client.get(reverse('finances:event_export_voucher', kwargs={
            'event': self.event.pk
        }))

        self.assertEqual(response.status_code, 403)

    def test_event_voucher_export_view(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:event_export_voucher', kwargs={
            'event': self.event.pk
        }))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.get('Content-Disposition'),
            'attachment; filename=%s' % (_('%(event)s-Vouchers-%(date)s.pdf') % {
                'event': self.event,
                'date': datetime.datetime.now().strftime('%Y-%m-%d')
            })
        )

    def test_account_export_view_not_allowed(self):
        response = self.client.get(reverse('finances:export_account', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 403)

    def test_account_export_view(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('finances:export_account', kwargs={
            'year': 2018
        }))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.get('Content-Disposition'),
            _('attachment; filename="Accounts-%s.csv"') % 2018
        )

    def test_settlement_create_view_not_allowed(self):
        response = self.client.get(
            reverse('finances:create_settlement')
        )

        self.assertEqual(response.status_code, 403)

    def test_settlement_create_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_settlement')
        )

        self.assertContains(response, _('Create settlement'), status_code=200)

    def test_settlement_create_view_post_account_closed_not_allowed(self):
        self.account1.closed = True
        self.account1.save()

        assign_role(self.user, roles.Leader)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Important things',
                'contra_account': self.account1.pk,
                'payee': self.bank_details1.pk
            }
            response = self.client.post(
                reverse('finances:create_settlement'),
                post_data,
                follow=True
            )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Settlement.objects.all().count(), 4)

    def test_settlement_create_view_post_wrong_bank_details_not_allowed(self):
        assign_role(self.user, roles.Leader)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Important things',
                'contra_account': self.account1.pk,
                'payee': self.bank_details2.pk
            }
            response = self.client.post(
                reverse('finances:create_settlement'),
                post_data,
                follow=True
            )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Settlement.objects.all().count(), 4)

    def test_settlement_create_view_post(self):
        assign_role(self.user, roles.Leader)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'voucher': voucher,
                'amount': '0.01',
                'comment': 'Important things',
                'contra_account': self.account1.pk,
                'payee': self.bank_details1.pk
            }
            response = self.client.post(
                reverse('finances:create_settlement'),
                post_data,
                follow=True
            )

        self.assertContains(response, 'Important things', status_code=200)
        self.assertEqual(models.Settlement.objects.all().count(), 5)

    def test_settlement_update_view_get_not_allowed(self):
        response = self.client.get(
            reverse('finances:edit_settlement', kwargs={
                'pk': self.settlement3.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_settlement_update_view_get_wrong_settlement_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:edit_settlement', kwargs={
                'pk': self.settlement1.pk
            })
        )

        self.assertContains(response, _('Edit settlement'), status_code=200)

    def test_settlement_update_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:edit_settlement', kwargs={
                'pk': self.settlement1.pk
            })
        )

        self.assertContains(response, _('Edit settlement'), status_code=200)

    def test_settlement_update_view_get_cashier(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:edit_settlement', kwargs={
                'pk': self.settlement3.pk
            })
        )

        self.assertContains(response, _('Edit settlement'), status_code=200)

    def test_settlement_update_view_post_account_closed_not_allowed(self):
        self.account1.closed = True
        self.account1.save()

        assign_role(self.user, roles.Leader)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Important things',
                'contra_account': self.account1.pk,
                'payee': self.bank_details1.pk
            }
            response = self.client.post(
                reverse('finances:edit_settlement', kwargs={
                    'pk': self.settlement1.pk
                }),
                post_data,
                follow=True
            )
        self.settlement1.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.settlement1.comment, 'Scissors')

    def test_settlement_update_view_post_wrong_bank_details_not_allowed(self):
        assign_role(self.user, roles.Leader)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Important things',
                'contra_account': self.account1.pk,
                'payee': self.bank_details2.pk
            }
            response = self.client.post(
                reverse('finances:edit_settlement', kwargs={
                    'pk': self.settlement1.pk
                }),
                post_data,
                follow=True
            )
        self.settlement1.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.settlement1.comment, 'Scissors')

    def test_settlement_update_view_post(self):
        assign_role(self.user, roles.Leader)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Important things',
                'contra_account': self.account1.pk,
                'payee': self.bank_details1.pk
            }
            response = self.client.post(
                reverse('finances:edit_settlement', kwargs={
                    'pk': self.settlement1.pk
                }),
                post_data,
                follow=True
            )

        self.assertContains(response, 'Important things', status_code=200)
        self.assertEqual(models.Settlement.objects.all().count(), 4)

    def test_settlement_delete_view_get_not_allowed(self):
        response = self.client.get(
            reverse('finances:delete_settlement', kwargs={
                'pk': self.settlement1.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_settlement_delete_view_get_wrong_settlement_not_allowed(self):
        response = self.client.get(
            reverse('finances:delete_settlement', kwargs={
                'pk': self.settlement3.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_settlement_delete_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:delete_settlement', kwargs={
                'pk': self.settlement1.pk
            })
        )

        self.assertContains(response, _('Delete settlement'), status_code=200)

    def test_settlement_delete_view_get_cashier(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:delete_settlement', kwargs={
                'pk': self.settlement3.pk
            })
        )

        self.assertContains(response, _('Delete settlement'), status_code=200)

    def test_settlement_delete_view_post(self):
        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('finances:delete_settlement', kwargs={
                'pk': self.settlement1.pk
            }),
            follow=True
        )

        self.assertNotContains(response, 'Scissors', status_code=200)
        self.assertEqual(models.Settlement.objects.all().count(), 3)

    def test_settlement_book_view_get_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:book_settlement', kwargs={
                'pk': self.settlement3.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_settlement_book_view_get(self):
        assign_role(self.user, roles.Cashier)

        response = self.client.get(
            reverse('finances:book_settlement', kwargs={
                'pk': self.settlement3.pk
            })
        )

        self.assertContains(response, _('Create entry'), status_code=200)

    def test_settlement_book_view_post(self):
        assign_role(self.user, roles.Cashier)

        with file_storage.open('voucher.pdf') as voucher:
            post_data = {
                'date': '14.02.2019',
                'amount': '0.01',
                'voucher': voucher,
                'comment': 'Important things',
                'debit_account': self.account1.pk,
                'credit_account': self.account2.pk
            }
            response = self.client.post(
                reverse('finances:book_settlement', kwargs={
                    'pk': self.settlement3.pk
                }),
                post_data,
                follow=True
            )
        self.settlement3.refresh_from_db()

        self.assertContains(response, 'Important things', status_code=200)
        self.assertEqual(models.Entry.objects.all().count(), 3)
        self.assertEqual(self.settlement3.entry.comment, 'Important things')

    def test_event_account_create_view_not_allowed(self):
        response = self.client.get(
            reverse('finances:create_event_account', kwargs={
                'event': self.event.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_event_account_create_view(self):
        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('finances:create_event_account', kwargs={
                'event': self.event.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Account.objects.all().count(), 3)

    def test_donation_create_view_not_allowed(self):
        response = self.client.get(
            reverse('finances:create_donation')
        )

        self.assertEqual(response.status_code, 403)

    def test_donation_create_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_donation')
        )

        self.assertContains(response, _('Create donation'), status_code=200)

    def test_donation_create_view_post_wrong_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'name': 'Printing',
            'amount': '4.55',
            'comment': 'Invitations',
            'payee': self.member3.pk
        }
        response = self.client.post(
            reverse('finances:create_donation'),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Donation.objects.all().count(), 2)

    def test_donation_create_view_post(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'name': 'Printing',
            'amount': '4.55',
            'comment': 'Invitations',
            'payee': self.member1.pk
        }
        response = self.client.post(
            reverse('finances:create_donation'),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Printing', status_code=200)
        self.assertEqual(models.Donation.objects.all().count(), 3)

    def test_donation_update_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:edit_donation', kwargs={
                'pk': self.donation1.pk
            })
        )

        self.assertContains(response, _('Edit donation'), status_code=200)

    def test_donation_update_view_post_wrong_donation_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'name': 'Printing',
            'amount': '4.55',
            'comment': 'Invitations',
            'payee': self.member1.pk
        }
        response = self.client.post(
            reverse('finances:edit_donation', kwargs={
                'pk': self.donation2.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 404)

    def test_donation_update_view_post_wrong_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'name': 'Printing',
            'amount': '4.55',
            'comment': 'Invitations',
            'payee': self.member3.pk
        }
        response = self.client.post(
            reverse('finances:edit_donation', kwargs={
                'pk': self.donation1.pk
            }),
            post_data,
            follow=True
        )
        self.donation1.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.donation1.payee, self.member1)

    def test_donation_update_view_post(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'name': 'Printing',
            'amount': '4.55',
            'comment': 'Invitations',
            'payee': self.member1.pk
        }
        response = self.client.post(
            reverse('finances:edit_donation', kwargs={
                'pk': self.donation1.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Invitations', status_code=200)
        self.assertEqual(models.Donation.objects.all().count(), 2)

    def test_donation_delete_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:delete_donation', kwargs={
                'pk': self.donation1.pk
            })
        )

        self.assertContains(response, _('Delete donation'), status_code=200)

    def test_donation_delete_view_post_wrong_donation_not_allowed(self):
        self.settlement2.delete()

        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('finances:delete_donation', kwargs={
                'pk': self.donation2.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(models.Donation.objects.all().count(), 2)

    def test_donation_delete_view_post(self):
        self.settlement2.delete()

        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('finances:delete_donation', kwargs={
                'pk': self.donation1.pk
            }),
            follow=True
        )

        self.assertNotContains(response, 'Printing', status_code=200)
        self.assertEqual(models.Donation.objects.all().count(), 1)

    def test_travelingcost_create_view_not_allowed(self):
        response = self.client.get(
            reverse('finances:create_travelingcost')
        )

        self.assertEqual(response.status_code, 403)

    def test_travelingcost_create_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:create_travelingcost')
        )

        self.assertContains(response, _('Create traveling cost'), status_code=200)

    def test_travelingcost_create_view_post_wrong_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'kilometers': 106,
            'from_place': 'Hamburg',
            'to_place': 'Berlin',
            'comment': 'Vacation',
            'payee': self.member3.pk
        }
        response = self.client.post(
            reverse('finances:create_travelingcost'),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.TravelingCost.objects.all().count(), 2)

    def test_travelingcost_create_view_post(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'kilometers': 106,
            'from_place': 'Hamburg',
            'to_place': 'Berlin',
            'comment': 'Vacation',
            'payee': self.member1.pk
        }
        response = self.client.post(
            reverse('finances:create_travelingcost'),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Hamburg', status_code=200)
        self.assertEqual(models.TravelingCost.objects.all().count(), 3)

    def test_travelingcost_update_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:edit_travelingcost', kwargs={
                'pk': self.traveling_costs1.pk
            })
        )

        self.assertContains(response, _('Edit traveling cost'), status_code=200)

    def test_travelingcost_update_view_post_wrong_travelingcost_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'kilometers': 106,
            'from_place': 'Hamburg',
            'to_place': 'Berlin',
            'comment': 'Vacation',
            'payee': self.member1.pk
        }
        response = self.client.post(
            reverse('finances:edit_travelingcost', kwargs={
                'pk': self.traveling_costs2.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 404)

    def test_travelingcost_update_view_post_wrong_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'kilometers': 106,
            'from_place': 'Hamburg',
            'to_place': 'Berlin',
            'comment': 'Vacation',
            'payee': self.member3.pk
        }
        response = self.client.post(
            reverse('finances:edit_travelingcost', kwargs={
                'pk': self.traveling_costs1.pk
            }),
            post_data,
            follow=True
        )
        self.traveling_costs1.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.traveling_costs1.payee, self.member1)

    def test_travelingcost_update_view_post(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'date': '14.02.2019',
            'kilometers': 106,
            'from_place': 'Hamburg',
            'to_place': 'Berlin',
            'comment': 'Vacation',
            'payee': self.member1.pk
        }
        response = self.client.post(
            reverse('finances:edit_travelingcost', kwargs={
                'pk': self.traveling_costs1.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Hamburg', status_code=200)
        self.assertEqual(models.TravelingCost.objects.all().count(), 2)

    def test_travelingcost_delete_view_get(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('finances:delete_travelingcost', kwargs={
                'pk': self.traveling_costs1.pk
            })
        )

        self.assertContains(response, _('Delete traveling cost'), status_code=200)

    def test_travelingcost_delete_view_post_wrong_travelingcost_not_allowed(self):
        self.settlement2.delete()

        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('finances:delete_travelingcost', kwargs={
                'pk': self.traveling_costs2.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(models.TravelingCost.objects.all().count(), 2)

    def test_travelingcost_delete_view_post(self):
        self.settlement2.delete()

        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('finances:delete_travelingcost', kwargs={
                'pk': self.traveling_costs1.pk
            }),
            follow=True
        )

        self.assertNotContains(response, 'Rothmannsthal', status_code=200)
        self.assertEqual(models.TravelingCost.objects.all().count(), 1)
