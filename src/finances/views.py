from datetime import datetime
from decimal import Decimal
import csv
import codecs

from . import admin, filters, forms, models, tables, utils
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic.base import View, TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.db.models import Sum
from django_filters.views import FilterView
from django.core.exceptions import PermissionDenied
from django_tables2 import SingleTableView, SingleTableMixin, MultiTableMixin
from easy_pdf.views import PDFTemplateView
from rolepermissions.checkers import has_role
from rolepermissions.mixins import HasRoleMixin
from import_export.formats import base_formats
from django_addanother.views import CreatePopupMixin
from easy_pdf.views import PDFTemplateView
from events.models import Event, Participation
from members.models import Member
from authentication import roles

# Create your views here.


class DebitListView(LoginRequiredMixin, MultiTableMixin, FilterView):
    template_name = 'finances/debits.html'
    filterset_class = filters.DebitFilter
    strict = False

    def dispatch(self, request, *args, **kwargs):
        self.debit_group = utils.get_current_debit_group(request)
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_tables(self):
        if has_role(self.user, [roles.Board, roles.Cashier]):
            new_debits = self.object_list.filter(group=self.debit_group).select_related(
                'member',
                'mfc',
                'member__mandate_membership_fee',
                'member__mandate_participation_fee'
            )
            debit_groups = models.DebitGroup.objects.exclude(id=self.debit_group.id)
            return [
                tables.DebitTable(new_debits, True, False, orderable=True),
                tables.DebitGroupTable(debit_groups, orderable=True)
            ]
        else:
            new_debits = self.object_list.filter(
                group=self.debit_group,
                member__user=self.user
            ).select_related(
                'member',
                'mfc',
                'member__mandate_membership_fee',
                'member__mandate_participation_fee'
            )
            old_debits = self.object_list.filter(
                member__user=self.user
            ).exclude(
                group__id=self.debit_group.id
            )
            return [
                tables.DebitTable(new_debits, False, False, orderable=True),
                tables.DebitTable(old_debits, False, True, orderable=True)
            ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["group"] = self.debit_group
        return context


class OldDebitListView(LoginRequiredMixin, HasRoleMixin, SingleTableMixin, FilterView):
    allowed_roles = [roles.Board, roles.Cashier]
    template_name = 'finances/old_debits.html'
    filterset_class = filters.DebitFilter
    strict = False

    def dispatch(self, request, group, *args, **kwargs):
        self.debit_group = models.DebitGroup.objects.get(id=group)
        return super().dispatch(request, *args, **kwargs)

    def get_table(self):
        return tables.DebitTable(
            self.object_list.filter(group=self.debit_group),
            False,
            False,
            orderable=True
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["group"] = self.debit_group
        return context


class SettlementListView(LoginRequiredMixin, HasRoleMixin, MultiTableMixin, FilterView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    template_name = 'finances/settlements.html'
    filterset_class = filters.SettlementFilter
    strict = False

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_tables(self):
        if has_role(self.user, roles.Board) or has_role(self.user, roles.Cashier):
            open_settlements = models.Settlement.objects.filter(
                entry__isnull=True
            ).select_related(
                'payee',
                'entry',
                'contra_account'
            )
            closed_settlements = self.object_list.filter(
                entry__isnull=False
            ).select_related(
                'payee',
                'entry',
                'contra_account'
            ).order_by('-entry__date')
            return [
                tables.OpenSettlementTable(open_settlements, True, orderable=True),
                tables.BookedSettlementTable(closed_settlements, orderable=True)
            ]
        else:
            open_settlements = models.Settlement.objects.filter(
                entry__isnull=True,
                payee__user=self.user
            ).select_related(
                'payee',
                'entry',
                'contra_account'
            )
            closed_settlements = self.object_list.filter(
                entry__isnull=False,
                payee__user=self.user
            ).select_related(
                'payee',
                'entry',
                'contra_account'
            ).order_by('-entry__date')
            return [
                tables.OpenSettlementTable(open_settlements, False, orderable=True),
                tables.BookedSettlementTable(closed_settlements, orderable=True)
            ]


class EntryListView(LoginRequiredMixin, HasRoleMixin, MultiTableMixin, FilterView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    template_name = 'finances/entries.html'
    filterset_class = filters.EntryFilter
    strict = False
    table_pagination = {
        'per_page': 0
    }

    def dispatch(self, request, year, *args, **kwargs):
        self.user = request.user
        self.year = year
        return super().dispatch(request, *args, **kwargs)

    def get_tables(self):
        if has_role(self.user, roles.Board) or has_role(self.user, roles.Cashier):
            can_edit = True
        else:
            can_edit = False
        tabs = []
        for i in range(1, 13):
            tabs.append(
                tables.EntryTable(
                    self.object_list.filter(
                        date__month=i,
                        date__year=self.year
                    ).select_related(
                        'debit_account',
                        'credit_account'
                    ),
                    can_edit,
                    orderable=True
                )
            )
        return tabs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_year"] = int(self.year)
        years = [
            year.year for year in models.Entry.objects.dates('date', 'year')
        ] + [
            datetime.today().year
        ]
        years.sort()
        context["years"] = set(years)
        return context


class AccountListView(LoginRequiredMixin, HasRoleMixin, TemplateView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    template_name = 'finances/accounts.html'

    def dispatch(self, request, year, *args, **kwargs):
        self.year = year
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_year"] = int(self.year)
        context["years"] = set(
            list(models.Account.objects.order_by('year').values_list(
                'year',
                flat=True
            ).distinct()) +
            [datetime.today().year]
        )
        context["asset_accounts"] = models.Account.objects.filter(
            type=0,
            year=self.year
        ).prefetch_related(
            'debit',
            'credit'
        )
        context["pal_accounts"] = models.Account.objects.filter(
            type=1,
            year=self.year
        ).prefetch_related(
            'debit',
            'credit'
        )

        return context


class DonationListView(LoginRequiredMixin, HasRoleMixin, MultiTableMixin, TemplateView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    template_name = 'finances/donations.html'

    def dispatch(self, request, year, *args, **kwargs):
        self.user = request.user
        self.year = year
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        donation_years = list(
            models.Donation.objects.filter(payee__user=self.user).dates('date', 'year')
        )
        traveling_years = list(
            models.TravelingCost.objects.filter(payee__user=self.user).dates('date', 'year')
        )
        context['current_year'] = int(self.year)
        years = [
            year.year for year in donation_years
        ] + [
            year.year for year in traveling_years
        ] + [
            datetime.today().year
        ]
        years.sort()
        context['years'] = set(years)
        return context

    def get_tables(self):
        return [
            tables.DonationTable(models.Donation.objects.filter(
                date__year=self.year,
                payee__user=self.user
            ), orderable=True),
            tables.TravelingCostTable(models.TravelingCost.objects.filter(
                date__year=self.year,
                payee__user=self.user
            ), orderable=True)
        ]


class DonationExportView(LoginRequiredMixin, HasRoleMixin, PDFTemplateView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    template_name = 'finances/donation_receipt.html'

    def dispatch(self, request, year, *args, **kwargs):
        self.user = request.user
        self.year = year
        return super().dispatch(request, *args, **kwargs)

    def get_pdf_filename(self):
        return _('donation_receipt_%s.pdf') % datetime.now().strftime('%d-%m-%Y')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        donations = models.Donation.objects.filter(
            date__year=self.year,
            payee__user=self.user
        )
        travelingCosts = models.TravelingCost.objects.filter(
            date__year=self.year,
            payee__user=self.user
        )
        sum_donations = donations.aggregate(Sum('amount'))['amount__sum']
        sum_traveling = Decimal(travelingCosts.aggregate(
            Sum('kilometers')
        )['kilometers__sum'] or '0') * Decimal('0.3')
        if sum_donations is None:
            sum_donations = 0
        if sum_traveling is None:
            sum_traveling = 0
        if travelingCosts.first() is not None:
            payee = travelingCosts.first().payee
        elif donations.first() is not None:
            payee = donations.first().payee
        else:
            return context 

        context['member'] = payee
        context['today'] = datetime.today()
        context['donations'] = donations
        context['sum_donations'] = sum_donations
        context['travelingCosts'] = travelingCosts
        context['sum_traveling'] = sum_traveling
        context['sum'] = sum_donations + sum_traveling

        return context


class DebitCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    """
    Allows Cashier and Board members to create one single debit.
    
    The current debit group is automatically assigned. 
    As there is no related MembershipFeeCollection the debit counts as particpation fee.
    """

    allowed_roles = [roles.Board, roles.Cashier]
    form_class = forms.DebitForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.debit_group = utils.get_current_debit_group(request)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.group = self.debit_group
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('finances:debits')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create debit')
        return context


class DebitUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Cashier]
    form_class = forms.DebitForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.group = utils.get_current_debit_group(request)
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, *args, **kwargs):
        debit = super().get_object(*args, **kwargs)
        if debit.group.to_date < datetime.today().date():
            raise PermissionDenied
        return debit

    def form_valid(self, form):
        form.instance.group = self.group
        return super().form_valid(form)

    def get_queryset(self):
        return models.Debit.objects.all()

    def get_success_url(self):
        return reverse('finances:debits')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit debit')
        return context


class DebitDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Cashier]
    model = models.Debit
    success_url = reverse_lazy('finances:debits')

    def get_object(self, *args, **kwargs):
        debit = super().get_object(*args, **kwargs)
        if debit.group.to_date < datetime.today().date():
            raise PermissionDenied
        return debit


class DebitsExportView(LoginRequiredMixin, HasRoleMixin, View):
    allowed_roles = [roles.Board, roles.Cashier]

    def dispatch(self, request, group, *args, **kwargs):
        self.debit_group = models.DebitGroup.objects.get(id=group)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        qs = models.Debit.objects.filter(group=self.debit_group)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = ('attachment; filename="SEPA-%s.csv"'
                                           % self.debit_group.execution_date.strftime('%d-%m-%Y'))

        dataset = admin.DebitResource().export(queryset=qs)
        response.write(codecs.BOM_UTF8)
        response.write(base_formats.CSV().export_data(
            dataset,
            delimiter=';',
            quoting=csv.QUOTE_NONE)
        )

        return response


class MembershipFeeCollectionCreateView(LoginRequiredMixin, HasRoleMixin, SingleTableView):
    """
    Cashier, Secretary or Board members can create debits for periodical membership fee collections.

    The year and a sequential number for the MFC have to be given in the form.
    Individual members can be excluded from the collection using the table.
    A MembershipFeeCollection object is created which is related to the debits (making them use the membership fee mandate).
    """

    allowed_roles = [roles.Board, roles.Cashier, roles.Secretary]
    queryset = Member.objects.all().select_related(
        'section'
    ).select_related(
        'mandate_membership_fee'
    )
    table_class = tables.MembershipFeeCollectionCreateTable
    template_name = 'finances/create_mfc.html'
    table_pagination = False

    def dispatch(self, request, *args, **kwargs):
        self.group = utils.get_current_debit_group(request)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = forms.MembershipFeeCollectionCreateForm()
        return context

    def post(self, request):
        form = forms.MembershipFeeCollectionCreateForm(request.POST)
        if form.is_valid():
            mfc = models.MembershipFeeCollection.objects.create(
                year=form.cleaned_data['year'],
                nr=form.cleaned_data['nr']
            )
            create = request.POST.getlist('create')
            qs = Member.objects.filter(pk__in=create)
            models.Debit.objects.bulk_create([models.Debit(
                member=member,
                amount=member.fee,
                reference=_('Membership fee %(year)s/%(nr)s') % {
                    'year': form.cleaned_data['year'],
                    'nr': form.cleaned_data['nr']
                },
                group=self.group,
                mfc=mfc) for member in qs]
            )
            return redirect('members:mfc')

        return super().post(request)


class EventDebitCreateView(LoginRequiredMixin, HasRoleMixin, SingleTableView):
    allowed_roles = [roles.Board]
    table_class = tables.EventDebitCreateTable
    template_name = 'finances/create_event_debits.html'
    table_pagination = False

    def dispatch(self, request, event, *args, **kwargs):
        self.event = get_object_or_404(Event, pk=event)
        self.group = utils.get_current_debit_group(request)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return Participation.objects.filter(event=self.event, debit__isnull=True).select_related(
            'member'
        ).select_related(
            'event'
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['event'] = self.event.name
        return context

    def post(self, request):
        create = request.POST.getlist('create')
        qs = Participation.objects.filter(pk__in=create)
        models.Debit.objects.bulk_create([models.Debit(
            member=participation.member,
            amount=self.event.fee,
            reference=_('Participation fee %s') % self.event.name,
            group=self.group,
            participation=participation) for participation in qs]
        )
        return redirect(reverse('events:details', kwargs={'pk': self.event.pk}))


class EntryCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.Cashier]
    form_class = forms.EntryForm
    template_name = 'finances/form.html'

    def get_success_url(self):
        return reverse('finances:entries', kwargs={'year': self.object.date.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create entry')
        return context


class EntryUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Cashier]
    form_class = forms.EntryForm
    template_name = 'finances/form.html'

    def get_success_url(self):
        return reverse('finances:entries', kwargs={'year': self.object.date.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit entry')
        return context

    def get_queryset(self):
        return models.Entry.objects.all()


class EntryDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Cashier]
    model = models.Entry

    def get_success_url(self):
        return reverse('finances:entries', kwargs={'year': self.object.date.year})


class AccountCreateView(LoginRequiredMixin, HasRoleMixin, CreatePopupMixin, CreateView):
    allowed_roles = [roles.Board, roles.Cashier]
    form_class = forms.AccountCreateForm
    template_name = 'finances/form.html'

    def form_valid(self, form):
        self.year = form.cleaned_data['year']
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('finances:accounts', kwargs={'year': self.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create account')
        return context


class ContraAccountCreateView(LoginRequiredMixin, HasRoleMixin, CreatePopupMixin, CreateView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    form_class = forms.ContraAccountForm
    template_name = 'finances/form.html'

    def form_valid(self, form):
        self.year = form.cleaned_data['year']
        contra_account = form.save(commit=False)
        contra_account.type = 1
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('finances:accounts', kwargs={'year': self.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create contra account')
        return context


class AccountUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Cashier]
    form_class = forms.AccountUpdateForm
    template_name = 'finances/form.html'

    def form_valid(self, form):
        self.year = form.cleaned_data['year']
        return super().form_valid(form)

    def get_queryset(self):
        return models.Account.objects.all()

    def get_success_url(self):
        return reverse('finances:accounts', kwargs={'year': self.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit account')
        return context


class AccountCloseView(LoginRequiredMixin, HasRoleMixin, SingleTableView):
    allowed_roles = [roles.Board, roles.Cashier]
    template_name = 'finances/close_accounts.html'
    table_class = tables.AccountTable
    table_pagination = False

    def dispatch(self, request, year, *args, **kwargs):
        self.year = year
        self.accounts = models.Account.objects.filter(year=self.year, closed=False)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.accounts

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['year'] = self.year
        context['accounts'] = self.accounts
        return context

    def post(self, request):
        for a in self.accounts:
            a.closed = True
            a.save()
        return redirect(reverse('finances:accounts', kwargs={'year': self.year}))


class EntryExportView(LoginRequiredMixin, HasRoleMixin, View):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]

    def dispatch(self, request, year, *args, **kwargs):
        self.year = year
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = (_('attachment; filename="Entries-%s.csv"')
                                           % self.year)
        response.write(codecs.BOM_UTF8)
        dataset = admin.EntryResource().export(
            queryset=models.Entry.objects.filter(date__year=self.year)
        )
        response.write(dataset.csv)

        return response


class VoucherExportView(LoginRequiredMixin, HasRoleMixin, PDFTemplateView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    template_name = 'finances/vouchers.html'

    def dispatch(self, request, year, *args, **kwargs):
        self.year = year
        return super().dispatch(request, *args, **kwargs)

    def get_pdf_filename(self):
        return _('Vouchers-%s.pdf') % self.year

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entries'] = models.Entry.objects.filter(date__year=self.year).order_by('id')
        return context


class EventVoucherExportView(LoginRequiredMixin, HasRoleMixin, PDFTemplateView):
    allowed_roles = [roles.Board, roles.Leader]
    template_name = 'finances/vouchers.html'

    def dispatch(self, request, event, *args, **kwargs):
        self.event = Event.objects.get(pk=event)
        return super().dispatch(request, *args, **kwargs)

    def get_pdf_filename(self):
        return _('%(event)s-Vouchers-%(date)s.pdf') % {
            'event': self.event,
            'date': datetime.now().strftime('%Y-%m-%d')
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entries'] = models.Entry.objects.filter(
            debit_account__event=self.event
        ).order_by('id')
        return context


class AccountExportView(LoginRequiredMixin, HasRoleMixin, View):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]

    def dispatch(self, request, year, *args, **kwargs):
        self.year = year
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = (_('attachment; filename="Accounts-%s.csv"')
                                           % self.year)
        response.write(codecs.BOM_UTF8)

        accounts = models.Account.objects.filter(year=self.year)
        for a in accounts:
            response.write('%s\n' % a.name)
            response.write('%s,%s,%s,%s,%s,%s,%s\n' % (
                _('date'),
                _('no.'),
                _('debit amount'),
                _('credit amount'),
                _('contra account'),
                _('comment'),
                _('voucher'),
            ))
            for t, e in a.get_entries_with_type:
                if e.voucher is None:
                    e.voucher = ''
                if t == 'debit':
                    response.write('%s,%s,"%s",,%s,%s,%s\n' % (
                        e.date.strftime('%d.%m.%Y'),
                        e.id,
                        str(e.amount).replace('.', ','),
                        e.credit_account,
                        e.comment,
                        e.voucher
                    ))
                else:
                    response.write('%s,%s,,"%s",%s,%s,%s\n' % (
                        e.date.strftime('%d.%m.%Y'),
                        e.id,
                        str(e.amount).replace('.', ','),
                        e.debit_account,
                        e.comment,
                        e.voucher
                    ))
            t, b = a.get_balance_with_type
            response.write('%s,"%s",%s\n' % (
                _('Balance'),
                str(b).replace('.', ','),
                _(t.capitalize())
            ))
            response.write('\n')

        return response


class SettlementCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    form_class = forms.SettlementForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.user
        return kwargs

    def get_success_url(self):
        return reverse('finances:settlements')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create settlement')
        return context

    def form_valid(self, form):
        result = super().form_valid(form)
        utils.notify_cashier_settlement(self.request, self.object)
        return result


class SettlementUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    form_class = forms.SettlementForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.user
        return kwargs

    def get_queryset(self):
        if has_role(self.user, roles.Board) or has_role(self.user, roles.Cashier):
            return models.Settlement.objects.filter(entry__isnull=True)
        else:
            return models.Settlement.objects.filter(payee__user=self.user, entry__isnull=True)

    def get_success_url(self):
        return reverse('finances:settlements')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit settlement')
        return context


class SettlementDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Cashier, roles.Leader]
    form_class = forms.SettlementForm
    success_url = reverse_lazy('finances:settlements')

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if has_role(self.user, roles.Board) or has_role(self.user, roles.Cashier):
            return models.Settlement.objects.filter(entry__isnull=True)
        else:
            return models.Settlement.objects.filter(payee__user=self.user, entry__isnull=True)


class SettlementBookView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.Cashier]
    form_class = forms.EntryForm
    template_name = 'finances/form.html'

    def dispatch(self, request, pk, *args, **kwargs):
        self.settlement = models.Settlement.objects.get(pk=pk)
        if self.settlement.entry is not None:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        account = models.Account.objects.filter(closed=False, type=0).first()
        return {
            'date': datetime.today(),
            'amount': self.settlement.amount,
            'voucher': self.settlement.voucher,
            'comment': self.settlement.comment,
            'debit_account': self.settlement.contra_account,
            'credit_account': account,
        }

    def form_valid(self, form):
        result = super().form_valid(form)
        self.settlement.entry = self.object
        self.settlement.save()
        utils.notify_leader_settlement(self.request, self.settlement)
        return result

    def get_success_url(self):
        return reverse('finances:entries', kwargs={'year': self.object.date.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create entry')
        return context


class EventAccountCreateView(LoginRequiredMixin, HasRoleMixin, View):
    allowed_roles = [roles.Board, roles.Leader]

    def dispatch(self, request, event, *args, **kwargs):
        self.event = Event.objects.get(pk=event)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request):
        account = models.Account.objects.create(
            name=self.event.name,
            type=1,
            year=self.event.from_date.year
        )
        self.event.account = account
        self.event.save()
        return redirect(reverse('events:details', kwargs={'pk': self.event.pk}))


class DonationCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    form_class = forms.DonationForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.user
        return kwargs

    def form_valid(self, form):
        self.year = form.cleaned_data['date'].year
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('finances:donations', kwargs={'year': self.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create donation')
        return context


class DonationUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    form_class = forms.DonationForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.user
        return kwargs

    def get_queryset(self):
        return models.Donation.objects.filter(payee__user=self.user)

    def form_valid(self, form):
        self.year = form.cleaned_data['date'].year
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('finances:donations', kwargs={'year': self.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit donation')
        return context


class DonationDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    model = models.Donation

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return models.Donation.objects.filter(payee__user=self.user)

    def get_success_url(self):
        return reverse_lazy('finances:donations', kwargs={'year': self.object.date.year})


class TravelingCostCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    form_class = forms.TravelingCostForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.user
        return kwargs

    def form_valid(self, form):
        self.year = form.cleaned_data['date'].year
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('finances:donations', kwargs={'year': self.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create traveling cost')
        return context


class TravelingCostUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    form_class = forms.TravelingCostForm
    template_name = 'finances/form.html'

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.user
        return kwargs

    def get_queryset(self):
        return models.TravelingCost.objects.filter(payee__user=self.user)

    def form_valid(self, form):
        self.year = form.cleaned_data['date'].year
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('finances:donations', kwargs={'year': self.year})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit traveling cost')
        return context


class TravelingCostDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Leader, roles.Cashier]
    model = models.TravelingCost

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return models.TravelingCost.objects.filter(payee__user=self.user)

    def get_success_url(self):
        return reverse_lazy('finances:donations', kwargs={'year': self.object.date.year})
