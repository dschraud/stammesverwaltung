from datetime import datetime
from django.core.mail import EmailMultiAlternatives
from django.core.exceptions import ObjectDoesNotExist
from django.template import loader
from django.contrib.sites.shortcuts import get_current_site
from rolepermissions.checkers import has_role
from django.db.models import Count, Sum
from django.conf import settings

from . import models
from authentication import roles
from authentication.models import SVUser, CashierEmailSettings, LeaderEmailSettings


def get_debit_dates():
    today = datetime.today()
    to = today.replace(day=10)
    if today.day > 10:
        if today.month == 12:
            to = to.replace(year=today.year+1)
            to = to.replace(month=1)
        else:
            to = to.replace(month=today.month+1)

    if to.month == 1:
        fromd = to.replace(year=to.year-1)
        fromd = fromd.replace(month=12)
    else:
        fromd = to.replace(month=to.month-1)
        fromd = fromd.replace(day=11)

    execute = to.replace(day=15)

    return (fromd, to, execute)


def get_current_debit_group(request):
    try:
        debit_group = models.DebitGroup.objects.get(
            from_date__lte=datetime.today(),
            to_date__gte=datetime.today()
        )
    except ObjectDoesNotExist:
        fromd, to, execute = get_debit_dates()
        debit_group = models.DebitGroup.objects.create(
            from_date=fromd,
            to_date=to,
            execution_date=execute
        )

        old_debits = [d for d in models.Debit.objects.exclude(
            group=debit_group
        ) if d.mandate() is None]
        for od in old_debits:
            od.group = debit_group
            od.save()

        notify_debits(request)
    return debit_group


class DebitsCashierEmail(EmailMultiAlternatives):
    template_name = 'finances/debits_cashier_email.txt'
    subject_template_name = 'finances/debits_cashier_email_subject.txt'
    html_template_name = 'finances/debits_cashier_email.html'
    from_email = 'Stammesverwaltung DPSG St. Vitus Hirschaid <sv@pfadfinder-hirschaid.de>'

    def __init__(self, user, domain, debit_group):
        context = {
            'first_name': user.first_name,
            'domain': domain,
            'protocol': 'https',
            'group': debit_group,
            'execution_date': debit_group.execution_date,
            'year': datetime.today().year,
            'debug': settings.DEBUG,
        }
        subject = loader.render_to_string(self.subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(self.template_name, context)
        html = loader.render_to_string(self.html_template_name, context)

        super().__init__(
            subject=subject,
            body=body,
            from_email=self.from_email,
            to=[user.email],
        )
        self.attach_alternative(html, 'text/html')


class DebitsMemberEmail(EmailMultiAlternatives):
    template_name = 'finances/debits_member_email.txt'
    subject_template_name = 'finances/debits_member_email_subject.txt'
    html_template_name = 'finances/debits_member_email.html'
    from_email = 'Stammesverwaltung DPSG St. Vitus Hirschaid <sv@pfadfinder-hirschaid.de>'

    def __init__(self, user, domain, debits):
        sum = debits.aggregate(Sum('amount'))['amount__sum']
        count = debits.aggregate(Count('id'))['id__count']
        user = SVUser.objects.get(id=user)
        context = {
            'first_name': user.first_name,
            'domain': domain,
            'protocol': 'https',
            'sum': sum,
            'count': count,
            'execution_date': debits.first().group.execution_date,
            'year': datetime.today().year,
            'debug': settings.DEBUG,
        }
        subject = loader.render_to_string(self.subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(self.template_name, context)
        html = loader.render_to_string(self.html_template_name, context)

        super().__init__(
            subject=subject,
            body=body,
            from_email=self.from_email,
            to=[user.email],
        )
        self.attach_alternative(html, 'text/html')


class SettlementCashierEmail(EmailMultiAlternatives):
    template_name = 'finances/settlement_cashier_email.txt'
    subject_template_name = 'finances/settlement_cashier_email_subject.txt'
    html_template_name = 'finances/settlement_cashier_email.html'
    from_email = 'Stammesverwaltung DPSG St. Vitus Hirschaid <sv@pfadfinder-hirschaid.de>'

    def __init__(self, cashier, domain, settlement):
        context = {
            'first_name': cashier.first_name,
            'domain': domain,
            'protocol': 'https',
            'user': settlement.payee.user,
            'comment': settlement.comment,
            'year': datetime.today().year,
            'debug': settings.DEBUG,
        }
        subject = loader.render_to_string(self.subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(self.template_name, context)
        html = loader.render_to_string(self.html_template_name, context)

        super().__init__(
            subject=subject,
            body=body,
            from_email=self.from_email,
            to=[cashier.email],
        )
        self.attach_alternative(html, 'text/html')


class SettlementLeaderEmail(EmailMultiAlternatives):
    template_name = 'finances/settlement_leader_email.txt'
    subject_template_name = 'finances/settlement_leader_email_subject.txt'
    html_template_name = 'finances/settlement_leader_email.html'
    from_email = 'Stammesverwaltung DPSG St. Vitus Hirschaid <sv@pfadfinder-hirschaid.de>'

    def __init__(self, user, domain, settlement):
        context = {
            'first_name': user.first_name,
            'domain': domain,
            'protocol': 'https',
            'user': settlement.payee.user,
            'comment': settlement.comment,
            'year': datetime.today().year,
            'debug': settings.DEBUG,
        }
        subject = loader.render_to_string(self.subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(self.template_name, context)
        html = loader.render_to_string(self.html_template_name, context)

        super().__init__(
            subject=subject,
            body=body,
            from_email=self.from_email,
            to=[user.email],
        )
        self.attach_alternative(html, 'text/html')


def notify_debits(request):
    domain = get_current_site(request).domain
    todo = models.DebitGroup.objects.filter(notified=False, to_date__lt=datetime.today())
    cashiers = [u for u in SVUser.objects.all() if has_role(
        u, [roles.Board, roles.Cashier]
    ) and CashierEmailSettings.objects.get_or_create(user=u)[0].debit_available]
    for group in todo:
        if group.debit_set.count() != 0:
            for c in cashiers:
                DebitsCashierEmail(c, domain, group).send()

            user = group.debit_set.values_list('member__user', flat=True).distinct()
            for u in user:
                if u is not None:
                    debits = group.debit_set.filter(member__user=u)
                    DebitsMemberEmail(u, domain, debits).send()
        group.notified = True
        group.save()


def notify_cashier_settlement(request, settlement):
    domain = get_current_site(request).domain
    cashiers = [u for u in SVUser.objects.all() if has_role(
        u, [roles.Board, roles.Cashier]
    ) and CashierEmailSettings.objects.get_or_create(user=u)[0].settlement_created]

    for c in cashiers:
        SettlementCashierEmail(c, domain, settlement).send()


def notify_leader_settlement(request, settlement):
    user = settlement.payee.user
    if LeaderEmailSettings.objects.get_or_create(user=user)[0].settlement_completed:
        domain = get_current_site(request).domain
        SettlementLeaderEmail(user, domain, settlement).send()
