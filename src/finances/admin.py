from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.fields import Field
from django.utils.translation import gettext_lazy as _

from . import models

# Register your models here.

admin.site.register(models.DebitGroup)
admin.site.register(models.MembershipFeeCollection)
admin.site.register(models.Settlement)
admin.site.register(models.Donation)
admin.site.register(models.TravelingCost)
admin.site.register(models.Account)


class DebitResource(resources.ModelResource):
    label = Field(attribute='member', column_name='Bezeichnung der Basislastschrift')
    depositor = Field(column_name='Zahlungspflichtiger')
    address = Field(column_name='Adresse')
    country_code = Field(column_name='Länder-Kennzeichen')
    country = Field(column_name='Land')
    iban = Field(column_name='IBAN des Zahlungspflichtigen')
    bic = Field(column_name='BIC')
    bank = Field(column_name='Bei Kreditinstitut')
    amount = Field(attribute='amount', column_name='Betrag')
    reference1 = Field(attribute='reference', column_name='Verwendungszweck 1')
    reference2 = Field(attribute='member', column_name='Verwendungszweck 2')
    iban_dpsg = Field(column_name='IBAN des Zahlungsempfängers')
    recipient = Field(column_name='Zahlungsempfänger')
    man_ref = Field(column_name='Mandatsreferenz')
    date_mandate = Field(column_name='Unterschrieben am')
    creditor_id = Field(column_name='Gläubiger ID des Zahlungsempfängers')

    def dehydrate_depositor(self, debit):
        if debit.mfc is not None:
            return debit.member.mandate_membership_fee.depositor
        else:
            return debit.member.mandate_participation_fee.depositor

    def dehydrate_iban(self, debit):
        if debit.mfc is not None:
            return debit.member.mandate_membership_fee.iban
        else:
            return debit.member.mandate_participation_fee.iban

    def dehydrate_bic(self, debit):
        if debit.mfc is not None:
            return debit.member.mandate_membership_fee.bic
        else:
            return debit.member.mandate_participation_fee.bic

    def dehydrate_bank(self, debit):
        if debit.mfc is not None:
            return debit.member.mandate_membership_fee.bank
        else:
            return debit.member.mandate_participation_fee.bank

    def dehydrate_man_ref(self, debit):
        if debit.mfc is not None:
            return debit.member.mandate_membership_fee.reference
        else:
            return debit.member.mandate_participation_fee.reference

    def dehydrate_date_mandate(self, debit):
        if debit.mfc is not None:
            return debit.member.mandate_membership_fee.date.strftime('%d.%m.%Y')
        else:
            return debit.member.mandate_participation_fee.date.strftime('%d.%m.%Y')

    def dehydrate_iban_dpsg(self, debit):
        return 'DE87763910000004116631'

    def dehydrate_recipient(self, debit):
        return 'Deutsche Pfadfinderschaft St. Georg'

    def dehydrate_creditor_id(self, debit):
        return 'DE10HIR00000527857'

    def dehydrate_amount(self, debit):
        return str(debit.amount).replace('.', ',')

    class Meta:
        model = models.Debit
        fields = (
            'bezeichnung',
            'kontoinhaber',
            'adresse',
            'land_kennz',
            'land',
            'iban',
            'bic',
            'bank',
            'betrag',
            'verwendungszweck1',
            'verwendungszweck2',
            'iban_dpsg',
            'empfaenger',
            'man_ref',
            'datum_mandat',
            'glaeubiger_id'
        )


class EntryResource(resources.ModelResource):
    date = Field(attribute='date', column_name=_('date'))
    nr = Field(attribute='id', column_name=_('no.'))
    amount = Field(attribute='amount', column_name=_('amount'))
    debit_account = Field(attribute='debit_account__name', column_name=_('debit account'))
    credit_account = Field(attribute='credit_account__name', column_name=_('credit account'))
    comment = Field(attribute='comment', column_name=_('comment'))

    def dehydrate_date(self, entry):
        return entry.date.strftime('%d.%m.%Y')

    def dehydrate_amount(self, entry):
        return str(entry.amount).replace('.', ',')

    class Meta:
        model = models.Entry
        exclude = ('id', 'voucher')


@admin.register(models.Debit)
class DebitAdmin(ImportExportModelAdmin):
    resource_class = DebitResource


@admin.register(models.Entry)
class EntryAdmin(ImportExportModelAdmin):
    resource_class = EntryResource
