from django.db import models
from django.db.models import Count, Sum
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from decimal import Decimal

from members.models import Member
from authentication.models import BankDetails
from events.models import Participation

# Create your models here.

ACCOUNT_TYPES = (
    (0, _('Asset account')),
    (1, _('Profit and loss account')),
)


class MembershipFeeCollection(models.Model):
    year = models.IntegerField(_('Year'))
    nr = models.IntegerField(_('Nr.'))

    def __str__(self):
        return '%s/%s' % (self.year, self.nr)

    def reference(self):
        if self.debit_set.exists():
            return self.debit_set.first().reference

    reference.short_description = _('Reference')

    def count(self):
        return self.debit_set.aggregate(Count('id'))['id__count']

    count.short_description = _('Count')

    def sum(self):
        return self.debit_set.aggregate(Sum('amount'))['amount__sum']

    sum.short_description = _('Sum')

    class Meta:
        verbose_name = _('Membership fee collection')
        verbose_name_plural = _('Membership fee collections')
        ordering = ['year', 'nr']


class DebitGroup(models.Model):
    from_date = models.DateField(_('from'))
    to_date = models.DateField(_('to'))
    execution_date = models.DateField(_('execution date'))
    notified = models.BooleanField(_('notified'), default=False)

    def __str__(self):
        return str(self.execution_date)

    def count(self):
        return self.debit_set.aggregate(Count('id'))['id__count']

    count.short_description = _('Count')

    def sum(self):
        return self.debit_set.aggregate(Sum('amount'))['amount__sum'] or Decimal('0')

    sum.short_description = _('Sum')

    class Meta:
        verbose_name = _('Debit group')
        verbose_name_plural = _('Debit groups')
        ordering = ['-to_date']


class Debit(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE, verbose_name=_('member'))
    amount = models.DecimalField(_('amount'), max_digits=5, decimal_places=2)
    reference = models.CharField(_('reference'), max_length=100)
    group = models.ForeignKey(DebitGroup, on_delete=models.PROTECT, verbose_name=_('group'))
    mfc = models.ForeignKey(
        MembershipFeeCollection,
        on_delete=models.CASCADE,
        null=True, blank=True
    )
    participation = models.ForeignKey(
        Participation,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return '%s (%s)' % (self.reference, self.member)

    def mandate(self):
        if self.mfc is not None:
            return self.member.mandate_membership_fee
        else:
            return self.member.mandate_participation_fee

    mandate.short_description = _('Mandate')

    class Meta:
        verbose_name = _('debit')
        verbose_name_plural = _('debits')
        ordering = ['member', 'reference']


class Account(models.Model):
    name = models.CharField(_('name'), max_length=50)
    type = models.IntegerField(_('type'), choices=ACCOUNT_TYPES)
    year = models.IntegerField(_('year'))
    closed = models.BooleanField(_('closed'), default=False)

    def __str__(self):
        return '%s %s' % (self.name, self.year)

    @cached_property
    def get_entries_with_type(self):
        result = list()
        debits = self.debit.all().select_related(
            'debit_account',
            'credit_account'
        )
        for debit in debits:
            result.append(('debit', debit))
        credits = self.credit.all().select_related(
            'debit_account',
            'credit_account'
        )
        for credit in credits:
            result.append(('credit', credit))
        return result

    @cached_property
    def get_balance_with_type(self):
        debit_sum = 0
        debits = self.debit.all()
        for debit in debits:
            debit_sum = debit_sum + debit.amount
        credit_sum = 0
        credits = self.credit.all()
        for credit in credits:
            credit_sum = credit_sum + credit.amount
        if debit_sum > credit_sum:
            return ('debit', debit_sum - credit_sum)
        else:
            return ('credit', credit_sum - debit_sum)

    class Meta:
        verbose_name = _('Account')
        verbose_name_plural = _('Accounts')
        ordering = ['year', 'type', 'name']


class Entry(models.Model):
    date = models.DateField(_('date'))
    amount = models.DecimalField(_('amount'), max_digits=7, decimal_places=2)
    voucher = models.FileField(_('voucher'), null=True, blank=True)
    comment = models.CharField(_('comment'), max_length=100)
    debit_account = models.ForeignKey(
        Account,
        verbose_name=_('debit account'),
        on_delete=models.PROTECT,
        related_name='debit'
    )
    credit_account = models.ForeignKey(
        Account,
        verbose_name=_('credit account'),
        on_delete=models.PROTECT,
        related_name='credit'
    )

    def __str__(self):
        return '%s -> %s: %s (%s)' % (
            self.debit_account,
            self.credit_account,
            self.amount,
            self.comment
        )

    class Meta:
        verbose_name = _('Entry')
        verbose_name_plural = _('Entries')
        ordering = ['date', 'id']


class Settlement(models.Model):
    amount = models.DecimalField(_('amount'), max_digits=7, decimal_places=2)
    voucher = models.FileField(_('voucher'))
    comment = models.CharField(_('comment'), max_length=100)
    contra_account = models.ForeignKey(
        Account,
        verbose_name=_('contra account'),
        on_delete=models.PROTECT
    )
    payee = models.ForeignKey(
        BankDetails,
        verbose_name=_('payee'),
        on_delete=models.PROTECT
    )
    entry = models.ForeignKey(
        Entry,
        verbose_name=_('entry'),
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    def __str__(self):
        return '%s: %s (%s)' % (
            self.comment,
            self.amount,
            self.payee
        )

    class Meta:
        verbose_name = _('Settlement')
        verbose_name_plural = _('Settlements')
        ordering = ['payee', 'id']


class Donation(models.Model):
    date = models.DateField(_('date'))
    name = models.CharField(_('name'), max_length=100)
    amount = models.DecimalField(_('amount'), max_digits=7, decimal_places=2)
    comment = models.CharField(_('comment'), max_length=100)
    payee = models.ForeignKey(
        Member,
        verbose_name=_('payee'),
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '%s (%s)' % (
            self.name,
            self.payee
        )

    class Meta:
        verbose_name = _('Donation')
        verbose_name_plural = _('Donation')
        ordering = ['date', 'payee', 'id']


class TravelingCost(models.Model):
    date = models.DateField(_('date'))
    kilometers = models.IntegerField(_('kilometers'))
    from_place = models.CharField(_('from'), max_length=50)
    to_place = models.CharField(_('to'), max_length=50)
    comment = models.CharField(_('comment'), max_length=100)
    payee = models.ForeignKey(
        Member,
        verbose_name=_('payee'),
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '%s -> %s (%s)' % (
            self.from_place,
            self.to_place,
            self.payee
        )

    @property
    def amount(self):
        return self.kilometers * Decimal('0.3')

    class Meta:
        verbose_name = _('Traveling cost')
        verbose_name_plural = _('Traveling costs')
        ordering = ['date', 'payee', 'id']
