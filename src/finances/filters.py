from django_filters import FilterSet, CharFilter
from django.db.models import Q

from . import models


class DebitFilter(FilterSet):
    member_reference = CharFilter(method='filter_mr')

    def filter_mr(self, queryset, name, value):
        return queryset.filter(
            Q(member__first_name__icontains=value) |
            Q(member__last_name__icontains=value) |
            Q(reference__icontains=value)
        )

    class Meta:
        model = models.Debit
        fields = []


class SettlementFilter(FilterSet):
    comment_contra_account = CharFilter(method='filter_cca')

    def filter_cca(self, queryset, name, value):
        return queryset.filter(
            Q(comment__icontains=value) |
            Q(contra_account__name__icontains=value)
        )

    class Meta:
        model = models.Settlement
        fields = []


class EntryFilter(FilterSet):
    comment_account = CharFilter(method='filter_ca')

    def filter_ca(self, queryset, name, value):
        return queryset.filter(
            Q(comment__icontains=value) |
            Q(debit_account__name__icontains=value) |
            Q(credit_account__name__icontains=value)
        )

    class Meta:
        model = models.Entry
        fields = []
