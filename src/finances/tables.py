from django_tables2 import Table
from django_tables2.columns import TemplateColumn, Column, CheckBoxColumn
from django.db.models.expressions import Case, When
from django.db.models import Count, Sum
from django.utils.translation import gettext_lazy as _

from . import models
from members.models import Member
from events.models import Participation


class DebitTable(Table):
    mandate = Column(verbose_name=_('SEPA mandate'), empty_values=())
    edit = TemplateColumn(
        ('<a href="{%% url "finances:edit_debit" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "finances:delete_debit" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )
    execution_date = Column(verbose_name=_('execution date'), empty_values=())
    amount = Column(verbose_name=_('amount'), empty_values=())

    def __init__(self, queryset, can_edit, execution_date, *args, **kwargs):
        super().__init__(queryset, args, kwargs)
        if not execution_date:
            self.exclude = self.exclude + ('execution_date',)
        if not can_edit:
            self.exclude = self.exclude + ('edit',)
            self.exclude = self.exclude + ('delete',)

    def render_execution_date(self, record):
        return record.group.execution_date

    def render_amount(self, record):
        return ('%.2f EUR' % record.amount).replace('.', ',')

    def order_mandate(self, QuerySet, is_descending):
        QuerySet = QuerySet.annotate(
            mandate=Case(
                When(mfc__isnull=False, then='member__mandate_membership_fee'),
                When(mfc__isnull=True, then='member__mandate_participation_fee')
            )
        ).order_by(('-' if is_descending else '') + 'mandate')
        return (QuerySet, True)

    class Meta:
        model = models.Debit
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'member',
            'mandate',
            'reference',
        )
        sequence = (
            'execution_date',
            'member',
            'mandate',
            'reference',
            'amount',
            'edit',
            'delete',
        )
        attrs = {'class': 'table table-striped table-sm'}


class DebitGroupTable(Table):
    view = TemplateColumn(
        ('<a href="{%% url "finances:old_debits" record.id %%}" '
         'class="btn btn-secondary btn-sm btn-esm">%s</a>') % _('View'),
        verbose_name=_('View'),
        orderable=False
    )
    export = TemplateColumn(
        ('<a href="{%% url "finances:export_debits" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Export'),
        verbose_name=_('Export'),
        orderable=False
    )
    count = Column(verbose_name=_('Count'))
    sum = Column(verbose_name=_('Sum'), empty_values=())

    def order_count(self, QuerySet, is_descending):
        QuerySet = QuerySet.annotate(
            count=Count('debit')
        ).order_by(('-' if is_descending else '') + 'count')
        return (QuerySet, True)

    def render_sum(self, record):
        return ('%.2f EUR' % record.sum()).replace('.', ',')

    def order_sum(self, QuerySet, is_descending):
        QuerySet = QuerySet.annotate(
            count=Sum('debit__amount')
        ).order_by(('-' if is_descending else '') + 'count')
        return (QuerySet, True)

    class Meta:
        model = models.DebitGroup
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'execution_date',
            'count',
        )
        sequence = (
            'execution_date',
            'count',
            'sum'
        )
        attrs = {'class': 'table table-striped table-sm'}


class CheckBoxColumnNoHeader(CheckBoxColumn):
    @property
    def header(self):
        return ' '

    def is_checked(self, value, record):
        return record.fee > 0


class MembershipFeeCollectionTable(Table):
    count = Column(verbose_name=_('Count'))
    sum = Column(verbose_name=_('Sum'))
    execution_date = Column(verbose_name=_('execution date'), empty_values=())

    def render_execution_date(self, record):
        debits = models.Debit.objects.filter(mfc=record)
        return models.DebitGroup.objects.filter(
            debit__in=debits
        ).order_by(
            'execution_date'
        ).first().execution_date

    def order_count(self, QuerySet, is_descending):
        QuerySet = QuerySet.annotate(
            count=Count('debit')
        ).order_by(('-' if is_descending else '') + 'count')
        return (QuerySet, True)

    def render_sum(self, record):
        return ('%.2f EUR' % record.sum()).replace('.', ',')

    def order_sum(self, QuerySet, is_descending):
        QuerySet = QuerySet.annotate(
            count=Sum('debit__amount')
        ).order_by(('-' if is_descending else '') + 'count')
        return (QuerySet, True)

    class Meta:
        model = models.MembershipFeeCollection
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'year',
            'nr',
            'count',
        )
        sequence = (
            'year',
            'nr',
            'execution_date',
            'count',
            'sum',
        )
        attrs = {'class': 'table table-striped table-sm'}


class MembershipFeeCollectionCreateTable(Table):
    create = CheckBoxColumnNoHeader(accessor='pk')

    def render_fee(self, record):
        return ('%.2f EUR' % record.fee).replace('.', ',')

    class Meta:
        model = Member
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'create',
            'nr',
            'first_name',
            'last_name',
            'function',
            'section',
            'mandate_membership_fee',
            'fee',
        )
        sequence = (
            'create',
            'nr',
            'first_name',
            'last_name',
            'function',
            'section',
            'mandate_membership_fee',
            'fee',
        )
        attrs = {'class': 'table table-striped table-sm'}


class EventDebitCreateTable(Table):
    create = CheckBoxColumnNoHeader(accessor='pk')
    first_name = Column(accessor='member.first_name')
    last_name = Column(accessor='member.last_name')
    function = Column(accessor='member.function')
    section = Column(accessor='member.section')
    mandate = Column(accessor='member.mandate_participation_fee')
    participation_fee = Column(verbose_name=_('fee'), empty_values=())

    def render_participation_fee(self, record):
        return ('%.2f EUR' % record.event.fee).replace('.', ',')

    class Meta:
        model = Participation
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'create',
            'first_name',
            'last_name',
            'function',
            'section',
            'mandate',
            'participation_fee',
        )
        sequence = (
            'create',
            'first_name',
            'last_name',
            'function',
            'section',
            'mandate',
            'participation_fee',
        )
        attrs = {'class': 'table table-striped table-sm'}


class EntryTable(Table):
    nr = Column(accessor='id', verbose_name=_('Nr.'))
    edit = TemplateColumn(
        ('<a href="{%% url "finances:edit_entry" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "finances:delete_entry" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    def render_amount(self, record):
        return ('%.2f EUR' % record.amount).replace('.', ',')

    def __init__(self, queryset, can_edit, *args, **kwargs):
        super().__init__(queryset, args, kwargs)
        if not can_edit:
            self.exclude = ['edit', 'delete']

    class Meta:
        model = models.Entry
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'nr',
            'date',
            'amount',
            'debit_account',
            'credit_account',
            'comment',
            'voucher',
        )
        attrs = {'class': 'table table-striped table-sm'}


class AccountTable(Table):
    balance = Column(empty_values=())

    def render_balance(self, record):
        t, b = record.get_balance_with_type
        if t == 'debit':
            return ('%.2f EUR %s' % (b, _('Debit'))).replace('.', ',')
        else:
            return ('%.2f EUR %s' % (b, _('Credit'))).replace('.', ',')

    class Meta:
        model = models.Account
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'name',
            'year',
            'type',
        )
        attrs = {'class': 'table table-striped table-sm'}


class OpenSettlementTable(Table):
    book = TemplateColumn(
        ('<a href="{%% url "finances:book_settlement" record.id %%}" '
         'class="btn btn-success btn-sm btn-esm">%s</a>') % _('Book'),
        verbose_name=_('Book'),
        orderable=False
    )
    edit = TemplateColumn(
        ('<a href="{%% url "finances:edit_settlement" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "finances:delete_settlement" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    def render_amount(self, record):
        return ('%.2f EUR' % record.amount).replace('.', ',')

    def __init__(self, queryset, can_book, *args, **kwargs):
        super().__init__(queryset, args, kwargs)
        if not can_book:
            self.exclude = self.exclude + ('book',)

    class Meta:
        model = models.Settlement
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'amount',
            'contra_account',
            'comment',
            'payee',
            'voucher'
        )
        sequence = (
            'payee',
            'amount',
            'contra_account',
            'comment',
            'voucher',
            'book',
            'edit',
            'delete'
        )
        attrs = {'class': 'table table-striped table-sm'}


class BookedSettlementTable(Table):
    date = Column(accessor='entry.date')

    def render_amount(self, record):
        return ('%.2f EUR' % record.amount).replace('.', ',')

    class Meta:
        model = models.Settlement
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'amount',
            'contra_account',
            'comment',
            'payee',
            'voucher'
        )
        sequence = (
            'date',
            'payee',
            'amount',
            'contra_account',
            'comment',
            'voucher',
        )
        attrs = {'class': 'table table-striped table-sm'}


class DonationTable(Table):
    edit = TemplateColumn(
        ('<a href="{%% url "finances:edit_donation" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "finances:delete_donation" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    def render_amount(self, record):
        return ('%.2f EUR' % record.amount).replace('.', ',')

    class Meta:
        model = models.Donation
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'date',
            'payee',
            'name',
            'amount',
            'comment'
        )
        sequence = (
            'date',
            'payee',
            'name',
            'amount',
            'comment',
            'edit',
            'delete'
        )
        attrs = {'class': 'table table-striped table-sm'}


class TravelingCostTable(Table):
    edit = TemplateColumn(
        ('<a href="{%% url "finances:edit_travelingcost" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "finances:delete_travelingcost" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )
    amount = Column(verbose_name=_('amount'), accessor='amount')

    def render_amount(self, record):
        return ('%.2f EUR' % record.amount).replace('.', ',')

    class Meta:
        model = models.TravelingCost
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'date',
            'payee',
            'from_place',
            'to_place',
            'kilometers',
            'comment'
        )
        sequence = (
            'date',
            'payee',
            'from_place',
            'to_place',
            'kilometers',
            'amount',
            'comment',
            'edit',
            'delete'
        )
        attrs = {'class': 'table table-striped table-sm'}
