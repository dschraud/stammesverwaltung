{% extends  "_base_email.txt" %}

{% load i18n %}

{% block message %}
    {% blocktrans %}your settlement "{{ comment }}" has just been completed. The money should arrive at the account you specified shortly.{% endblocktrans %}
{% endblock %}

{% block clicklink %}
    {% trans "Click the following link to get an overview over all your settlements." %}
{% endblock %}

{% block url %}{% url 'finances:settlements' %}{% endblock %}
