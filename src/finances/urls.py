"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

app_name = 'finances'

urlpatterns = [
    path(
        _('debits/'),
        views.DebitListView.as_view(),
        name='debits'
    ),
    path(
        _('settlements/'),
        views.SettlementListView.as_view(),
        name='settlements'
    ),
    path(
        _('donations/<year>/'),
        views.DonationListView.as_view(),
        name='donations'
    ),
    path(
        _('entries/<year>/'),
        views.EntryListView.as_view(),
        name='entries'
    ),
    path(
        _('edit-entry/<pk>/'),
        views.EntryUpdateView.as_view(),
        name='edit_entry'
    ),
    path(
        _('delete-entry/<pk>/'),
        views.EntryDeleteView.as_view(),
        name='delete_entry'
    ),
    path(
        _('accounts/<year>/'),
        views.AccountListView.as_view(),
        name='accounts'
    ),
    path(_('old-debits/<group>/'), views.OldDebitListView.as_view(), name='old_debits'),
    path(_('create-debit/'), views.DebitCreateView.as_view(), name='create_debit'),
    path(_('edit-debit/<pk>/'), views.DebitUpdateView.as_view(), name='edit_debit'),
    path(_('delete-debit/<pk>/'), views.DebitDeleteView.as_view(), name='delete_debit'),
    path(_('export-debits/<group>/'), views.DebitsExportView.as_view(), name='export_debits'),
    path(
        _('create-mfc/'),
        views.MembershipFeeCollectionCreateView.as_view(),
        name='create_mfc'
    ),
    path(
        _('create-event-debits/<event>/'),
        views.EventDebitCreateView.as_view(),
        name='create_event_debits'
    ),
    path(
        _('create-entry/'),
        views.EntryCreateView.as_view(),
        name='create_entry'
    ),
    path(
        _('create-account/'),
        views.AccountCreateView.as_view(),
        name='create_account'
    ),
    path(
        _('create-contra-account/'),
        views.ContraAccountCreateView.as_view(),
        name='create_contra_account'
    ),
    path(
        _('edit-account/<pk>/'),
        views.AccountUpdateView.as_view(),
        name='edit_account'
    ),
    path(
        _('close-accounts/<year>/'),
        views.AccountCloseView.as_view(),
        name='close_accounts'
    ),
    path(
        _('export-entry/<year>/'),
        views.EntryExportView.as_view(),
        name='export_entry'
    ),
    path(
        _('export-voucher/<year>/'),
        views.VoucherExportView.as_view(),
        name='export_voucher'
    ),
    path(
        _('event-export-voucher/<event>/'),
        views.EventVoucherExportView.as_view(),
        name='event_export_voucher'
    ),
    path(
        _('export-account/<year>/'),
        views.AccountExportView.as_view(),
        name='export_account'
    ),
    path(
        _('create-settlement/'),
        views.SettlementCreateView.as_view(),
        name='create_settlement'
    ),
    path(
        _('edit-settlement/<pk>/'),
        views.SettlementUpdateView.as_view(),
        name='edit_settlement'
    ),
    path(
        _('delete-settlement/<pk>/'),
        views.SettlementDeleteView.as_view(),
        name='delete_settlement'
    ),
    path(
        _('book-settlement/<pk>/'),
        views.SettlementBookView.as_view(),
        name='book_settlement'
    ),
    path(
        _('create-event-account/<event>/'),
        views.EventAccountCreateView.as_view(),
        name='create_event_account'
    ),
    path(
        _('create-donation/'),
        views.DonationCreateView.as_view(),
        name='create_donation'
    ),
    path(
        _('edit-donation/<pk>/'),
        views.DonationUpdateView.as_view(),
        name='edit_donation'
    ),
    path(
        _('delete-donation/<pk>/'),
        views.DonationDeleteView.as_view(),
        name='delete_donation'
    ),
    path(
        _('create-travelingcost/'),
        views.TravelingCostCreateView.as_view(),
        name='create_travelingcost'
    ),
    path(
        _('edit-travelingcost/<pk>/'),
        views.TravelingCostUpdateView.as_view(),
        name='edit_travelingcost'
    ),
    path(
        _('delete-travelingcost/<pk>/'),
        views.TravelingCostDeleteView.as_view(),
        name='delete_travelingcost'
    ),
    path(
        _('export-donations/<year>/'),
        views.DonationExportView.as_view(),
        name='export_donations'
    ),
]
