from django_tables2 import Table
from django_tables2.columns import TemplateColumn, Column
from django.utils.translation import gettext_lazy as _
from django.utils.html import format_html
from django.db.models import Exists, OuterRef
from django.utils.timezone import make_aware
from rolepermissions.checkers import has_role

from datetime import datetime

from . import models
from authentication import roles


class OrderTable(Table):
    edit = TemplateColumn(
        ('<a href="{%% url "stock:edit_order" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "stock:delete_order" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )
    complete = TemplateColumn(
        ('<a href="{%% url "stock:complete_order" record.id %%}" '
         'class="btn btn-success btn-sm btn-esm">%s</a>') % _('Complete'),
        verbose_name=_('Complete'),
        orderable=False
    )

    def render_prize(self, record):
        return ('%.2f EUR' % record.prize).replace('.', ',')

    def __init__(self, queryset, can_complete, can_edit, *args,  **kwargs):
        super().__init__(queryset, *args, **kwargs)
        if not can_complete:
            self.exclude = self.exclude + ('complete',)
        if not can_edit:
            self.exclude = self.exclude + ('edit',)
            self.exclude = self.exclude + ('delete',)

    class Meta:
        model = models.Order
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'date',
            'member',
            'count',
            'name',
            'item_no',
            'size_color',
            'comment',
            'prize',
        )
        sequence = (
            'date',
            'member',
            'count',
            'name',
            'item_no',
            'size_color',
            'comment',
            'prize',
        )
        attrs = {'class': 'table table-striped table-sm'}


class StockTable(Table):
    status = Column(empty_values=(), verbose_name=_('Status'))
    reservations = TemplateColumn(
        ('<a href="{%% url "stock:reservations" record.id %%}" '
         'class="btn btn-secondary btn-sm btn-esm">%s</a>') % _('Reservations'),
        verbose_name=_('Reservations'),
        orderable=False
    )
    edit = TemplateColumn(
        ('<a href="{%% url "stock:edit_stock" record.id %%}" '
         'class="btn btn-primary btn-sm btn-esm">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "stock:delete_stock" record.id %%}" '
         'class="btn btn-danger btn-sm btn-esm">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    def render_status(self, record):
        if models.Reservation.objects.filter(
            stock=record,
            from_time__lte=make_aware(datetime.now()),
            to_time__gte=make_aware(datetime.now())
        ).exists():
            return format_html('<div class="text-danger">{}</div>', _('reserved'))
        else:
            return format_html('<div class="text-success">{}</div>', _('available'))

    def order_status(self, QuerySet, is_descending):
        current_reservations = models.Reservation.objects.filter(
            stock=OuterRef('pk'),
            from_time__lte=make_aware(datetime.now()),
            to_time__gte=make_aware(datetime.now())
        )
        QuerySet = QuerySet.annotate(
            status=Exists(current_reservations)
        ).order_by(('-' if is_descending else '') + 'status')
        return (QuerySet, True)

    class Meta:
        model = models.Stock
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'name',
            'category',
            'place',
            'comments',
        )
        sequence = (
            'name',
            'category',
            'place',
            'comments',
        )
        attrs = {'class': 'table table-striped table-sm'}


class ReservationTable(Table):
    edit = TemplateColumn(
        ('<a href="{%% url "stock:edit_reservation" record.id %%}" class="btn '
         'btn-primary btn-sm btn-esm {%% if record.member.user != user and not '
         'allowed %%}disabled{%% endif %%}">%s</a>') % _('Edit'),
        verbose_name=_('Edit'),
        orderable=False
    )
    delete = TemplateColumn(
        ('<a href="{%% url "stock:delete_reservation" record.id %%}" class="btn '
         'btn-danger btn-sm btn-esm {%% if record.member.user != user and not '
         'allowed %%}disabled{%% endif %%}">%s</a>') % _('Delete'),
        verbose_name=_('Delete'),
        orderable=False
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        extra_context = {
            'user': self.user,
            'allowed': has_role(self.user, [roles.Board, roles.StockWarden])
        }
        self.base_columns['edit'].extra_context = extra_context
        self.base_columns['delete'].extra_context = extra_context
        super().__init__(*args, **kwargs)

    class Meta:
        model = models.Reservation
        template_name = 'django_tables2/bootstrap4.html'
        fields = (
            'from_time',
            'to_time',
            'member',
            'event',
            'comments',
        )
        sequence = (
            'from_time',
            'to_time',
            'member',
            'event',
            'comments',
        )
        attrs = {'class': 'table table-striped table-sm'}
