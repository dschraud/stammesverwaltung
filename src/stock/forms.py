from django import forms
from django.utils.translation import gettext_lazy as _
from rolepermissions.checkers import has_role
from bootstrap_datepicker_plus.widgets import DateTimePickerInput

from . import models
from members.models import Member
from authentication import roles
from config.widgets import EURWidget, BootstrapSelect2Widget


class OrderForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        if not has_role(self.user, [roles.Board, roles.StockWarden]):
            self.fields['member'].queryset = Member.objects.filter(user=self.user)
            self.fields['payed_by_group'].widget = forms.HiddenInput()

    def import_item(self, item):
        self.fields['item_no'].initial = item.item_no
        self.fields['name'].initial = item.name
        self.fields['prize'].initial = item.prize

    def is_valid(self):
        valid = super().is_valid()
        if not has_role(self.user, [roles.Board, roles.StockWarden]):
            if self.cleaned_data['payed_by_group']:
                return False
        return valid

    class Meta:
        model = models.Order
        exclude = ['date', 'completed']
        widgets = {
            'member': BootstrapSelect2Widget,
            'count': forms.NumberInput(attrs={'min': '1'}),
            'prize': EURWidget,
        }


class OrderCompleteForm(forms.Form):
    comment = forms.CharField(label=_('comment'), widget=forms.widgets.Textarea(attrs={'rows': 3}))


class StockForm(forms.ModelForm):
    class Meta:
        model = models.Stock
        fields = [
            'name',
            'category',
            'place',
            'comments',
        ]
        widgets = {
            'category': BootstrapSelect2Widget,
        }


class ReservationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)
        self.fields['member'].queryset = Member.objects.filter(user=self.user)

    def is_valid(self):
        valid = super().is_valid()
        if self.cleaned_data['from_time'] >= self.cleaned_data['to_time']:
            return False
        return valid

    class Meta:
        model = models.Reservation
        fields = [
            'from_time',
            'to_time',
            'member',
            'comments',
        ]
        widgets = {
            'from_time': DateTimePickerInput(options={
                'format': 'DD.MM.YYYY HH:mm',
                'locale': 'de'}
            ),
            'to_time': DateTimePickerInput(options={
                'format': 'DD.MM.YYYY HH:mm',
                'locale': 'de'}
            ),
            'member': BootstrapSelect2Widget,
        }
