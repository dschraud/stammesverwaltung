from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from rolepermissions.roles import assign_role
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import make_aware
from datetime import datetime

from members.models import Member, Section, SEPAMandate
from finances.models import Debit
from authentication.models import SVUser
from authentication import roles
from . import models

# Create your tests here.


class ViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = SVUser.objects.create_user(
            email='test@test.com',
            first_name='Te',
            last_name='St',
            password='1234asdfg'
        )
        self.user2 = SVUser.objects.create_user(
            email='tast@test.com',
            first_name='Tesd',
            last_name='Stas',
            password='1235asdfg'
        )
        self.user.is_active = True
        self.user.save()
        self.client.login(email='test@test.com', password='1234asdfg')

        self.section1 = Section.objects.create(
            name='Beever',
            min_age=10,
            max_age=14
        )
        self.section2 = Section.objects.create(
            name='Cupse',
            min_age=13,
            max_age=17
        )
        self.mandate1 = SEPAMandate.objects.create(
            reference='1-1',
            depositor='Martina Mustermann',
            iban='DE00770500000000123456',
            bic='DJANGODEF12',
            bank='Django Reserve',
            date='2017-04-18'
        )
        self.mandate2 = SEPAMandate.objects.create(
            reference='1-2',
            depositor='Jochen Mustermann',
            iban='DE00770500000000789012',
            bic='DJANGODEF12',
            bank='Django Reserve',
            date='2018-05-20'
        )
        self.mandate3 = SEPAMandate.objects.create(
            reference='2-1',
            depositor='Jakob Mustermann',
            iban='DE00770500000000789013',
            bic='DJANGODEF12',
            bank='Django Reserve',
            date='2018-05-20'
        )
        self.member1 = Member.objects.create(
            nr=1,
            first_name='Max',
            last_name='Mustermann',
            section=self.section1,
            function=1,
            nami_nr=100001,
            fee=15.00,
            mandate_membership_fee=self.mandate1,
            mandate_participation_fee=self.mandate2,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2000-01-01',
            gender='m',
            mobile='016012345678',
            user=self.user
        )
        self.member2 = Member.objects.create(
            nr=2,
            first_name='Maria',
            last_name='Mustermann',
            section=self.section1,
            function=0,
            nami_nr=100002,
            fee=15.00,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2000-01-01',
            gender='f'
        )
        self.member3 = Member.objects.create(
            nr=3,
            first_name='Mario',
            last_name='Mustermann',
            section=self.section2,
            function=0,
            nami_nr=100003,
            fee=15.00,
            street='Musterstreet 12',
            zip='12345',
            town='Mustertown',
            dob='2002-01-01',
            gender='m'
        )
        self.order1 = models.Order.objects.create(
            member=self.member1,
            item_no='123',
            name='Badge',
            count=3,
            size_color='Blue',
            prize=1.2,
            date='2019-01-05'
        )
        self.order2 = models.Order.objects.create(
            member=self.member1,
            item_no='231',
            name='Scarf',
            count=1,
            size_color='Green',
            prize=7.43,
            date='2019-02-09',
            completed=True
        )
        self.order3 = models.Order.objects.create(
            member=self.member3,
            item_no='456',
            name='Uniform',
            count=2,
            size_color='XL',
            prize=20,
            date='2019-03-14',
        )
        self.order4 = models.Order.objects.create(
            member=self.member3,
            item_no='654',
            name='Knot',
            count=18,
            size_color='Brown',
            prize=1.15,
            date='2019-02-09',
            completed=True
        )
        self.stock = models.Stock.objects.create(
            name='Pot',
            category=1,
            place='Basement',
            comments='50l'
        )
        self.reservation1 = models.Reservation.objects.create(
            stock=self.stock,
            from_time=make_aware(datetime(2019, 2, 2, hour=16, minute=34)),
            to_time=make_aware(datetime(2019, 2, 24, hour=15, minute=12)),
            member=self.member1,
            comments='I need this...'
        )
        self.reservation2 = models.Reservation.objects.create(
            stock=self.stock,
            from_time=make_aware(datetime(2019, 4, 3, hour=8,)),
            to_time=make_aware(datetime(2019, 5, 1, hour=8)),
            member=self.member3
        )

    def test_order_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('stock:orders'))

        self.assertContains(response, 'Badge', status_code=200)
        self.assertContains(response, 'Scarf', status_code=200)
        self.assertNotContains(response, 'Uniform', status_code=200)
        self.assertNotContains(response, 'Knot', status_code=200)

    def test_order_list_view_board(self):
        assign_role(self.user, roles.Board)

        response = self.client.get(reverse('stock:orders'))

        self.assertContains(response, 'Badge', status_code=200)
        self.assertContains(response, 'Scarf', status_code=200)
        self.assertContains(response, 'Uniform', status_code=200)
        self.assertContains(response, 'Knot', status_code=200)

    def test_order_create_view_get(self):
        response = self.client.get(reverse('stock:create_order'))

        self.assertContains(response, _('Create order'), status_code=200)
        self.assertContains(response, _('Choose item'), status_code=200)

    def test_order_create_view_post_leader(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member1.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55
        }
        response = self.client.post(
            reverse('stock:create_order'),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Hat', status_code=200)

    def test_order_create_view_post_leader_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member3.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55
        }
        response = self.client.post(
            reverse('stock:create_order'),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Order.objects.all().count(), 4)

    def test_order_create_view_post_leader_payed_by_group_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member1.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55,
            'payed_by_group': 'on'
        }
        response = self.client.post(
            reverse('stock:create_order'),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Order.objects.all().count(), 4)

    def test_order_create_view_post_board(self):
        assign_role(self.user, roles.Board)

        post_data = {
            'member': self.member3.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55,
            'payed_by_group': 'on'
        }
        response = self.client.post(
            reverse('stock:create_order'),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Hat', status_code=200)

    def test_order_update_view_get(self):
        response = self.client.get(reverse('stock:edit_order', kwargs={
            'pk': self.order1.pk
        }))

        self.assertContains(response, _('Edit order'), status_code=200)
        self.assertContains(response, 'Badge', status_code=200)

    def test_order_update_view_post_leader(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member1.pk,
            'item_no': 1,
            'name': 'Hut',
            'count': 1,
            'size_color': '52',
            'prize': 6.55
        }
        response = self.client.post(
            reverse('stock:edit_order', kwargs={
                'pk': self.order1.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Hut', status_code=200)

    def test_order_update_view_post_leader_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member3.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55
        }
        response = self.client.post(
            reverse('stock:edit_order', kwargs={
                'pk': self.order1.pk
            }),
            post_data,
            follow=True
        )
        self.order1.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.order1.member, self.member1)

    def test_order_update_view_post_leader_payed_by_group_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member1.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55,
            'payed_by_group': 'on'
        }
        response = self.client.post(
            reverse('stock:edit_order', kwargs={
                'pk': self.order1.pk
            }),
            post_data,
            follow=True
        )
        self.order1.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.order1.payed_by_group, False)

    def test_order_update_view_post_leader_false_order_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'member': self.member1.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55
        }
        response = self.client.post(
            reverse('stock:edit_order', kwargs={
                'pk': self.order3.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 404)

    def test_order_update_view_post_board(self):
        assign_role(self.user, roles.Board)

        post_data = {
            'member': self.member3.pk,
            'item_no': 1,
            'name': 'Hat',
            'count': 1,
            'size_color': '52',
            'prize': 6.55,
            'payed_by_group': 'on'
        }
        response = self.client.post(
            reverse('stock:edit_order', kwargs={
                'pk': self.order3.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Hat', status_code=200)

    def test_order_delete_view_get(self):
        response = self.client.get(reverse('stock:delete_order', kwargs={
            'pk': self.order1.pk
        }))

        self.assertContains(response, _('Delete order'), status_code=200)
        self.assertContains(response, 'Badge', status_code=200)

    def test_order_delete_view_post_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('stock:delete_order', kwargs={
                'pk': self.order1.pk
            }),
            follow=True
        )

        self.assertNotContains(response, 'Badge', status_code=200)

    def test_order_delete_view_post_leader_false_order_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('stock:delete_order', kwargs={
                'pk': self.order3.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 404)

    def test_order_delete_view_post_board(self):
        assign_role(self.user, roles.Board)

        response = self.client.post(
            reverse('stock:delete_order', kwargs={
                'pk': self.order3.pk
            }),
            follow=True
        )

        self.assertNotContains(response, 'Uniform', status_code=200)

    def test_order_complete_view_get_leader_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(reverse('stock:complete_order', kwargs={
            'pk': self.order1.pk
        }))

        self.assertEqual(response.status_code, 403)

    def test_order_confirm_view_get_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.get(reverse('stock:complete_order', kwargs={
            'pk': self.order3.pk
        }))

        self.assertContains(response, _('Complete order'), status_code=200)
        self.assertContains(response, 'Uniform', status_code=200)

    def test_order_confirm_view_post_leader_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'comment': 'In your inbox'
        }
        response = self.client.post(
            reverse('stock:complete_order', kwargs={
                'pk': self.order1.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_order_confirm_view_post_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        post_data = {
            'comment': 'In your inbox'
        }
        response = self.client.post(
            reverse('stock:complete_order', kwargs={
                'pk': self.order1.pk
            }),
            post_data,
            follow=True
        )
        self.order1.refresh_from_db()

        self.assertContains(response, 'In your inbox', status_code=200)
        self.assertTrue(self.order1.completed)
        self.assertEqual(Debit.objects.all().count(), 1)

    def test_stock_list_view_not_allowed(self):
        response = self.client.get(
            reverse('stock:stock')
        )

        self.assertEqual(response.status_code, 403)

    def test_stock_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:stock')
        )

        self.assertContains(response, 'Pot', status_code=200)

    def test_stock_create_view_get_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:create_stock')
        )

        self.assertEqual(response.status_code, 403)

    def test_stock_create_view_get_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.get(
            reverse('stock:create_stock')
        )

        self.assertContains(response, _('Create stock'), status_code=200)

    def test_stock_create_view_post_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'name': 'Tent',
            'category': 0,
            'place': 'Attic',
        }
        response = self.client.post(
            reverse('stock:create_stock'),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_stock_create_view_post_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        post_data = {
            'name': 'Tent',
            'category': 0,
            'place': 'Attic',
        }
        response = self.client.post(
            reverse('stock:create_stock'),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Tent', status_code=200)

    def test_stock_update_view_get_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:edit_stock', kwargs={
                'pk': self.stock.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_stock_update_view_get_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.get(
            reverse('stock:edit_stock', kwargs={
                'pk': self.stock.pk
            })
        )

        self.assertContains(response, _('Edit stock'), status_code=200)
        self.assertContains(response, 'Pot', status_code=200)

    def test_stock_update_view_post_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'name': 'Tent',
            'category': 0,
            'place': 'Attic',
        }
        response = self.client.post(
            reverse('stock:edit_stock', kwargs={
                'pk': self.stock.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_stock_update_view_post_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        post_data = {
            'name': 'Tent',
            'category': 0,
            'place': 'Attic',
        }
        response = self.client.post(
            reverse('stock:edit_stock', kwargs={
                'pk': self.stock.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, 'Tent', status_code=200)
        self.assertNotContains(response, 'Pot', status_code=200)

        self.assertContains(response, 'Tent', status_code=200)

    def test_stock_delete_view_get_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:delete_stock', kwargs={
                'pk': self.stock.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_stock_delete_view_get_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.get(
            reverse('stock:delete_stock', kwargs={
                'pk': self.stock.pk
            })
        )

        self.assertContains(response, _('Delete stock'), status_code=200)
        self.assertContains(response, 'Pot', status_code=200)

    def test_stock_delete_view_post_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('stock:delete_stock', kwargs={
                'pk': self.stock.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_stock_delete_view_post_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.post(
            reverse('stock:delete_stock', kwargs={
                'pk': self.stock.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Stock.objects.all().count(), 0)

    def test_reservation_list_view_not_allowed(self):
        response = self.client.get(
            reverse('stock:reservations', kwargs={
                'stock': self.stock.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_reservation_list_view_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:reservations', kwargs={
                'stock': self.stock.pk
            })
        )

        self.assertContains(response, 'Max', status_code=200)
        self.assertContains(response, 'Mario', status_code=200)

    def test_reservation_create_view_get_not_allowed(self):
        response = self.client.get(
            reverse('stock:create_reservation', kwargs={
                'stock': self.stock.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_reservation_create_view_get_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:create_reservation', kwargs={
                'stock': self.stock.pk
            })
        )

        self.assertContains(response, _('Create reservation for %s') % self.stock, status_code=200)

    def test_reservation_create_view_post_not_allowed(self):
        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member1.pk,
        }
        response = self.client.post(
            reverse('stock:create_reservation', kwargs={
                'stock': self.stock.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_reservation_create_view_post_leader_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member3.pk,
        }
        response = self.client.post(
            reverse('stock:create_reservation', kwargs={
                'stock': self.stock.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.Reservation.objects.all().count(), 2)

    def test_reservation_create_view_post_leader(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member1.pk,
        }
        response = self.client.post(
            reverse('stock:create_reservation', kwargs={
                'stock': self.stock.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, '07.04.2019 07:59', status_code=200)

    def test_reservation_update_view_get_not_allowed(self):
        response = self.client.get(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation1.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_reservation_update_view_get_leader_wrong_reservation_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation2.pk
            })
        )

        self.assertEqual(response.status_code, 404)

    def test_reservation_update_view_get_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation1.pk
            })
        )

        self.assertContains(response, _('Edit reservation for %s') % self.stock, status_code=200)

    def test_reservation_update_view_get_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.get(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation2.pk
            })
        )

        self.assertContains(response, _('Edit reservation for %s') % self.stock, status_code=200)

    def test_reservation_update_view_post_not_allowed(self):
        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member1.pk,
            'comments': 'Blub'
        }
        response = self.client.post(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation1.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_reservation_update_view_post_leader_member_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member3.pk,
        }
        response = self.client.post(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation1.pk
            }),
            post_data,
            follow=True
        )
        self.reservation1.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.reservation1.member, self.member1)

    def test_reservation_update_view_post_leader_wrong_reservation_not_allowed(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member1.pk,
        }
        response = self.client.post(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation2.pk
            }),
            post_data,
            follow=True
        )

        self.assertEqual(response.status_code, 404)

    def test_reservation_update_view_post_leader(self):
        assign_role(self.user, roles.Leader)

        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member1.pk,
        }
        response = self.client.post(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation1.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, '07.04.2019 07:59', status_code=200)

    def test_reservation_update_view_post_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        post_data = {
            'from_time': '05.04.2019 08:00:02',
            'to_time': '07.04.2019 07:59:59',
            'member': self.member1.pk,
        }
        response = self.client.post(
            reverse('stock:edit_reservation', kwargs={
                'pk': self.reservation2.pk
            }),
            post_data,
            follow=True
        )

        self.assertContains(response, '07.04.2019 07:59', status_code=200)

    def test_reservation_delete_view_get_not_allowed(self):
        response = self.client.get(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation1.pk
            })
        )

        self.assertEqual(response.status_code, 403)

    def test_reservation_delete_view_get_leader_wrong_reservation_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation2.pk
            })
        )

        self.assertEqual(response.status_code, 404)

    def test_reservation_delete_view_get_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.get(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation1.pk
            })
        )

        self.assertContains(
            response,
            '%s %s' % (_('Delete reservation for'), self.stock),
            status_code=200
        )

    def test_reservation_delete_view_get_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.get(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation2.pk
            })
        )

        self.assertContains(
            response,
            '%s %s' % (_('Delete reservation for'), self.stock),
            status_code=200
        )

    def test_reservation_delete_view_post_not_allowed(self):
        response = self.client.post(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation1.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_reservation_delete_view_post_leader_wrong_reservation_not_allowed(self):
        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation2.pk
            }),
            follow=True
        )

        self.assertEqual(response.status_code, 404)

    def test_reservation_delete_view_post_leader(self):
        assign_role(self.user, roles.Leader)

        response = self.client.post(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation1.pk
            }),
            follow=True
        )

        self.assertNotContains(response, '02.02.2019 16:34', status_code=200)

    def test_reservation_delete_view_post_stock_warden(self):
        assign_role(self.user, roles.StockWarden)

        response = self.client.post(
            reverse('stock:delete_reservation', kwargs={
                'pk': self.reservation2.pk
            }),
            follow=True
        )

        self.assertNotContains(response, '03.04.2019 08:00', status_code=200)
