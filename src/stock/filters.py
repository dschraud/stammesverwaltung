from django_filters import FilterSet, CharFilter
from django.db.models import Q

from . import models


class StockFilter(FilterSet):
    name = CharFilter(method='filter_name')

    def filter_name(self, queryset, name, value):
        return queryset.filter(name__icontains=value)

    class Meta:
        model = models.Stock
        fields = []


class OrderFilter(FilterSet):
    name_member_item_no = CharFilter(method='filter_nmin')

    def filter_nmin(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) |
            Q(member__first_name__icontains=value) |
            Q(member__last_name__icontains=value) |
            Q(item_no__icontains=value)
        )

    class Meta:
        model = models.Order
        fields = []
