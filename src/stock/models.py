from django.utils.translation import gettext_lazy as _
from django.db import models
from django.utils import timezone
from members.models import Member
from events.models import Event

# Create your models here.

STOCK_CATEGORIES = (
    (0, _('tents')),
    (1, _('kitchen')),
    (2, _('books')),
    (9, _('miscellaneous')),
)


class Order(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE, verbose_name=_('member'))
    item_no = models.CharField(_('item number'), max_length=10)
    name = models.CharField(_('name'), max_length=50)
    count = models.IntegerField(_('count'), default=1)
    size_color = models.CharField(_('size or color'), max_length=30, null=True, blank=True)
    prize = models.DecimalField(_('prize'), max_digits=5, decimal_places=2)
    date = models.DateField(_('date'), default=timezone.now)
    comment = models.TextField(_('comment'), null=True, blank=True)
    payed_by_group = models.BooleanField(_('payed by group'), default=False)
    completed = models.BooleanField(_('completed'), default=False)

    def __str__(self):
        return '%s (%s)' % (self.name, self.member)

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')
        ordering = ['date', 'member', 'item_no']


class Item(models.Model):
    item_no = models.CharField(_('item number'), primary_key=True, max_length=10)
    name = models.CharField(_('name'), max_length=50)
    prize = models.DecimalField(_('prize'), max_digits=5, decimal_places=2)
    link = models.URLField(null=True, blank=True)

    def __str__(self):
        return '%s (#%s)' % (self.name, self.item_no)

    class Meta:
        verbose_name = _('Item')
        verbose_name_plural = _('Items')
        ordering = ['item_no']


class Stock(models.Model):
    name = models.CharField(_('name'), max_length=30)
    category = models.IntegerField(_('category'), choices=STOCK_CATEGORIES)
    place = models.CharField(_('place'), max_length=30)
    comments = models.TextField(_('comments'), null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Stock')
        verbose_name_plural = _('Stock')
        ordering = ['category', 'name']


class Reservation(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE, verbose_name=_('stock'))
    from_time = models.DateTimeField(_('from'))
    to_time = models.DateTimeField(_('to'))
    member = models.ForeignKey(Member, on_delete=models.CASCADE, verbose_name=_('member'))
    event = models.ForeignKey(Event, on_delete=models.CASCADE,
                              verbose_name=_('event'), null=True, blank=True)
    comments = models.TextField(_('comments'), null=True, blank=True)

    def __str__(self):
        return '%s :%s - %s' % (self.stock, self.from_time, self.to_time)

    class Meta:
        verbose_name = _('Reservation')
        verbose_name_plural = _('Reservations')
        ordering = ['stock', 'from_time', 'to_time']
