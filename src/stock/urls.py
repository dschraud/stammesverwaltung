"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

app_name = 'stock'

urlpatterns = [
    path(
        _('orders/'),
        views.OrderListView.as_view(),
        name='orders'
    ),
    path(
        _('create-order/'),
        views.OrderCreateView.as_view(),
        name='create_order'
    ),
    path(
        _('edit-order/<pk>/'),
        views.OrderUpdateView.as_view(),
        name='edit_order'
    ),
    path(
        _('delete-order/<pk>/'),
        views.OrderDeleteView.as_view(),
        name='delete_order'
    ),
    path(
        _('complete-order/<pk>/'),
        views.OrderCompleteView.as_view(),
        name='complete_order'
    ),
    path(
        _('stock/'),
        views.StockListView.as_view(),
        name='stock'
    ),
    path(
        _('create-stock/'),
        views.StockCreateView.as_view(),
        name='create_stock'
    ),
    path(
        _('edit-stock/<pk>/'),
        views.StockUpdateView.as_view(),
        name='edit_stock'
    ),
    path(
        _('delete-stock/<pk>/'),
        views.StockDeleteView.as_view(),
        name='delete_stock'
    ),
    path(
        _('reservations/<stock>/'),
        views.ReservationListView.as_view(),
        name='reservations'
    ),
    path(
        _('create-reservation/<stock>/'),
        views.ReservationCreateView.as_view(),
        name='create_reservation'
    ),
    path(
        _('edit-reservation/<pk>/'),
        views.ReservationUpdateView.as_view(),
        name='edit_reservation'
    ),
    path(
        _('delete-reservation/<pk>/'),
        views.ReservationDeleteView.as_view(),
        name='delete_reservation'
    ),
]
