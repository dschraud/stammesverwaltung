from django.contrib.auth.mixins import LoginRequiredMixin
from rolepermissions.checkers import has_role
from rolepermissions.mixins import HasRoleMixin
from django_tables2 import SingleTableMixin, MultiTableMixin, SingleTableView
from django_filters.views import FilterView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.utils.translation import gettext_lazy as _
from django.urls import reverse_lazy, reverse
from django.core.exceptions import ObjectDoesNotExist

from . import models, filters, tables, forms
from authentication import roles
from finances.models import Debit, DebitGroup
from finances.utils import get_debit_dates

from datetime import datetime

# Create your views here.


class OrderListView(LoginRequiredMixin, MultiTableMixin, FilterView):
    template_name = 'stock/orders.html'
    filterset_class = filters.OrderFilter
    strict = False

    def get_tables(self):
        if has_role(self.request.user, [roles.Board, roles.StockWarden]):
            new_orders = models.Order.objects.filter(
                completed=False
            ).select_related('member')
            old_orders = self.object_list.filter(
                completed=True
            ).select_related('member')
            return [
                tables.OrderTable(new_orders, True, True),
                tables.OrderTable(old_orders, False, False)
            ]
        else:
            new_orders = models.Order.objects.filter(
                completed=False,
                member__user=self.request.user
            ).select_related('member')
            old_orders = self.object_list.filter(
                completed=True,
                member__user=self.request.user
            ).select_related('member')
            return [
                tables.OrderTable(new_orders, False, True),
                tables.OrderTable(old_orders, False, False)
            ]


class OrderCreateView(LoginRequiredMixin, CreateView):
    form_class = forms.OrderForm
    template_name = 'stock/create_order.html'

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse('stock:orders')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['items'] = models.Item.objects.all()
        return context

    def get_form(self):
        form = super().get_form()
        item_no = self.request.GET.get('import', 0)
        if item_no != 0:
            item = models.Item.objects.get(pk=item_no)
            form.import_item(item)
        return form


class OrderUpdateView(LoginRequiredMixin, UpdateView):
    form_class = forms.OrderForm
    template_name = 'stock/form.html'

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse('stock:orders')

    def get_queryset(self):
        if has_role(self.request.user, [roles.Board, roles.Secretary]):
            return models.Order.objects.all()
        else:
            return models.Order.objects.filter(member__user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit order')
        return context


class OrderDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Order
    success_url = reverse_lazy('stock:orders')

    def get_queryset(self):
        if has_role(self.request.user, [roles.Board, roles.Secretary]):
            return models.Order.objects.all()
        else:
            return models.Order.objects.filter(member__user=self.request.user)


class OrderCompleteView(LoginRequiredMixin, HasRoleMixin, FormView):
    allowed_roles = [roles.Board, roles.StockWarden]
    template_name = 'stock/complete_order.html'
    form_class = forms.OrderCompleteForm
    success_url = reverse_lazy('stock:orders')

    def dispatch(self, request, pk, *args, **kwargs):
        try:
            debit_group = DebitGroup.objects.get(
                from_date__lte=datetime.today(),
                to_date__gte=datetime.today()
            )
        except ObjectDoesNotExist:
            fromd, to, execute = get_debit_dates()
            debit_group = DebitGroup.objects.create(
                from_date=fromd,
                to_date=to,
                execution_date=execute
            )
        self.order = models.Order.objects.get(pk=pk)
        if not self.order.payed_by_group:
            self.debit = Debit(
                member=self.order.member,
                amount=self.order.prize,
                reference=_('Order %(member)s %(count)sx %(item)s') % {
                    'member': self.order.member,
                    'count': self.order.count,
                    'item': self.order.name
                },
                group=debit_group
            )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['debit'] = self.debit
        return context

    def form_valid(self, form):
        self.order.comment = form.cleaned_data['comment']
        self.order.completed = True
        self.order.save()
        if self.debit:
            self.debit.save()
        return super().form_valid(form)


class StockListView(LoginRequiredMixin, HasRoleMixin, SingleTableMixin, FilterView):
    allowed_roles = [roles.Board, roles.Leader, roles.StockWarden]
    table_class = tables.StockTable
    template_name = 'stock/stock.html'
    filterset_class = filters.StockFilter
    strict = False

    def get_table_kwargs(self):
        if not has_role(self.request.user, [roles.Board, roles.StockWarden]):
            return {
                'exclude': ['edit', 'delete']
            }
        return {}


class StockCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.StockWarden]
    form_class = forms.StockForm
    template_name = 'stock/form.html'

    def get_success_url(self):
        return reverse('stock:stock')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create stock')
        return context


class StockUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.StockWarden]
    form_class = forms.StockForm
    template_name = 'stock/form.html'

    def get_queryset(self):
        return models.Stock.objects.all()

    def get_success_url(self):
        return reverse('stock:stock')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit stock')
        return context


class StockDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.StockWarden]
    model = models.Stock
    success_url = reverse_lazy('stock:stock')


class ReservationListView(LoginRequiredMixin, HasRoleMixin, SingleTableView):
    allowed_roles = [roles.Board, roles.Leader, roles.StockWarden]
    table_class = tables.ReservationTable
    template_name = 'stock/reservations.html'

    def dispatch(self, request, stock, *args, **kwargs):
        self.stock = models.Stock.objects.get(pk=stock)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return models.Reservation.objects.filter(stock=self.stock)

    def get_table_kwargs(self):
        return {
            'user': self.request.user
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["stock"] = self.stock
        return context


class ReservationCreateView(LoginRequiredMixin, HasRoleMixin, CreateView):
    allowed_roles = [roles.Board, roles.Leader, roles.StockWarden]
    form_class = forms.ReservationForm
    template_name = 'stock/form.html'

    def dispatch(self, request, stock, *args, **kwargs):
        self.stock = models.Stock.objects.get(pk=stock)
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.instance.stock = self.stock
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('stock:reservations', kwargs={'stock': self.stock.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Create reservation for %s') % self.stock
        return context


class ReservationUpdateView(LoginRequiredMixin, HasRoleMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Leader, roles.StockWarden]
    form_class = forms.ReservationForm
    template_name = 'stock/form.html'

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def get_queryset(self):
        if has_role(self.request.user, [roles.Board, roles.StockWarden]):
            return models.Reservation.objects.all()
        else:
            return models.Reservation.objects.filter(member__user=self.request.user)

    def get_success_url(self):
        return reverse('stock:reservations', kwargs={'stock': self.object.stock.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit reservation for %s') % self.object.stock
        return context


class ReservationDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Leader, roles.StockWarden]
    model = models.Reservation

    def get_queryset(self):
        if has_role(self.request.user, [roles.Board, roles.StockWarden]):
            return models.Reservation.objects.all()
        else:
            return models.Reservation.objects.filter(member__user=self.request.user)

    def get_success_url(self):
        return reverse('stock:reservations', kwargs={'stock': self.object.stock.id})
