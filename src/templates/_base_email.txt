{% load i18n %}

{% block opening %}
    {% blocktrans %}
        Hello {{ first_name }},
    {% endblocktrans %}
{% endblock %}

{% block message %}
{% endblock %}

{% block clicklink %}
{% endblock %}

{{ protocol }}://{{ domain }}{% block url %}{% endblock %}
