# Generated by Django 2.0.6 on 2018-08-01 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20180731_1940'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventfield',
            name='event',
        ),
        migrations.RemoveField(
            model_name='participationfield',
            name='event_field',
        ),
        migrations.RemoveField(
            model_name='participationfield',
            name='participation',
        ),
        migrations.AddField(
            model_name='event',
            name='scoutnet_id',
            field=models.IntegerField(blank=True, null=True, verbose_name='Scoutnet ID'),
        ),
        migrations.DeleteModel(
            name='EventField',
        ),
        migrations.DeleteModel(
            name='ParticipationField',
        ),
    ]
