import requests
from datetime import datetime


class Scoutnet:
    server = 'http://www.scoutnet.de/api/0.2'
    stamm_id = '2379'
    bezirk_id = '85'
    dv_id = '84'

    def __init__(self):
        self.session = requests.Session()

    def beautify(event):
        if event['start_date']:
            event['start_date'] = datetime.strptime(event['start_date'], '%Y-%m-%d')
        if event['end_date']:
            event['end_date'] = datetime.strptime(event['end_date'], '%Y-%m-%d')

        stufen = []
        if '16' in event['keywords']['elements']:
            stufen.append(event['keywords']['elements']['16'])
        if '17' in event['keywords']['elements']:
            stufen.append(event['keywords']['elements']['17'])
        if '18' in event['keywords']['elements']:
            stufen.append(event['keywords']['elements']['18'])
        if '19' in event['keywords']['elements']:
            stufen.append(event['keywords']['elements']['19'])
        event['stufen'] = ', '.join(stufen)

        if event['group_id'] == Scoutnet.stamm_id:
            event['ebene'] = 'St. Vitus Hirschaid'
        elif event['group_id'] == Scoutnet.bezirk_id:
            event['ebene'] = 'Bezirk 2'
        elif event['group_id'] == Scoutnet.dv_id:
            event['ebene'] = 'DV Bamberg'

        return event

    def get_event(self, id):
        url = "%s/event/%s/" % (Scoutnet.server, id)
        r = self.session.get(url, timeout=2)
        r.raise_for_status()
        event = r.json()

        if 'id' not in event:
            return None

        return Scoutnet.beautify(event)

    def get_all_events(self):
        url = ("%s/events/?json=[\"start_date > '%s' AND (group_id =="
               "'%s' OR group_id == '%s' OR group_id == '%s')\"]") % (
                      Scoutnet.server,
                      datetime.today().strftime('%Y-%m-%d'),
                      Scoutnet.stamm_id,
                      Scoutnet.bezirk_id,
                      Scoutnet.dv_id
                )
        r = self.session.get(url, timeout=2)
        r.raise_for_status()
        events = r.json()['elements']

        for event in events:
            event = Scoutnet.beautify(event)

        return events
