from django.db import models
from django.utils.translation import gettext_lazy as _

from members.models import Section, Member

# Create your models here.

LEVEL = (
        (0, _('Group')),
        (1, _('District')),
        (2, _('Diocese')),
        (3, _('Association')),
        (4, 'WOSM'),
        )

TYPES = (
        (0, _('Text')),
        (1, _('Textarea')),
        (2, _('Number')),
        (3, _('Choice')),
        (4, _('Radiobutton')),
        )


class Event(models.Model):
    name = models.CharField(_('name'), max_length=50)
    from_date = models.DateField(_('from'))
    to_date = models.DateField(_('to'))
    place = models.CharField(_('place'), max_length=50, null=True, blank=True)
    fee = models.DecimalField(_('fee'), max_digits=5, decimal_places=2)
    sections = models.ManyToManyField(Section, verbose_name=_('sections'))
    level = models.IntegerField(_('level'), choices=LEVEL)
    description = models.TextField(_('description'), null=True, blank=True)
    deadline = models.DateField(_('deadline'))
    letter = models.FileField(_('letter'), null=True, blank=True)
    participants = models.ManyToManyField(
            Member,
            through='Participation',
            verbose_name=_('participants')
    )
    scoutnet_id = models.IntegerField('Scoutnet ID', null=True, blank=True)
    account = models.ForeignKey(
        'finances.Account',
        verbose_name=_('account'),
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('event')
        verbose_name_plural = _('events')
        ordering = ['-from_date']


class Participation(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE, verbose_name=_('member'))
    event = models.ForeignKey(Event, on_delete=models.CASCADE, verbose_name=_('event'))
    time = models.DateTimeField(_('time of enrollment'), auto_now=True)

    def __str__(self):
        return "%s -> %s" % (self.member, self.event)

    class Meta:
        verbose_name = _('participation')
        verbose_name_plural = _('participations')
        unique_together = ('member', 'event')


class EventField(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, verbose_name=_('event'))
    type = models.IntegerField(_('type'), choices=TYPES)
    name = models.CharField(_('name'), max_length=100)
    required = models.BooleanField(_('required'), default=False)
    choices = models.CharField(_('choices'), max_length=200, blank=True, null=True)

    def __str__(self):
        return "%s: %s" % (self.name, self.get_type_display())

    class Meta:
        verbose_name = _('Event field')
        verbose_name_plural = _('Event fields')


class ParticipationField(models.Model):
    event_field = models.ForeignKey(
        EventField,
        on_delete=models.CASCADE,
        verbose_name=_('event field')
    )
    participation = models.ForeignKey(
        Participation,
        on_delete=models.CASCADE,
        verbose_name=_('participation')
    )
    value = models.CharField(_('value'), max_length=100)

    def __str__(self):
        return "%s (%s)" % (self.value, self.event_field)

    class Meta:
        verbose_name = _('Participation field')
        verbose_name_plural = _('Participation fields')
