from django.test import TestCase
from django.test.client import Client
from django.urls import reverse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.utils._os import upath
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from rolepermissions.roles import assign_role
import os
import datetime

from authentication.models import SVUser
from members.models import Member
from finances.models import Account, Debit, DebitGroup
from . import models
from authentication import roles
from finances.utils import get_debit_dates

# Create your tests here.

THIS_FILE = upath(__file__.rstrip("c"))
UPLOADED_FILE_NAME = 'tests.py'
file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'tmp'))


class ViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = SVUser.objects.create_user(
                email='test@test.com',
                first_name='Te',
                last_name='St',
                password='1234asdfg'
                )
        self.user.is_active = True
        self.user.save()
        self.client.login(email='test@test.com', password='1234asdfg')
        self.section = models.Section.objects.create(
            name='Beever',
            min_age=10,
            max_age=14
        )
        self.event = models.Event.objects.create(
            name='Camp',
            from_date='2018-01-01',
            to_date='2018-01-08',
            place='Somewhere',
            fee=100.99,
            level=1,
            description="It's gonna be great",
            deadline=datetime.date.today() + datetime.timedelta(days=1)
        )
        self.event.sections.add(self.section)
        self.event.save()
        self.member = Member.objects.create(
                nr=1,
                first_name='Max',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100001,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m',
                user=self.user
        )

    def tearDown(self):
        try:
            os.remove(os.path.join(settings.MEDIA_ROOT, UPLOADED_FILE_NAME))
            pass
        except FileNotFoundError:
            pass

    def test_event_list_view_login_required(self):
        self.client.logout()
        response = self.client.get(reverse('events:list'))

        self.assertNotEqual(response.status_code, 200)

    def test_event_list_view(self):
        response = self.client.get(reverse('events:list'))

        self.assertContains(response, 'Camp')
        self.assertContains(response, 'Beever')

    def test_event_list_view_with_participant(self):
        models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        response = self.client.get(reverse('events:list'))

        self.assertContains(response, 'Beever')
        self.assertContains(response, 'Mustermann')

    def test_event_detail_view_not_allowed(self):
        self.member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100002,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='f'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        participation = models.Participation.objects.create(
            member=self.member2,
            event=self.event
        )
        fd, td, ed = get_debit_dates()
        debit_group = DebitGroup.objects.create(
                from_date=fd,
                to_date=td,
                execution_date=ed
        )
        self.debit = Debit.objects.create(
                member=self.member,
                amount=16.00,
                reference='Particpation fee',
                group=debit_group,
                participation=participation
        )

        response = self.client.get(reverse(
            'events:details',
            kwargs={'pk': self.event.id}
        ))

        self.assertEquals(response.status_code, 403)

    def test_event_detail_view(self):
        self.member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100002,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='f'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        participation = models.Participation.objects.create(
            member=self.member2,
            event=self.event
        )
        fd, td, ed = get_debit_dates()
        debit_group1 = DebitGroup.objects.create(
                from_date=fd,
                to_date=td,
                execution_date=ed
        )
        debit_group2 = DebitGroup.objects.create(
                from_date='2019-01-11',
                to_date='2019-02-10',
                execution_date='2019-02-15'
        )
        self.debit1 = Debit.objects.create(
                member=self.member,
                amount=16.00,
                reference='Particpation fee',
                group=debit_group1,
                participation=participation
        )
        self.debit2 = Debit.objects.create(
                member=self.member,
                amount=16.00,
                reference='Particpation fee',
                group=debit_group2,
                participation=participation
        )

        assign_role(self.user, roles.Leader)
        response = self.client.get(reverse(
            'events:details',
            kwargs={'pk': self.event.id}
        ))

        self.assertContains(response, _('From'))
        self.assertContains(response, 'Beever')
        self.assertContains(response, 'Mustermann')
        self.assertContains(response, '16,00 EUR')
        self.assertContains(response, '100,99 EUR')
        self.assertContains(response, '201,98 EUR')

    def test_event_detail_view_with_participant(self):
        assign_role(self.user, roles.Leader)
        models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        response = self.client.get(reverse(
            'events:details',
            kwargs={'pk': self.event.id}
        ))

        self.assertContains(response, _('From'))
        self.assertContains(response, 'Beever')
        self.assertContains(response, 'Mustermann')

    def test_event_detail_view_with_event_field(self):
        assign_role(self.user, roles.Leader)
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')
        response = self.client.get(reverse(
            'events:details',
            kwargs={'pk': self.event.id}
        ))

        self.assertContains(response, _('From'))
        self.assertContains(response, 'Beever')
        self.assertContains(response, 'Mustermann')
        self.assertContains(response, 'Vegetarian')
        self.assertContains(response, 'Noxx')

    def test_participant_list_view_not_allowed(self):
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')
        response = self.client.get(reverse(
            'events:participant_list',
            kwargs={'event': self.event.id}
        ))

        self.assertEqual(response.status_code, 403)

    def test_participant_list_view(self):
        assign_role(self.user, roles.Leader)
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')
        response = self.client.get(reverse(
            'events:participant_list',
            kwargs={'event': self.event.id}
        ))

        self.assertEquals(
            response.get('Content-Disposition'),
            'attachment; filename="%s"' % (
                _('participation_list_%s.csv') % slugify(self.event.name)
            )
        )
        self.assertContains(response, 'Beever')
        self.assertContains(response, 'Mustermann')
        self.assertContains(response, 'Vegetarian')
        self.assertContains(response, 'Noxx')

    def test_event_create_wizard_upload_letter_not_allowed(self):
        step_data = {
            'event_create_wizard_view-current_step': 'general',
            'general-name': 'Camp',
            'general-from_date': '01.01.2017',
            'general-to_date': '08.01.2017',
            'general-place': 'Somewhere',
            'general-level': 2,
            'general-sections': self.section.id,
            'general-description': 'Cool event!',
        }
        response = self.client.post(reverse('events:create_event'), step_data)

        self.assertEqual(response.status_code, 403)

    def test_event_create_wizard_upload_letter(self):
        assign_role(self.user, roles.Leader)
        response = self.client.get(reverse('events:create_event'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'general')

        step_data = {
            'event_create_wizard_view-current_step': 'general',
            'general-name': 'Weekend',
            'general-from_date': '01.01.2017',
            'general-to_date': '08.01.2017',
            'general-place': 'Rothmannsthal',
            'general-level': 2,
            'general-sections': self.section.id,
            'general-description': 'Cool event!',
        }
        response = self.client.post(reverse('events:create_event'), step_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'letter')

        step_data = {
            'event_create_wizard_view-current_step': 'letter',
            'letter-fee': '100.99',
            'letter-deadline': '31.12.2016',
            'letter-letter_source': 'True',
        }
        with open(upath(THIS_FILE), 'rb') as post_file:
            step_data['letter-letter'] = post_file
            response = self.client.post(reverse('events:create_event'), step_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'enrollment')
        with open(THIS_FILE, 'rb') as f, file_storage.open(UPLOADED_FILE_NAME) as f2:
            self.assertEqual(f.read(), f2.read())

        step_data = {
            'event_create_wizard_view-current_step': 'enrollment',
            'enrollment-0-id': '',
            'enrollment-0-name': 'Remarks',
            'enrollment-0-type': 0,
            'enrollment-0-required': 'False',
            'enrollment-1-id': '',
            'enrollment-1-name': 'Veggie',
            'enrollment-1-type': 4,
            'enrollment-1-required': 'True',
            'enrollment-1-choices': 'Yes, No',
            'enrollment-INITIAL_FORMS': 0,
            'enrollment-MAX_NUM_FORMS': 1000,
            'enrollment-MIN_NUM_FORMS': 0,
            'enrollment-TOTAL_FORMS': 2,
        }
        response = self.client.post(reverse('events:create_event'), step_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'finances')

        step_data = {
            'event_create_wizard_view-current_step': 'finances',
            'finances-create_account': 'True',
        }
        response = self.client.post(reverse('events:create_event'), step_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'stock')

        step_data = {
            'event_create_wizard_view-current_step': 'stock',
        }
        response = self.client.post(reverse('events:create_event'), step_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['wizard']['steps'].current, 'confirmation')
        self.assertContains(response, 'Weekend')
        self.assertContains(response, 'Cool')
        self.assertContains(response, 'Veggie')
        self.assertContains(response, 'Radiobutton')
        self.assertContains(response, UPLOADED_FILE_NAME)

        step_data = {
            'event_create_wizard_view-current_step': 'confirmation',
        }
        response = self.client.post(reverse('events:create_event'), step_data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Weekend')
        self.assertFalse(file_storage.exists(UPLOADED_FILE_NAME))

        event = models.Event.objects.get(name='Weekend')
        self.assertEqual(event.place, 'Rothmannsthal')
        self.assertEqual(event.letter.name, UPLOADED_FILE_NAME)

        efield = models.EventField.objects.get(name='Remarks')
        self.assertEqual(efield.type, 0)

        self.assertTrue(Account.objects.filter(pk=event.account.id).exists())

    def test_event_update_view_not_allowed(self):
        response = self.client.get(reverse('events:edit_event', kwargs={'pk': self.event.pk}))
        self.assertEqual(response.status_code, 403)

        post_data = {
            'name': 'Comp',
            'from_date': '01.01.2017',
            'to_date': '08.01.2017',
            'place': 'Somewhere',
            'level': 3,
            'sections': self.section.id,
            'description': 'Cool event!',
            'fee': '100.99',
            'deadline': '31.12.2016',
            'letter': ''
        }
        response = self.client.post(
            reverse('events:edit_event', kwargs={'pk': self.event.pk}),
            post_data,
            follow=True
        )
        self.assertEqual(response.status_code, 403)

        event = models.Event.objects.get(pk=1)
        self.assertEqual(event.name, 'Camp')

    def test_event_update_view(self):
        assign_role(self.user, roles.Leader)
        response = self.client.get(reverse('events:edit_event', kwargs={'pk': self.event.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, _('Edit event'))

        post_data = {
            'name': 'Comp',
            'from_date': '01.01.2017',
            'to_date': '08.01.2017',
            'place': 'Somewhere',
            'level': 3,
            'sections': self.section.id,
            'description': 'Cool event!',
            'fee': '100.99',
            'deadline': '31.12.2016',
            'letter': ''
        }
        response = self.client.post(
            reverse('events:edit_event', kwargs={'pk': self.event.pk}),
            post_data,
            follow=True
        )
        self.assertEqual(response.status_code, 200)

        event = models.Event.objects.get(pk=1)
        self.assertEqual(event.name, 'Comp')
        self.assertEqual(event.level, 3)

    def test_event_delete_view_not_allowed(self):
        response = self.client.post(reverse('events:delete_event', kwargs={'pk': self.event.pk}))

        self.assertEqual(response.status_code, 403)

        events = models.Event.objects.all()
        self.assertEqual(len(events), 1)

    def test_event_delete_view(self):
        assign_role(self.user, roles.Leader)
        self.client.post(reverse('events:delete_event', kwargs={'pk': self.event.pk}))

        events = models.Event.objects.all()
        self.assertEqual(len(events), 0)

    def test_participation_create_view(self):
        efield1 = models.EventField.objects.create(
            event=self.event,
            type=1,
            name='Bla',
            required=False,
        )
        efield2 = models.EventField.objects.create(
            event=self.event,
            type=0,
            name='Remarks',
            required=False,
        )
        efield3 = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )

        post_data = {
            'member': self.member.nr,
            'Remarks': 'Hello World',
            'Vegetarian': 'Yesxx',
        }
        response = self.client.post(
            reverse('events:create_participation', kwargs={'event': self.event.pk}),
            post_data,
            follow=True
        )
        self.assertEqual(response.status_code, 200)

        participation = models.Participation.objects.get(pk=1)
        self.assertEquals(participation.member, self.member)
        self.assertEquals(participation.event, self.event)

        pfield = models.ParticipationField.objects.get(event_field=efield1)
        self.assertEquals(pfield.value, '')

        pfield = models.ParticipationField.objects.get(event_field=efield2)
        self.assertEquals(pfield.value, 'Hello World')

        pfield = models.ParticipationField.objects.get(event_field=efield3)
        self.assertEquals(pfield.value, 'Yesxx')

    def test_participation_create_view_wrong_user(self):
        member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100001,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m'
        )
        models.EventField.objects.create(
            event=self.event,
            type=1,
            name='Bla',
            required=False,
        )
        models.EventField.objects.create(
            event=self.event,
            type=0,
            name='Remarks',
            required=False,
        )
        models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )

        post_data = {
            'member': member2.nr,
            'Remarks': 'Hello World',
            'Vegetarian': 'Yesxx',
        }
        response = self.client.post(
            reverse('events:create_participation', kwargs={'event': self.event.pk}),
            post_data
        )
        self.assertEqual(response.status_code, 200)

        participations = models.Participation.objects.all()
        self.assertEquals(len(participations), 0)

        pfields = models.ParticipationField.objects.all()
        self.assertEquals(len(pfields), 0)

    def test_participation_create_view_deadline_over(self):
        self.event.deadline = datetime.date.today() - datetime.timedelta(days=1)
        self.event.save()
        models.EventField.objects.create(
            event=self.event,
            type=1,
            name='Bla',
            required=False,
        )
        models.EventField.objects.create(
            event=self.event,
            type=0,
            name='Remarks',
            required=False,
        )
        models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )

        post_data = {
            'member': self.member.nr,
            'Remarks': 'Hello World',
            'Vegetarian': 'Yesxx',
        }
        response = self.client.post(
            reverse('events:create_participation', kwargs={'event': self.event.pk}),
            post_data
        )
        self.assertEqual(response.status_code, 403)

        participations = models.Participation.objects.all()
        self.assertEquals(len(participations), 0)

        pfields = models.ParticipationField.objects.all()
        self.assertEquals(len(pfields), 0)

    def test_participation_update_view_get(self):
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        response = self.client.get(
            reverse('events:edit_participation', kwargs={'pk': participation.pk}),
        )

        self.assertContains(response, _('Change enrollment of %(member)s for %(event)s') % {
            'member': self.member,
            'event': self.event
        }, status_code=200)

    def test_participation_update_view_get_wrong_user(self):
        member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100001,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m'
        )
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=member2,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        response = self.client.get(
            reverse('events:edit_participation', kwargs={'pk': participation.pk}),
        )
        self.assertEqual(response.status_code, 403)

    def test_participation_update_view_post(self):
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        post_data = {
            'Vegetarian': 'Yesxx',
        }
        response = self.client.post(
            reverse('events:edit_participation', kwargs={'pk': participation.pk}),
            post_data,
            follow=True
        )
        self.assertEqual(response.status_code, 200)

        pfield = models.ParticipationField.objects.get(pk=1)
        self.assertEquals(pfield.value, 'Yesxx')

    def test_participation_update_view_post_wrong_user(self):
        member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100001,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m'
        )
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=member2,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        post_data = {
            'Vegetarian': 'Yesxx',
        }
        response = self.client.post(
            reverse('events:edit_participation', kwargs={'pk': participation.pk}),
            post_data,
            follow=True
        )
        self.assertEqual(response.status_code, 403)

        pfield = models.ParticipationField.objects.get(pk=1)
        self.assertEquals(pfield.value, 'Noxx')

    def test_participation_update_view_deadline_over(self):
        self.event.deadline = datetime.date.today() - datetime.timedelta(days=1)
        self.event.save()
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx'
        )

        post_data = {
            'Vegetarian': 'Yesxx',
        }
        response = self.client.post(
            reverse('events:edit_participation', kwargs={'pk': participation.pk}),
            post_data,
            follow=True
        )
        self.assertEqual(response.status_code, 403)

        pfield = models.ParticipationField.objects.get(pk=1)
        self.assertEquals(pfield.value, 'Noxx')

    def test_participation_delete_view_get(self):
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        response = self.client.get(
            reverse('events:delete_participation', kwargs={'pk': participation.pk})
        )

        self.assertContains(response, _('Withdraw enrollment of %(member)s for %(event)s') % {
            'member': self.member,
            'event': self.event
        }, status_code=200)

    def test_participation_delete_view_get_wrong_user(self):
        member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100001,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m'
        )
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=member2,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        response = self.client.get(
            reverse('events:delete_participation', kwargs={'pk': participation.pk}),
        )
        self.assertEqual(response.status_code, 403)

    def test_participation_delete_view_post(self):
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        response = self.client.post(
            reverse('events:delete_participation', kwargs={'pk': participation.pk}),
            follow=True
        )
        self.assertEqual(response.status_code, 200)

        pfields = models.ParticipationField.objects.all()
        self.assertEquals(len(pfields), 0)

    def test_participation_delete_view_post_wrong_user(self):
        member2 = Member.objects.create(
                nr=2,
                first_name='Maria',
                last_name='Mustermann',
                section=self.section,
                function=0,
                nami_nr=100001,
                fee=15.00,
                street='Musterstreet 12',
                zip='12345',
                town='Mustertown',
                dob='2000-01-01',
                gender='m'
        )
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=member2,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx')

        response = self.client.post(
            reverse('events:delete_participation', kwargs={'pk': participation.pk}),
            follow=True
        )
        self.assertEqual(response.status_code, 403)

        pfields = models.ParticipationField.objects.all()
        self.assertEquals(len(pfields), 1)

    def test_participation_delete_view_post_deadline_over(self):
        self.event.deadline = datetime.date.today() - datetime.timedelta(days=1)
        self.event.save()
        efield = models.EventField.objects.create(
            event=self.event,
            type=4,
            name='Vegetarian',
            required=True,
            choices='Yesxx, Noxx'
        )
        participation = models.Participation.objects.create(
            member=self.member,
            event=self.event
        )
        models.ParticipationField.objects.create(
            event_field=efield,
            participation=participation,
            value='Noxx'
        )

        response = self.client.post(
            reverse('events:delete_participation', kwargs={'pk': participation.pk}),
            follow=True
        )
        self.assertEqual(response.status_code, 403)

        pfields = models.ParticipationField.objects.all()
        self.assertEquals(len(pfields), 1)

    def test_subvention_list_view_not_allowed(self):
        response = self.client.get(
            reverse('events:subvention_list', kwargs={'event': self.event.pk}),
            follow=True
        )
        self.assertEqual(response.status_code, 403)
