from django import forms
from django.core.exceptions import ValidationError
from members.models import Member
from django.utils.translation import gettext_lazy as _
from django.forms import inlineformset_factory
from bootstrap_datepicker_plus.widgets import DatePickerInput

from members.models import Section
from . import models
from config.widgets import EURWidget, FileWidget
from config.widgets import BootstrapSelect2Widget, BootstrapSelect2MultipleWidget

TYPE_MAP = {
            0: forms.CharField(max_length=100),
            1: forms.CharField(
                max_length=100,
                widget=forms.Textarea(attrs={'rows': 5, 'cols': 30})
            ),
            2: forms.IntegerField(),
            3: forms.ChoiceField(),
            4: forms.ChoiceField(widget=forms.RadioSelect)
}

BOOL_CHOICES = ((True, _('Yes')), (False, _('No')))


class ParticipationCreateForm(forms.ModelForm):
    """
    Form for the creation of Participations in ParticipationCreateView.

    The users can choose from all their members which are not enrolled yet.
    All the EventField for the event are taken to build the form dynamically.
    For the value of every EventField a ParticipationField is stored related to the participation.
    """

    def __init__(self, user=None, event=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.fields['member'].queryset = Member.objects.filter(
            user=user
        ).exclude(
            participation__event=event
        )
        self.event_fields = models.EventField.objects.filter(event=event)
        for field in self.event_fields:
            self.fields[field.name] = TYPE_MAP[field.type]
            self.fields[field.name].required = field.required
            self.fields[field.name].label = field.name
            if field.choices:
                choices = field.choices.split(',')
                self.fields[field.name].choices = list(zip(choices, choices))

    def clean_member(self):
        member = self.cleaned_data['member']
        if member.user != self.user:
            raise ValidationError(_('Invalid member'), code='invalid')
        return member

    def save(self, commit=True):
        p = super().save()
        for field in self.event_fields:
            models.ParticipationField.objects.create(
                event_field=field,
                participation=p,
                value=self.cleaned_data[field.name]
            )
        return p

    class Meta:
        model = models.Participation
        fields = ['member']
        widgets = {
            'member': BootstrapSelect2Widget,
        }


class ParticipationUpdateForm(forms.ModelForm):
    def __init__(self, event=None, instance=None, *args, **kwargs):
        super().__init__(instance=instance, *args, **kwargs)
        self.event_fields = models.EventField.objects.filter(event=event)
        for field in self.event_fields:
            self.fields[field.name] = TYPE_MAP[field.type]
            self.fields[field.name].required = field.required
            if field.choices:
                choices = field.choices.split(',')
                self.fields[field.name].choices = list(zip(choices, choices))
            if instance:
                try:
                    self.fields[field.name].initial = models.ParticipationField.objects.get(
                        participation=instance,
                        event_field=field
                    ).value
                except Exception:
                    pass

    def save(self, commit=True):
        p = super().save()
        for field in self.event_fields:
            models.ParticipationField.objects.update_or_create(
                event_field=field,
                participation=p,
                defaults={'value': self.cleaned_data[field.name]}
            )
        return p

    class Meta:
        model = models.Participation
        fields = []


class EventUpdateForm(forms.ModelForm):
    class Meta:
        model = models.Event
        fields = [
            'name',
            'from_date',
            'to_date',
            'place',
            'fee',
            'sections',
            'level',
            'description',
            'deadline',
            'letter',
            'scoutnet_id',
        ]
        widgets = {
            'from_date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'to_date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'fee': EURWidget,
            'sections': BootstrapSelect2MultipleWidget,
            'level': BootstrapSelect2Widget,
            'deadline': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'letter': FileWidget,
            'scoutnet_id': forms.NumberInput(attrs={'min': '0'})
        }


class EventGeneralForm(forms.ModelForm):
    def import_scoutnet(self, event):
        self.fields['name'].initial = event['title']
        self.fields['from_date'].initial = event['start_date']
        self.fields['to_date'].initial = event['end_date']
        self.fields['place'].initial = event['location']
        sections = Section.objects.exclude(name='Ehemalige')

        if 'Wölfling' not in event['stufen']:
            sections = sections.exclude(name='Wölfling')
        if 'Jungpfadfinder' not in event['stufen']:
            sections = sections.exclude(name='Jungpfadfinder')
        if 'Pfadfinder' not in event['stufen']:
            sections = sections.exclude(name='Pfadfinder')
        if 'Rover' not in event['stufen']:
            sections = sections.exclude(name='Rover')
        self.fields['sections'].initial = sections
        if event['ebene'] == 'St. Vitus Hirschaid':
            self.fields['level'].initial = 0
        elif event['ebene'] == 'Bezirk 2':
            self.fields['level'].initial = 1
        elif event['ebene'] == 'DV Bamberg':
            self.fields['level'].initial = 2
        self.fields['description'].initial = event['description']
        self.fields['scoutnet_id'].initial = event['id']

    class Meta:
        model = models.Event
        fields = [
            'name',
            'from_date',
            'to_date',
            'place',
            'sections',
            'level',
            'description',
            'scoutnet_id',
        ]
        widgets = {
            'from_date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'to_date': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'sections': BootstrapSelect2MultipleWidget,
            'level': BootstrapSelect2Widget,
            'scoutnet_id': forms.NumberInput(attrs={'min': '0'})
        }


class EventLetterForm(forms.ModelForm):
    CHOICES = [
        ('True', 'Upload'),
        ('False', 'Create'),
    ]
    letter_source = forms.BooleanField(
            widget=forms.RadioSelect(choices=CHOICES),
            initial=True,
            required=False
    )

    class Meta:
        model = models.Event
        fields = [
            'fee',
            'deadline',
            'letter_source',
            'letter',
        ]
        widgets = {
            'fee': EURWidget,
            'deadline': DatePickerInput(options={'format': 'DD.MM.YYYY', 'locale': 'de'}),
            'letter': FileWidget,
        }


class EventFieldForm(forms.ModelForm):
    class Meta:
        model = models.EventField
        exclude = ['event']
        widgets = {
            'required': forms.RadioSelect(choices=[
                (False, _('No')),
                (True, _('Yes'))
            ]),
            'type': BootstrapSelect2Widget,
        }


EventFieldFormSet = inlineformset_factory(
    models.Event,
    models.EventField,
    form=EventFieldForm,
    extra=1
)


class LetterForm(forms.Form):
    title = forms.CharField(label=_('Title'), max_length=50)
    opening = forms.CharField(
        label=_('Opening'),
        max_length=100,
        initial=_('Dear scouts, dear parents,')
    )
    text = forms.CharField(label=_('Text'), widget=forms.Textarea)
    closing = forms.CharField(
        label=_('Closing'),
        max_length=50,
        initial=_('Be prepared, your Leaders'),
        disabled=True
    )
    summary = forms.BooleanField(label=_('Summary'), required=False)


class EventFinancesForm(forms.Form):
    create_account = forms.BooleanField(
        label=_('Create account for event?'),
        widget=forms.RadioSelect(
            choices=BOOL_CHOICES,
        ),
        initial=True,
        required=False
    )


class EventStockForm(forms.Form):
    pass


class EventConfirmationForm(forms.Form):
    pass
