"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.urls import path
from django.utils.translation import gettext_lazy as _

from . import views

app_name = 'events'

urlpatterns = [
    path(
        _('subvention-list/<event>/'),
        views.SubventionListView.as_view(),
        name='subvention_list'
    ),
    path(
        _('participant-list/<event>/'),
        views.ParticipantListView.as_view(),
        name='participant_list'
    ),
    path(_('create-event/'), views.EventCreateWizardView.as_view(), name='create_event'),
    path(_('edit-event/<pk>/'), views.EventUpdateView.as_view(), name='edit_event'),
    path(_('delete-event/<pk>/'), views.EventDeleteView.as_view(), name='delete_event'),
    path(
        _('create-participation/<event>/'),
        views.ParticipationCreateView.as_view(),
        name='create_participation'
    ),
    path(
        _('edit-participation/<pk>/'),
        views.ParticipationUpdateView.as_view(),
        name='edit_participation'
    ),
    path(
        _('delete-participation/<pk>/'),
        views.ParticipationDeleteView.as_view(),
        name='delete_participation'
    ),
    path(_('letter-preview/'), views.LetterPreviewView.as_view(), name='letter_preview'),
    path('<pk>/', views.EventDetailView.as_view(), name='details'),
    path('', views.EventListView.as_view(), name='list'),
]
