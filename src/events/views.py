from django.views.generic.base import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.utils.translation import gettext_lazy as _
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.utils.text import slugify
from django_tex.views import render_to_pdf
from django_tex.core import compile_template_to_pdf
from easy_pdf.views import PDFTemplateView
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.templatetags.static import static
from django.http import HttpResponseBadRequest, HttpResponse
from django.core.files.uploadedfile import SimpleUploadedFile
from django.db.models import Sum
from rolepermissions.mixins import HasRoleMixin
from formtools.wizard.views import SessionWizardView
from decimal import Decimal
import datetime
import os
import csv
import codecs

from . import models
from . import forms
from . import utils
from finances.utils import get_debit_dates
from finances.models import Debit, Account, DebitGroup
from authentication import roles

# Create your views here.


class EventListView(LoginRequiredMixin, ListView):
    model = models.Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = datetime.date.today()
        for event in context['object_list']:
            event.participating = models.Participation.objects.filter(
                member__user=self.request.user,
                event=event
            )
        return context


class EventDetailView(LoginRequiredMixin, HasRoleMixin, DetailView):
    """
    Shows leaders detailed informations about an event.

    Also loads the respective data from the Scoutnet calendar.
    Lists all participants with the possibility to export lists.
    Shows an overview over all debits grouped accordingly.
    Shows the account related to the event.
    """

    allowed_roles = [roles.Board, roles.Leader]
    model = models.Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            scoutnet = utils.Scoutnet()
            context['scoutnet_object'] = scoutnet.get_event(self.object.scoutnet_id)
        except Exception:
            context['scoutnet_object'] = {'title': _('Scoutnet is not available at the moment!')}

        # Get current debit group
        try:
            current_group = DebitGroup.objects.get(
                from_date__lte=datetime.date.today(),
                to_date__gte=datetime.date.today()
            )
        except ObjectDoesNotExist:
            fromd, to, execute = get_debit_dates()
            current_group = DebitGroup.objects.create(
                from_date=fromd,
                to_date=to,
                execution_date=execute
            )

        debits = Debit.objects.filter(participation__event=self.object)
        groups = DebitGroup.objects.filter(
            debit__in=debits
        ).exclude(
            id=current_group.id
        ).order_by(
            'execution_date'
        ).distinct()

        context['current_group'] = {
            'date': current_group.execution_date,
            'count': debits.filter(group=current_group).count(),
            'sum': debits.filter(group=current_group).aggregate(Sum('amount'))['amount__sum']
        }
        if context['current_group']['sum'] is None:
            context['current_group']['sum'] = Decimal(0.00)

        context['groups'] = []
        for group in groups:
            g = {
                'date': group.execution_date,
                'count': debits.filter(group=group).count(),
                'sum': debits.filter(group=group).aggregate(Sum('amount'))['amount__sum']
            }
            if g['sum'] is None:
                g['sum'] = Decimal(0.00)
            context['groups'].append(g)

        context['all'] = {
            'count': models.Participation.objects.filter(event=self.object).count(),
            'sum': models.Participation.objects.filter(
                event=self.object
            ).count() * self.object.fee,
        }

        group_count = 0
        group_sum = Decimal(0.00)
        for g in context['groups']:
            group_count = group_count + g['count']
            group_sum = group_sum + g['sum']

        context['notcreated'] = {
            'count': context['all']['count'] - context['current_group']['count'] - group_count,
            'sum': context['all']['sum'] - context['current_group']['sum'] - group_sum
        }

        context['participations'] = self.object.participation_set.order_by(
            'member__section',
            'member__function',
            'member__first_name',
            'member__last_name'
        )

        return context


class SubventionListView(LoginRequiredMixin, HasRoleMixin, PDFTemplateView):
    allowed_roles = [roles.Board, roles.Leader]
    template_name = 'events/subvention_list.html'

    def dispatch(self, request, event, *args, **kwargs):
        self.event = get_object_or_404(models.Event, pk=event)
        return super().dispatch(request, *args, **kwargs)

    def get_pdf_filename(self):
        return _('subvention_list_%s.pdf') % slugify(self.event.name)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['participants'] = self.event.participants.filter(function=0)
        context['leaders'] = self.event.participants.filter(function=1)
        return context


class ParticipantListView(LoginRequiredMixin, HasRoleMixin, View):
    allowed_roles = [roles.Board, roles.Leader]

    def dispatch(self, request, event, *args, **kwargs):
        self.event = get_object_or_404(models.Event, pk=event)
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        participations = self.event.participation_set.all().order_by('member')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = ('attachment; filename="%s"'
                                           % (_('participation_list_%s.csv')
                                              % slugify(self.event.name)))
        response.write(codecs.BOM_UTF8)
        writer = csv.writer(response)
        writer.writerow([
            _('first name'),
            _('last name'),
            _('section'),
            _('function'),
            _('date of birth'),
            _('landline number'),
            _('photo rights')
        ] + [field.name for field in self.event.eventfield_set.all()])
        for p in participations:
            writer.writerow([
                p.member.first_name,
                p.member.last_name,
                p.member.section,
                p.member.get_function_display(),
                p.member.dob,
                p.member.landline,
                _('Yes') if p.member.photo_rights else _('No'),
            ] + [field.value for field in p.participationfield_set.all()])
        return response


class EventCreateWizardView(LoginRequiredMixin, HasRoleMixin, SessionWizardView):
    allowed_roles = [roles.Board, roles.Leader]
    form_list = [
        ('general', forms.EventGeneralForm),
        ('letter', forms.EventLetterForm),
        ('enrollment', forms.EventFieldFormSet),
        ('finances', forms.EventFinancesForm),
        ('stock', forms.EventStockForm),
        ('confirmation', forms.EventConfirmationForm),
    ]
    file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'tmp'))
    templates = {
        'general': 'events/event_general.html',
        'letter': 'events/event_letter.html',
        'enrollment': 'events/event_field.html',
        'finances': 'events/event_finances.html',
        'stock': 'events/event_stock.html',
        'confirmation': 'events/event_confirmation.html',
    }
    instance = None

    def get_template_names(self):
        return [self.templates[self.steps.current]]

    def done(self, form_list, form_dict, **kwargs):
        event = form_dict['general'].save(commit=False)
        event.fee = form_dict['letter'].cleaned_data['fee']
        event.letter = form_dict['letter'].cleaned_data['letter']
        event.deadline = form_dict['letter'].cleaned_data['deadline']
        event.save()
        event.sections.set(form_dict['general'].cleaned_data['sections'])
        if form_dict['finances'].cleaned_data['create_account']:
            event.account = Account.objects.create(
                name=event.name,
                type=1,
                year=event.from_date.year
            )
        event.save()
        for event_field in form_dict['enrollment'].save(commit=False):
            event_field.event = event
            event_field.save()
        return redirect('events:details', pk=event.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        step = self.steps.current
        if step == 'general':
            try:
                scoutnet = utils.Scoutnet()
                context['scoutnet_events'] = scoutnet.get_all_events()
            except Exception:
                context['scoutnet_events'] = [
                    {'title': _('Scoutnet is not available at the moment!')}
                ]
        if step == 'letter':
            context['letter_form'] = forms.LetterForm(
                initial={
                    'title': self.get_cleaned_data_for_step('general')['name']
                }
            )
        if step == 'confirmation':
            context['event'] = self.get_all_cleaned_data()
            context['level'] = models.Event(level=context['event']['level']).get_level_display()
            context['event']['enrollment'] = context['event'].pop('formset-enrollment')
            for field in context['event']['enrollment']:
                if field:
                    field['type_display'] = models.EventField(
                        type=field['type']).get_type_display()

        return context

    def get_form(self, step=None, data=None, files=None):
        form = super().get_form(step, data, files)
        if step is None:
            step = self.steps.current
        if step == 'general':
            scoutnet_id = self.request.GET.get('import', 0)
            if scoutnet_id != 0:
                try:
                    scoutnet = utils.Scoutnet()
                    scoutnet_event = scoutnet.get_event(scoutnet_id)
                    form.import_scoutnet(scoutnet_event)
                except Exception:
                    pass
        return form

    def get_form_instance(self, step):
        if step is None:
            step = self.steps.current
        if step != 'general':
            if self.instance is None:
                self.instance = models.Event()
            return self.instance
        else:
            return super().get_form_instance(step)

    def get_form_step_files(self, form):
        step = self.steps.current
        if step == 'letter':
            if not self.get_cleaned_data_for_step('letter')['letter_source']:
                letter_data = (self.request.session
                               ['wizard_event_create_wizard_view']['step_data']['letter'])
                return {
                    'letter-letter': SimpleUploadedFile(
                        '%s.pdf' % letter_data['title'][0].lower().replace(' ', ''),
                        compile_template_to_pdf('events/letter.tex', {
                            'logo': '%s%s' % (
                                settings.BASE_DIR,
                                os.path.splitext(static('img/logo.png'))[0]
                            ),
                            'dpsg_logo': '%s%s' % (
                                settings.BASE_DIR,
                                os.path.splitext(static('img/dpsg_logo.png'))[0]
                            ),
                            'start': '%s%s' % (
                                settings.BASE_DIR,
                                os.path.splitext(static('img/start.png'))[0]
                            ),
                            'finish': '%s%s' % (
                                settings.BASE_DIR,
                                os.path.splitext(static('img/finish.png'))[0]
                            ),
                            'event_data': (self.request.session
                                           ['wizard_event_create_wizard_view']['step_data']),
                            'title': letter_data['title'][0],
                            'opening': letter_data['opening'][0],
                            'text': letter_data['text'][0],
                            'closing': letter_data['closing'][0].replace(', ', ',\\\\'),
                        })
                    )
                }
        return form.files


class EventUpdateView(LoginRequiredMixin, HasRoleMixin, SuccessMessageMixin, UpdateView):
    allowed_roles = [roles.Board, roles.Leader]
    success_message = _("Event '%(name)s' has been updated successfully!")
    form_class = forms.EventUpdateForm

    def get_queryset(self):
        return models.Event.objects.all()

    def get_success_url(self):
        return reverse('events:details', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Edit event')
        return context


class EventDeleteView(LoginRequiredMixin, HasRoleMixin, DeleteView):
    allowed_roles = [roles.Board, roles.Leader]
    success_message = _("Event '%(name)s' has been deleted successfully!")
    model = models.Event
    success_url = reverse_lazy('events:list')

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)


class ParticipationCreateView(LoginRequiredMixin, CreateView):
    """
    Users can create a participation to an event for one of their members.

    The event deadline is checked.
    """

    model = models.Participation
    form_class = forms.ParticipationCreateForm

    def dispatch(self, request, event, *args, **kwargs):
        self.event = get_object_or_404(models.Event, pk=event)
        if(self.event.deadline < datetime.date.today()):
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user, 'event': self.event})
        return kwargs

    def get_initial(self):
        return {'event': self.event}

    def get_success_url(self):
        return reverse('events:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Enroll for %(event)s') % {'event': self.event}
        return context

    def form_valid(self, form):
        form.instance.event = self.event
        return super().form_valid(form)


class ParticipationUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Participation
    form_class = forms.ParticipationUpdateForm

    def dispatch(self, request, pk, *args, **kwargs):
        self.user = request.user
        participation = get_object_or_404(models.Participation, pk=pk)
        if(participation.event.deadline < datetime.date.today()):
            raise PermissionDenied
        if participation.member.user != self.request.user:
            raise PermissionDenied
        return super().dispatch(request, pk, *args, **kwargs)

    def get_queryset(self):
        return models.Participation.objects.filter(member__user=self.user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'event': self.object.event})
        return kwargs

    def get_success_url(self):
        return reverse('events:list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Change enrollment of %(member)s for %(event)s') % {
            'member': self.object.member,
            'event': self.object.event
        }
        return context

    def form_valid(self, form):
        form.instance.event = self.object.event
        form.instance.member = self.object.member
        return super().form_valid(form)


class ParticipationDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Participation
    success_url = reverse_lazy('events:list')

    def dispatch(self, request, pk, *args, **kwargs):
        self.user = request.user
        participation = get_object_or_404(models.Participation, pk=pk)
        if(participation.event.deadline < datetime.date.today()):
            raise PermissionDenied
        if participation.member.user != self.request.user:
            raise PermissionDenied
        return super().dispatch(request, pk, *args, **kwargs)

    def get_queryset(self):
        return models.Participation.objects.filter(member__user=self.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Withdraw enrollment of %(member)s for %(event)s') % {
            'member': self.object.member,
            'event': self.object.event,
        }
        return context


class LetterPreviewView(LoginRequiredMixin, HasRoleMixin, View):
    allowed_roles = [roles.Board, roles.Leader]

    def post(self, request):
        form = forms.LetterForm(request.POST)
        if form.is_valid():
            return render_to_pdf('events/letter.tex', {
                'logo': '%s%s' % (
                    settings.BASE_DIR,
                    os.path.splitext(static('img/logo.png'))[0]
                ),
                'dpsg_logo': '%s%s' % (
                    settings.BASE_DIR,
                    os.path.splitext(static('img/dpsg_logo.png'))[0]
                ),
                'start': '%s%s' % (
                    settings.BASE_DIR,
                    os.path.splitext(static('img/start.png'))[0]
                ),
                'finish': '%s%s' % (
                    settings.BASE_DIR,
                    os.path.splitext(static('img/finish.png'))[0]
                ),
                'event_data': request.session['wizard_event_create_wizard_view']['step_data'],
                'title': form.cleaned_data['title'],
                'opening': form.cleaned_data['opening'],
                'text': form.cleaned_data['text'],
                'closing': form.cleaned_data['closing'].replace(', ', ',\\\\'),
                'summary': form.cleaned_data['summary'],
                }, filename=_('letter.pdf'))
        else:
            return HttpResponseBadRequest('Form validation error!')
