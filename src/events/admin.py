from django.contrib import admin

from . import models

# Register your models here.

admin.site.register(models.Event)
admin.site.register(models.Participation)
admin.site.register(models.EventField)
admin.site.register(models.ParticipationField)
