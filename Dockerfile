FROM python:3.12

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install pdm

COPY ./pyproject.toml .
RUN pdm install

COPY ./src ./src

WORKDIR /app/src